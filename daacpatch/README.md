# ospatch

Ansible code for OS patching/automation

## Instructions
<code>

#  first, send a  pre-patch message...
ansible -i [devhosts.ini|prodhosts.ini] -m shell -a 'echo "system will be coming down for patching in <N>  minutes.  If you have any questions, please call <Person> at <phone number>"| wall' all

#  next, in week1 (wednesday) patch "DEV" 
ansible-playbook -f 100 -i devhosts.ini patch.yaml 

#  next, in week2 (wednesday) patch "PROD" 
ansible-playbook -f 100 -i prodhosts.ini patch.yaml 

</code>

## NOTES:
1. Start patching at 06:00 to ensure last reboot completes by 08:00
2. Make sure to use '-f 100' to allow more parallel tasks.

## User settable variables
This playbooks behavior is altered by user-settable variables found in the inventory file 'hosts.ini', under the "[all:vars]" section header.  Any or all of the variables may be modified for a group by placing them in "[<group name>:vars]" or after a hostname entry.


## Other programs
1. showvars.yaml - a playbook to display the user-settable variables per host
   Run as follows...
<code>
   $ ansible-playbook -i hosts.ini showvars.yaml
</code>

2. lpass_vault.sh - a shell script that queries and displays LastPass password.  It is an alternate credential provided to Ansible vault when encrypting files or variables.  Instead of keeping a local vault password file, this allows us to extract one from a central (LastPass) source.  See README.vault.

