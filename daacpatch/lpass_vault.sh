#!/usr/bin/env bash 

#
# This script uses the 'lpass' utility to login to LastPass
# then extract/display a password from said account.
# I was using this to test accessing a vault password used
# to encrypt/decrypt sensitive content used for playbooks
# 
export LPASS_DISABLE_PINENTRY=1
lpass login rupppa@ornl.gov
PASSWORD=`lpass show --password "ansible vault"` 
echo $PASSWORD 
