#

export PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin


# increase history size
export HISTSIZE=5000
export HISTFILESIZE=5000
 
# add timestamp to history
export HISTTIMEFORMAT="%F %T "


# Set up history file based on logname

EAI_IDNAME=$( /usr/bin/id -un )
EAI_LOGNAME=$( /usr/bin/logname 2> /dev/null )

if [[ -z $EAI_LOGNAME ]]; then
    EAI_LOGNAME="undef"
fi

if [[ $EAI_IDNAME != $EAI_LOGNAME && ! $(echo "$HISTFILE" | grep -w $EAI_LOGNAME ) ]]; then
    export HISTFILE=$HISTFILE.$EAI_LOGNAME
fi

