#!/bin/bash
#
# Name:    check_f5_connections_age.sh
# Purpose: a Nagios plugin to check connections older than 24 hours 
#
###########################################################################
# Global Variables

BASE_DIR="/usr/lib/nagios/plugins"
PROGNAME=`/bin/basename $0`

# Include Nagios Utility Variables
. $BASE_DIR/utils.sh

###########################################################################
# Function: print_usage
# Purpose:  Short usage output
#
print_usage() {
    echo "Usage: $PROGNAME [-i 10.158.94.192/28] [-s server] [-h] [-p] [-w WARN] [-c CRIT]"
    echo "Usage: $PROGNAME --help"
}

###########################################################################
# Function: print_help
# Purpose:  Extended usage output
#
print_help() {
    echo $PROGNAME
    echo ""
    echo "A plugin to check connections older than 24 hours"
    echo ""
    print_usage
    echo ""
    echo "Options:"
    echo " -h"
    echo "    print detailed help screen"
    echo " -i <subnet to check>"
    echo " -p"
    echo "    Output perfdata"
    echo " -s <server>"
    echo " -w <WARN>"
    echo "    The warning threshold percentage"
    echo " -c <CRIT>"
    echo "    The critical threshold percentage"
    echo ""
}

###########################################################################
# Main

exitstatus=$STATE_UNKNOWN #default
while test -n "$1"; do
    case "$1" in
        -h)
            print_help
            exit $STATE_OK
            ;;
        -i)
            subnet=$2
            shift
            ;;
        -p)
            perfdata=1
            ;;
        -s)
            server=$2
            shift
            ;;
        -w)
            warn=$2
            shift
            ;;
        -c)
            crit=$2
            shift
            ;;
        *)
            echo "Unknown argument: $1"
            print_usage
            exit $STATE_UNKNOWN
            ;;
    esac
    shift
done

if [[ -n $warn ]] && [[ -n $crit ]]; then
    if [[ $warn -ge $crit ]]; then
        echo "The warning threshold must be lower than the critical threshold"
        print_usage
        exit $STATE_UNKNOWN
    fi
fi

if [[ ! -n $subnet ]]; then
    echo "Subnet is required."
    print_usage
    exit $STATE_UNKNOWN
fi

if [[ ! -n $server ]]; then
    echo "server is required."
    print_usage
    exit $STATE_UNKNOWN
fi

# Command to check connections that will be reaped
COMMAND='tmsh show /sys connection cs-client-addr '${subnet}' age 86400 max-result-limit infinite | grep ^Total | grep -o [0-9]*'
CONNECTIONS=`/bin/ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=error root@${server} ${COMMAND}`

if [[ $perfdata -eq 1 ]]; then
    PERF_OUT="|CONNECTIONS="$CONNECTIONS";${warn:-0};${crit:-0};"
fi

if [[ -n $warn ]] && [[ -n $crit ]]; then
    if [[ $CONNECTIONS -lt $warn ]]; then
        echo "OK - $CONNECTIONS$PERF_OUT"
        exit $STATE_OK
    elif [[ $CONNECTIONS -lt $crit ]]; then
        echo "WARNING - $CONNECTIONS$PERF_OUT"
        exit $STATE_WARNING
    else
        echo "CRITICAL - $CONNECTIONS$PERF_OUT"
        exit $STATE_CRITICAL
    fi
elif [[ -n $warn ]] && [[ -z $crit ]]; then
    if [[ $CONNECTIONS -lt $warn ]]; then
        echo "OK - $CONNECTIONS$PERF_OUT"
        exit $STATE_OK
    else
        echo "WARNING - $CONNECTIONS$PERF_OUT"
        exit $STATE_WARNING
    fi
elif [[ -z $warn ]] && [[ -n $crit ]]; then
    if [[ $CONNECTIONS -lt $crit ]]; then
        echo "OK - $CONNECTIONS$PERF_OUT"
        exit $STATE_OK
    else
        echo "CRITICAL - $CONNECTIONS$PERF_OUT"
        exit $STATE_CRITICAL
    fi
else
    echo "OK - $CONNECTIONS$PERF_OUT"
    exit $STATE_OK
fi

###########################################################################

