#!/bin/bash
#set -x

print_usage() {
    echo ""
    echo " Usage: snmpCounterGather.sh -v <SNMP Version> -c <SNMP Community> -h <host to poll> -u <units if the returned value> -o <SNMP OID to poll>"
    echo " Usage: snmpCounterGather.sh -help"
}

print_help() {
    echo ""
    echo "This script is to be used to poll values via SNMP.  It is called with 5 options:"
    echo "    SNMP Version: This should always be 2c. This script has not been tested"
    echo "                  with other snmp version"
    echo "    SNMP Community:  This should be public, unless you have reason to change it"
    echo "    Host to poll: The IP address or DNS name of the host to poll"
    echo "    Units: What are the units of the returned value? Bytes, packets, bits, etc."
    echo "    OID: The actual oid to poll. "
    echo ""
    echo "For example:"
    echo "    $ snmpCounterGather.sh -v 2c -c public -H ltm-sg-private-float -u bytes -o .1.3.6.1.4.1.3375.2.2.10.2.3.1.9.14.112.101.103.97.115.117.115.95.52.52.51.95.115.103"
    echo ""

}

if [ $# -ne 0 ] ; then
    while [[ $# -ne 0 && -n "$1" ]] ; do
        case "$1" in
            -h|-help)
                print_help
                exit
                ;;
            -v)
                shift
                VER=$1
                ;;
            -c)
                shift
                COM=$1
                ;;
            -H)
                shift
                HOST=$1
                ;;
            -u)
                shift
                VALUE=$1
                ;;
            -o)
                shift
                OID=$1
                ;;
            *)
                echo "Unknown argument: $1"
                print_usage
                exit
                ;;
        esac
        shift
    done
else
    print_usage
    exit
fi

#echo "the command line options given are:"
#echo " -v $VER"
#echo " -c $COM"
#echo " -H $HOST"
#echo " -u $VALUE"
#echo " -o $OID"

TRANS=`/usr/lib/nagios/plugins/check_snmp -H $HOST -C $COM -P $VER -o $OID | awk '{print $4}'`
if [[ $TRANS == "Timeout:" ]] ; then
    TRANS=U
fi

echo "OK $TRANS | ${VALUE}=${TRANS}c"
