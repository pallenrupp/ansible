#!/bin/bash
#
# Name:    check_tsm_session.sh
# Purpose: a Nagios plugin to check if active TSM session
# Author:  Kyle England
#
###########################################################################
# Global Variables

BASE_DIR="/usr/lib/nagios/plugins"
PROGNAME=`/bin/basename $0`
TSM_CMD="/usr/bin/dsmadmc -id=nagios -pa=nagios q ses > /dev/null"

# Include Nagios Utility Variables
. $BASE_DIR/utils.sh

TSM_OUTPUT=`$TSM_CMD`
if [[ $? -eq 0 ]]; then
    echo "TSM OK: Session check return code is zero"
    exit $STATE_OK
else
    echo "TSM CRITICAL: Session check return code is non-zero"
    exit $STATE_CRITICAL
fi

