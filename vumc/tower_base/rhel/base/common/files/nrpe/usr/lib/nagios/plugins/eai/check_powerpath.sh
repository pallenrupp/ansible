#!/bin/sh

# Author: Rael Mussell
# Date: 2/22/2010
# Purpose: Used through groundworks to detect degraded EMC paths.
#
# Changed state where powerpath not installed to warn -eah
# Changed path to sudo for RHEL -eah

HOSTNAME=$1
OS=`uname -s`

# Routine to check is PowerPath is installed and being used.  If not, exit 0
case "$OS" in
	'Linux' )
		POWERPATH_CHECK=`rpm -qa | grep EMC | wc -l`
			if [ $POWERPATH_CHECK -eq 1 -o $POWERPATH_CHECK -gt 1 ]; then
				CHECK_DEGRADED=`/usr/bin/sudo /sbin/powermt display | grep degraded | wc -l`
					if [ $CHECK_DEGRADED -eq 1 -o $CHECK_DEGRADED -gt 1 ]; then
						echo "CRITICAL - One or more SAN paths is in degraded mode."
						exit 2
					else
						echo "OK - No SAN paths in degraded mode"
						exit 0
					fi
			else
				echo "WARN - EMC PowerPath not installed.  No valid check."
				exit 1
			fi
	;;
	'HP-UX' )
		POWERPATH_CHECK=`/usr/sbin/swlist | grep EMC | wc -l`
			if [ $POWERPATH_CHECK -eq 1 -o $POWERPATH_CHECK -gt 1 ]; then
				CHECK_DEGRADED=`/usr/local/bin/sudo /sbin/powermt display | grep degraded | wc -l`
					if [ $CHECK_DEGRADED -eq 1 -o $CHECK_DEGRADED -gt 1 ]; then
						echo "SAN issue detected.  One or more SAN paths is in degraded mode."
						exit 2
					else
						echo "SAN OK"
						exit 0
					fi
			else
				echo "EMC PowerPath not installed.  No valid check."
				exit 1
			fi
	;;
	'SunOS' )
		POWERPATH_CHECK=`pkginfo | grep EMC | wc -l`
			if [ $POWERPATH_CHECK -eq 1 -o $POWERPATH_CHECK -gt 1 ]; then
				CHECK_DEGRADED=`/usr/local/bin/sudo /etc/powermt display | grep degraded | wc -l`
					if [ $CHECK_DEGRADED -eq 1 -o $CHECK_DEGRADED -gt 1 ]; then
						echo "SAN issue detected.  One or more SAN paths is in degraded mode."
						exit 2
					else
						echo "SAN OK."
						exit 0
					fi
			else
				echo "EMC PowerPath not installed.  No valid check."
				exit 1
			fi
	;;
esac
