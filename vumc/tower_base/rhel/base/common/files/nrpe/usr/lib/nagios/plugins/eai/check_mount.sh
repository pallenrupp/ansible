#!/bin/bash
# $Id: check_mount.sh,v 1.2 2013/12/19 16:55:36 shutemh Exp $ 
#
# description : Nagios plugin to check for mounted NFS mapping /etc/fstab 
# author      : Michael Shute
# usage       : check_mount.sh /localmount
# notes       : To fulfill IM0863709
#==============================================================================


# GET ARGS
NFS_MOUNT=( $1 );


# EXIT CODES
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3


# TRAP FOR MISSING MOUNT VALUE
if [ -f $NFS_MOUNT ]; then
        echo -e "Mount operator missing";
        exit $STATE_UNKNOWN
fi


# CHECK FOR NFS TYPE IN FSTAB
FSTAB_NFS_MOUNT=( $(/bin/grep -v ^\# /etc/fstab | /bin/grep $NFS_MOUNT | /bin/grep ' nfs ' | /bin/awk '{print $2}') )


# CHECK FOR MISSING NFS MOUNT
if [ "$FSTAB_NFS_MOUNT" = "$NFS_MOUNT" ]; then

        if [ $(/usr/bin/stat -f -c '%T' $NFS_MOUNT) = "nfs" ]; then
                echo "NFS MOUNT OK";
                exit $STATE_OK;
        else
                echo "CRITICAL: $NFS_MOUNT unavailable";
                exit $STATE_CRITICAL;
        fi

else
        echo "WARNING: $NFS_MOUNT missing from fstab or may not be NFS type";
        exit $STATE_WARNING
fi

