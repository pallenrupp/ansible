#!/bin/bash
#
# check_cf-agent.sh
#
# Nagios plugin to check for successful execution of cf-agent

##################################################
#
#  Added for troubleshooting -- CMW
#
#echo "OK, cf-agent ran successfully"
#exit 0
#
##################################################

myTime=`date +%s`
#echo "DEBUG:  \$myTime equals ***${myTime}***"

runlogTime=`stat -c '%Y' /var/cfengine/cf3.*.runlog`
runlogAge=`expr $myTime - $runlogTime`
#echo "DEBUG:  \$runlogAge equals ***${runlogAge}***"

sudo tail -n 1 /var/cfengine/promise_summary.log | grep "Promises observed to be kept" 2>&1 > /dev/null
invalidLastLine=`echo $?`
#echo "DEBUG:  \$invalidLastLine equals ***${invalidLastLine}***"

if [ $invalidLastLine -ne 0 ]; then
     echo "CRITICAL, cf-agent failed to recently run successfully"
     exit 2
elif [ $runlogAge -gt 3600 ]; then
     echo "CRITICAL, cf-agent failed to recently run successfully"
     exit 2
else
     lastRunTime=`sudo tail -n 1 /var/cfengine/promise_summary.log | grep "Promises observed to be kept" | sed 's/,.*//'`
     #echo "DEBUG:  \$lastRunTime equals ***${lastRunTime}***"
     timeDifference=`expr $myTime - $lastRunTime`
     #echo "DEBUG:  \$timeDifference equals ***${timeDifference}***"

     if [ $timeDifference -gt 3600 ]; then
          echo "CRITICAL, cf-agent failed to recently run successfully"
          exit 2
     else
          echo "OK, cf-agent has recently run successfully"
          exit 0
     fi
fi
