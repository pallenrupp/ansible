#!/usr/bin/perl -w
# This code is not supported by F5 Network and is offered under the GNU General Public License.  Use at your own risk.
 
use strict;
use Net::SNMP qw(:snmp);
use Switch;
 
my ($host, $snmp_comm, $warn, $crit, $error_msg);
 
if (! @ARGV) {
    print "Please enter the LTM host name or IP: ";
    chomp ($host = <STDIN>);
    print "\nPlease enter the LTM SNMP Community String: ";
    chomp ($snmp_comm = <STDIN>);
    print "\nPlease enter the warning threshold: ";
    chomp ($warn = <STDIN>);
    print "\nPlease enter the critical threshold: ";
    chomp ($crit = <STDIN>);
} else {
    my $usage = "f5_connections.pl <host> <snmp community> <warn> <crit>";
 
    die "Usage: $usage\n" if $#ARGV != 3;
 
    $host = $ARGV[0];
    $snmp_comm = $ARGV[1];
    $warn = $ARGV[2];
    $crit = $ARGV[3];
    chomp ($host , $snmp_comm);
}

my $ltm_ActiveConnections = ".1.3.6.1.4.1.3375.2.1.8.2.3.1.19.3.49.46.48";
 
my ($session, $error) = Net::SNMP->session(
            -hostname       => $host,
            -community      => $snmp_comm,
            -port           => 161,
            -version        => 'snmpv2c',
            -nonblocking    => 0
            );
 
if (!defined $session) {
        $error_msg="Received no SNMP response from $host";
        print STDERR "Error: $error\n";
        }
    
#Get first instance
my $oids_1 = $session->get_request(
	  -varbindlist =>
	  [$ltm_ActiveConnections] );

my $check_var=$oids_1->{$ltm_ActiveConnections};

if($check_var < $warn){
	print "OK - $check_var | connections=$check_var;$warn;$crit;0; \n"; 
	exit(0);
}elsif($check_var >= $warn && $check_var < $crit){
	print "WARNING - $check_var | connections=$check_var;$warn;$crit;0; \n"; 
	exit(1);
}elsif($check_var >= $crit){
	print "CRITICAL - $check_var | connections=$check_var;$warn;$crit;0; \n"; 
	exit(2);
}else{
	print "UNKNOWN - $error_msg\n"; exit(3);
}

