#!/bin/bash
#
# description : Nagios plugin to check for RO file systems on hosts and warn only.
# author      : Michael Shute
# usage       : check_ROfilesys.sh
# notes       : To fulfill IM0876452
# updates     : 2014-01-31 - Michael Shute
#             :    Corrected plugin to only look at ext filesys
#             : 2017-01-10 - Wilbur Longwisch
#             :    Added xfs to valid filesystem types for RHEL7
#             : 2017-05-25 - Wilbur Longwisch
#             :    Added in checks for snap shotted lvm volumes and to ignore
#             :    them outside of regular business hours since they are being
#             :    used by the backup system
#             : 2017-10-05 - Jeff Neely
#             :    alter check to ignore filesystems mounted with nouuid flag
#             :    it is not uncommon to filesystes to stay mounted for Avamar
#==============================================================================

# TIME 
START=060000
END=180000
NOW=`date +"%H%M%S"`

# EXIT CODES
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3


# CHECK FOR RO MOUNTS IN /proc/mounts
#RO_MOUNT=( $(awk '$4~/(^|,)ro($|,)/' /proc/mounts) )
#RO_MOUNT=( $(grep '\(xfs\|ext[3,4]\)' /proc/mounts | awk '$4~/(^|,)ro($|,)/') )
RO_MOUNT=( $(grep '\(xfs\|ext[3,4]\)' /proc/mounts | grep -v nouuid | awk '$4~/(^|,)ro($|,)/') )


# we've found a read only mount, look closer to see if it's an 
# lvm snapshot that we don't care about
if [ "$RO_MOUNT" ]; then
    OIFS="$IFS"
    IFS=$'\n'

    SNAPFS=""
    for x in `grep '\(xfs\|ext[3,4]\)' /proc/mounts | grep mapper | grep -v nouuid | awk '$4~/(^|,)ro($|,)/'`; do 
        MOUNT=`echo ${x} | cut -d\  -f1`; 
        SNAPTEST=$(sudo lvdisplay $MOUNT | grep -i snapshot);
        if [ $? -ne 0 ]
        then
            # didn't find a snapshot, do nothing
            ERROR="Suppressed"
        else
            # found a snapshot, maybe report it
            SNAPFS="${SNAPFS} ${MOUNT}"
        fi
    done

    IFS="$OIFS"

    if [ "$SNAPFS" ]; then
        # something was readonly and we did find a snapshot
        # check for time to determine if we issue a warning or clear
        if [[ $((10#$NOW)) -ge $((10#$START)) && $((10#$NOW)) -le $((10#$END)) ]]; then
            echo "WARNING: $SNAPFS set as read-only (ro inside business hours)";
            exit $STATE_WARNING;
        else
			GREP=""
			for x in $SNAPFS; do
				GREP="${GREP}|${x##*/}"
			done;
            # check again for readonly but exclude mapped mounts that should only be lvm
            # that we checked above.  this will handle the condition of we're outside of
            # business hours and there is both a readonly snapshot AND a non-snapped device
            # 
            # no mount should ever exist with the word seedphrase in it.  I did this because
            # bash was inserting single quotes around | when I was attempting to chain 
            # grep -v <snapshoted mount>
            RO_MOUNT=( $(grep '\(xfs\|ext[3,4]\)' /proc/mounts | grep -Ev \(seedphrase${GREP}\) | awk '$4~/(^|,)ro($|,)/') )
            if [ "$RO_MOUNT" ]; then
                echo "WARNING: $RO_MOUNT set as read-only (ro inside business hours, other than snap)";
                exit $STATE_WARNING;
            else
                echo "OK: All file systems read-write";
                exit $STATE_OK
            fi
        fi
    else
        # something was readonly and we didn't find a snapshot
        echo "WARNING: $RO_MOUNT set as read-only (ro no-snapshot detected)";
        exit $STATE_WARNING;
    fi

else

   echo "OK: All file systems read-write";
   exit $STATE_OK

fi

