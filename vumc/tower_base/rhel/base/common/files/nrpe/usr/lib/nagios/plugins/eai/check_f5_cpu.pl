#!/usr/bin/perl -w
# This code is not supported by F5 Network and is offered under the GNU General Public License.  Use at your own risk.
 
use strict;
use Net::SNMP qw(:snmp);
use Switch;
 
my ($host, $snmp_comm, $warn, $crit, $error_msg);
 
if (! @ARGV) {
    print "Please enter the LTM host name or IP: ";
    chomp ($host = <STDIN>);
    print "\nPlease enter the LTM SNMP Community String: ";
    chomp ($snmp_comm = <STDIN>);
    print "\nPlease enter the warning threshold: ";
    chomp ($warn = <STDIN>);
    print "\nPlease enter the critical threshold: ";
    chomp ($crit = <STDIN>);
} else {
    my $usage = "f5_cpu.pl <host> <snmp community> <warn> <crit>";
 
    die "Usage: $usage\n" if $#ARGV != 3;
 
    $host = $ARGV[0];
    $snmp_comm = $ARGV[1];
    $warn = $ARGV[2];
    $crit = $ARGV[3];
    chomp ($host, $snmp_comm, $warn, $crit);
}

my $ltm_CPU_5S            = ".1.3.6.1.4.1.3375.2.1.1.2.20.13.0";
my $ltm_CPU_1M            = ".1.3.6.1.4.1.3375.2.1.1.2.20.29.0";
my $ltm_CPU_5M            = ".1.3.6.1.4.1.3375.2.1.1.2.20.37.0";
 
my ($session, $error) = Net::SNMP->session(
            -hostname       => $host,
            -community      => $snmp_comm,
            -port           => 161,
            -version        => 'snmpv2c',
            -nonblocking    => 0
            );
 
if (!defined $session) {
        $error_msg="Received no SNMP response from $host";
        print STDERR "Error: $error\n";
        }
    
#Get first instance
my $oids_1 = $session->get_request(
	  -varbindlist =>
	  [$ltm_CPU_5S,$ltm_CPU_1M,$ltm_CPU_5M] );

my $cpu5s=$oids_1->{$ltm_CPU_5S};
my $cpu1m=$oids_1->{$ltm_CPU_1M};
my $cpu5m=$oids_1->{$ltm_CPU_5M};

my $check_var=$cpu1m;

if($check_var < $warn){
	print "OK - $cpu5s, $cpu1m, $cpu5m | cpu5s=$cpu5s%;$warn%;$crit%;0;100% cpu1m=$cpu1m%;$warn%;$crit%;0;100% cpu5m=$cpu5m%;$warn%;$crit%;0;100% \n"; 
	exit(0);
}elsif($check_var >= $warn && $check_var < $crit){
	print "WARNING - $cpu5s, $cpu1m, $cpu5m | cpu5s=$cpu5s%;$warn%;$crit%;0;100% cpu1m=$cpu1m%;$warn%;$crit%;0;100% cpu5m=$cpu5m%;$warn%;$crit%;0;100% \n"; 
	exit(1);
}elsif($check_var >= $crit){
	print "CRITICAL - $cpu5s, $cpu1m, $cpu5m | cpu5s=$cpu5s%;$warn%;$crit%;0;100% cpu1m=$cpu1m%;$warn%;$crit%;0;100% cpu5m=$cpu5m%;$warn%;$crit%;0;100% \n"; 
	exit(2);
}else{
	print "UNKNOWN - $error_msg\n"; exit(3);
}

