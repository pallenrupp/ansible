#!/bin/sh

progname=`/bin/basename $0`
base_dir="/usr/lib/nagios/plugins"
screen_dir="/var/run/screen"

. $base_dir/utils.sh

print_usage() {
    echo "Usage: $progname user1,user2"
    echo "Usage: $progname --help"
}

print_help() {
    echo $progname
    echo "" 
    echo "A Nagios plugin to check for active screen sessions for particular users"
    echo "" 
    print_usage 
    echo "" 
    echo "Options:"
    echo " -h, --help"
    echo "    print detailed help screen"
    echo " user1,user2"
    echo "    a comma separated list of users to check"
    echo ""
}

# Make sure the correct number of command line arguments have been supplied

if [ $# -ne 1 ]; then
    echo "Exactly one command line argument is expected"
    print_help
    exit $STATE_UNKNOWN
else
    users=$1
fi


# Grab the command line arguments

exitstatus=$STATE_UNKNOWN #default
while test -n "$1"; do
    case "$1" in
        --help)
            print_help
            exit $STATE_OK
            ;;
        -h)
            print_help
            exit $STATE_OK
            ;;
    esac
    shift
done


sockets=""
for i in $(echo $users | tr "," "\n")
do
    if [ -d $screen_dir/S-$i ]; then

        for file in `sudo /bin/ls $screen_dir/S-$i`; do
            if [[ $file =~ "pts" ]]; then
                sockets="$sockets$i,"
            fi
        done

    fi
done

if [ -z $sockets ]; then
    echo "OK: no active screen sessions for $users"
    exit $STATE_OK
else
    sockets=$(echo ${sockets%,*})
    echo "WARNING: active screen sessions for $sockets"
    exit $STATE_WARNING
fi
