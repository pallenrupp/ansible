#!/bin/sh
#
# This is a wrapper plugin, which will call other hardware/vmware plugins. The
# purpose of having this plugin is to make service definitions as generic as
# possible. We can then call this plugin for every server in the 
# infrastructure via regex and feed the hardware type by host custom macro.


BASE_DIR="/usr/lib/nagios/plugins"
PLUGIN_DELL="$BASE_DIR/monitoringexchange.com/check_openmanage"
PLUGIN_DELL_CMD="$PLUGIN_DELL -s -b ctrl_driver=all"
PLUGIN_HP_PROLIANT="$BASE_DIR/labs.consol.de/check_hpasm"
PLUGIN_HP_PROLIANT_CMD="$BASE_DIR/labs.consol.de/check_hpasm -t 300"
PROGNAME=`/bin/basename $0`


. $BASE_DIR/utils.sh

print_usage() {
    echo "Usage: $PROGNAME -m manufacturer [-p product]"
    echo "Usage: $PROGNAME --help"
}           
            
print_help() {
    echo $PROGNAME
    echo ""
    echo "A generic wrapper around multiple hardware plugins"
    echo ""
    print_usage
    echo ""
    echo "Options:"
    echo " -h, --help"
    echo "    print detailed help screen"
    echo " -m, --manufacturer"
    echo "    hardware manufacturer"
    echo "    currently supported is 'Dell', 'HP', and 'VMware'"
    echo "    case insensitive"
    echo " -p, --product"
    echo "    hardware product"
    echo "    not required for VMware"
    echo "    currently supported is *poweredge* and *proliant* (wildcards)"
    echo "    case insensitive"
    echo ""
}


# Make sure the correct number of command line arguments have been supplied
            
if [ $# -lt 2 ]; then
    print_help
    exit $STATE_UNKNOWN
fi


# Grab the command line arguments

exitstatus=$STATE_UNKNOWN #default
while test -n "$1"; do
    case "$1" in
        --help)
            print_help
            exit $STATE_OK
            ;;
        -h)
            print_help
            exit $STATE_OK
            ;;
        --manufacturer)
            manufacturer=$2
            shift
            ;;
        -m)
            manufacturer=$2
            shift
            ;;
        --product)
            product=$2
            shift
            ;;
        -p)
            product=$2
            shift
            ;;
        *)
            echo "Unknown argument: $1"
            print_usage
            exit $STATE_UNKNOWN
            ;;
    esac
    shift
done

if [ -n $manufacturer ]; then
    manufacturer=$(echo $manufacturer | tr '[A-Z]' '[a-z]'])
    
    if [ -z $product ]; then
        if [ $manufacturer != "vmware" ]; then
            echo "VMware is the only Manufacturer where Product is not required; found: $manufacturer"
            print_usage
            exit $STATE_UNKNOWN
        fi
    else
        product=$(echo $product | tr '[A-Z]' '[a-z]'])
    fi
else
    echo "Manufacturer is a required value"
    print_usage
    exit $STATE_UNKNOWN
fi



case "$manufacturer" in

    "dell")
        case "$product" in
            *poweredge*)
                if [ -f $PLUGIN_DELL ]; then
                    output_text=`$PLUGIN_DELL_CMD`
                    output_code=$?
                else
                    echo "Dell plugin not found at $PLUGIN_DELL"
                    exit $STATE_UNKNOWN
                fi
                ;;
            *)
                echo "Unhandled Dell Product: $product'"
                exit $STATE_UNKNOWN
                ;;
        esac
        ;;

    "compaq"|"hp")     
        case "$product" in
            *proliant*)
                if [ -f $PLUGIN_HP_PROLIANT ]; then
                    output_text=`$PLUGIN_HP_PROLIANT_CMD`
                    output_code=$?
                else
                    echo "HP Proliant plugin not found at $PLUGIN_HP_PROLIANT"
                    exit $STATE_UNKNOWN
                fi
                ;;
            *)
                echo "Unhandled HP Product: $product'"
                exit $STATE_UNKNOWN
                ;;
        esac
        ;;
    "ibm")
        echo "meaningful checks not currently setup for IBM hardware"
        exit $STATE_OK
        ;;
    "vmware")
        $(ps -ef |grep -q -E '[v]mtoolsd|[v]mware-guestd')
        output_code=$?
        if [ $output_code -eq 0 ]; then
            output_text="OK - VMwareTools found"
        else
            output_text="WARNING - VMwareTools NOT found"
        fi
        ;;
    *)
        echo "Invalid value for Manufacturer. Expected 'Dell', 'HP', 'IBM' or 'VMware'; found: $manufacturer"
        exit $STATE_UNKNOWN
        ;;
esac

echo "$output_text"
exit $output_code

