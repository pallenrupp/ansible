#!/bin/bash
#
# Name:    check_linux_mem.sh
# Purpose: a Nagios plugin to check Linux memory/swap usage
# Author:  Kyle England
#
###########################################################################
# Global Variables

BASE_DIR="/usr/lib/nagios/plugins"
PROGNAME=`/bin/basename $0`
FREE_BIN=/usr/bin/free

# Include Nagios Utility Variables
. $BASE_DIR/utils.sh

###########################################################################
# Function: print_usage
# Purpose:  Short usage output
#
print_usage() {
    echo "Usage: $PROGNAME [-m] [-mf MEM_FREE_IN_MB] [-p] [-w WARN] [-c CRIT]"
    echo "Usage: $PROGNAME --help"
}

###########################################################################
# Function: print_help
# Purpose:  Extended usage output
#
print_help() {
    echo $PROGNAME
    echo ""
    echo "A plugin to check memory and swap usage on Linux"
    echo ""
    print_usage
    echo ""
    echo "Options:"
    echo " -h"
    echo "    print detailed help screen"
    echo " -m"
    echo "    The default is to check supplied thresholds against swap usage."
    echo "    The -m flag will check against memory usage instead"
    echo " -mf <MEM_IN_MB>"
    echo "    If checking swap usage, first check to see if any buffer/cache"
    echo "    memory is free. If an appropriate amount is free, do not alert"
    echo "    on swap usage, even if 100% is used. Specified in MB. Default is"
    echo "    128."
    echo " -p"
    echo "    Output perfdata"
    echo " -w <WARN>"
    echo "    The warning threshold percentage"
    echo " -c <CRIT>"
    echo "    The critical threshold percentage"
    echo ""
}

###########################################################################
# Main

exitstatus=$STATE_UNKNOWN #default
while test -n "$1"; do
    case "$1" in
        -h)
            print_help
            exit $STATE_OK
            ;;
        -m)
            check_memory=1
            ;;
        -mf)
            mem_free=$2
            shift
            ;;
        -p)
            perfdata=1
            ;;
        -w)
            warn=$2
            shift
            ;;
        -c)
            crit=$2
            shift
            ;;
        *)
            echo "Unknown argument: $1"
            print_usage
            exit $STATE_UNKNOWN
            ;;
    esac
    shift
done

if [[ -n $warn ]] && [[ -n $crit ]]; then
    if [[ $warn -ge $crit ]]; then
        echo "The warning threshold must be lower than the critical threshold"
        print_usage
        exit $STATE_UNKNOWN
    fi
fi

MEMINFO=`cat /proc/meminfo`

MEMTOTAL=`echo "${MEMINFO}" | grep ^MemTotal | grep -o '[0-9]\{1,\}'`
MEMFREE=`echo "${MEMINFO}" | grep ^MemFree | grep -o '[0-9]\{1,\}'`
BUFFERS=`echo "${MEMINFO}" | grep ^Buffers | grep -o '[0-9]\{1,\}'`
CACHE=`echo "${MEMINFO}" | grep ^Cached | grep -o '[0-9]\{1,\}'`
SWAPTOTAL=`echo "${MEMINFO}" | grep ^SwapTotal | grep -o '[0-9]\{1,\}'`
SWAPFREE=`echo "${MEMINFO}" | grep ^SwapFree | grep -o '[0-9]\{1,\}'`

MEM_FREE=$(( MEMFREE + BUFFERS + CACHE ))
MEM_USED=$(( MEMTOTAL - MEM_FREE ))
SWAP_USED=$(( SWAPTOTAL - SWAPFREE ))

MEM_TOTAL_GB=`echo $MEMTOTAL/1048576 |bc -l`
MEM_TOTAL_GB=`printf "%.2f\n" $MEM_TOTAL_GB`
MEM_FREE_MB=` echo $MEM_FREE/1024 |bc -l`
MEM_FREE_GB=` echo $MEM_FREE/1048576 |bc -l`
MEM_FREE_GB=`printf "%.2f\n" $MEM_FREE_GB`
MEM_USED_GB=` echo $MEM_USED/1048576 |bc -l`
MEM_USED_GB=`printf "%.2f\n" $MEM_USED_GB`
SWAPTOTAL_GB=` echo $SWAPTOTAL/1048576 |bc -l`
SWAPTOTAL_GB=`printf "%.2f\n" $SWAPTOTAL_GB`
SWAPUSED_GB=` echo $SWAP_USED/1048576 |bc -l`
SWAPUSED_GB=`printf "%.2f\n" $SWAPUSED_GB`
MEM_PERCENT=`echo $MEM_USED/$MEMTOTAL |bc -l`
MEM_PERCENT=`printf "%.2f\n" $MEM_PERCENT`
MEM_PERCENT=`echo $MEM_PERCENT*100 |bc`
MEM_PERCENT=`printf "%0.f\n" $MEM_PERCENT`
SWAP_PERCENT=`echo $SWAP_USED/$SWAPTOTAL |bc -l`
SWAP_PERCENT=`printf "%.2f\n" $SWAP_PERCENT`
SWAP_PERCENT=`echo $SWAP_PERCENT*100 |bc`
SWAP_PERCENT=`printf "%0.f\n" $SWAP_PERCENT`

if [[ $perfdata -eq 1 ]]; then
    PERF_OUT="|PHYSICAL="$MEM_USED_GB"GB;0.00;$MEM_TOTAL_GB;$MEM_TOTAL_GB;$MEM_TOTAL_GB SWAP="$SWAPUSED_GB"GB;0.00;$SWAPTOTAL_GB;$SWAPTOTAL_GB;$SWAPTOTAL_GB"
fi

if [[ $check_memory -ne 1 ]]; then
   
    PERCENT_LABEL="SWAP" 
    if [ ! $mem_free ]; then
        # check swap, regardless of physical memory free
        PERCENT=$SWAP_PERCENT
        INFO="total: "$SWAPTOTAL_GB"GB - used: "$SWAPUSED_GB"GB ($SWAP_PERCENT%)"
    else
        # only check swap if only a certain amount of physical memory is free
        if [[ $MEM_FREE_MB -gt $mem_free ]]; then
            INFO="at least "$mem_free"MB of Physical RAM free"
            echo "$PERCENT_LABEL NOT CHECKED - $INFO$PERF_OUT"
            exit $STATE_OK
        else
            PERCENT=$SWAP_PERCENT
            INFO="total: "$SWAPTOTAL_GB"GB - used: "$SWAPUSED_GB"GB ($SWAP_PERCENT%)"
        fi
    fi
else
    if [ $mem_free ]; then
        echo "-m and -mf should not be used together"
        print_usage
        exit $STATE_UNKNOWN
    else
        PERCENT_LABEL="RAM"
        PERCENT=$MEM_PERCENT
        INFO="total: "$MEM_TOTAL_GB"GB - used: "$MEM_USED_GB"GB ($MEM_PERCENT%)"
    fi
fi

if [[ -n $warn ]] && [[ -n $crit ]]; then
    if [[ $PERCENT -lt $warn ]]; then
        echo "$PERCENT_LABEL OK - $INFO$PERF_OUT"
        exit $STATE_OK
    elif [[ $PERCENT -lt $crit ]]; then
        echo "$PERCENT_LABEL WARNING - $INFO$PERF_OUT"
        exit $STATE_WARNING
    else
        echo "$PERCENT_LABEL CRITICAL - $INFO$PERF_OUT"
        exit $STATE_CRITICAL
    fi
elif [[ -n $warn ]] && [[ -z $crit ]]; then
    if [[ $PERCENT -lt $warn ]]; then
        echo "$PERCENT_LABEL OK - $INFO$PERF_OUT"
        exit $STATE_OK
    else
        echo "$PERCENT_LABEL WARNING - $INFO$PERF_OUT"
        exit $STATE_WARNING
    fi
elif [[ -z $warn ]] && [[ -n $crit ]]; then
    if [[ $PERCENT -lt $crit ]]; then
        echo "$PERCENT_LABEL OK - $INFO$PERF_OUT"
        exit $STATE_OK
    else
        echo "$PERCENT_LABEL CRITICAL - $INFO$PERF_OUT"
        exit $STATE_CRITICAL
    fi
else
    echo "$PERCENT_LABEL OK - $INFO$PERF_OUT"
    exit $STATE_OK
fi

###########################################################################

