AddressFamily inet
Port 22

{% if ansible_local.ssh.epic is defined %}
# Additional port for SFTP
Port 2022

{% endif %}
UseDNS = yes

{% if ansible_local.ssh.loglevel|default(False) %}
LogLevel {{ ansible_local.ssh.loglevel }}
{% endif %}
SyslogFacility AUTHPRIV

#Cipher Hardening
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs umac-64-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,hmac-sha1
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc

#Key Locations
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

#Auth Stuff
PermitRootLogin without-password
PubkeyAuthentication yes
PermitEmptyPasswords no
UsePAM yes
KerberosAuthentication no
ChallengeResponseAuthentication no
IgnoreRhosts yes
HostbasedAuthentication no
#RhostsRSAAuthentication no
#RSAauthentication yes
PasswordAuthentication yes
#UseLogin no

# send a message to client every 300 seconds
ClientAliveInterval 300
# disconnect after 300*3 seconds, or 15 mins of no communication w client
ClientAliveCountMax 3
# This is different than ClientAlive settings. TCPKeepAlive is spoofable.
TCPKeepAlive no

{% if ansible_local.ssh.x11forwarding | default('false',true) == 'true' %}
#added for x11 forwarding
X11Forwarding yes

{% endif %}
Banner /etc/issue.net

{% if ansible_local.ssh.epic is defined %}
# Set SFTP subsystem to internal-sftp
Subsystem sftp internal-sftp
{% else %}
Subsystem   sftp    /usr/libexec/openssh/sftp-server
{% endif %}

AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

AuthorizedKeysFile  .ssh/authorized_keys
PrintMotd no

{% if ansible_local.ssh.epic is defined %}{% set env = ansible_local.ssh.epic %}
## Begin epic specific Configurations for restricted menus

# Force Epic TS Menu
Match LocalPort 22 Group sg-epc_menu_{{ env }}_ts
   ForceCommand /epic/bin/epicmenu.ts
   LogLevel VERBOSE

# Force Epic TS Menu for After Hours Support User
Match LocalPort 22 User epictxt
   ForceCommand /epic/bin/epicmenu.ts
   LogLevel VERBOSE

# Force Epic IS Menu
Match LocalPort 22 Group sg-epc_menu_{{ env }}_is
   ForceCommand /epic/bin/epicmenu.is
   LogLevel VERBOSE

# Force Epic Lookitt Menu
Match LocalPort 22 Group sg-epc_menu_{{ env }}_lookitt
   ForceCommand /epic/bin/epicmenu
   LogLevel VERBOSE

# Force Epic App Menu
Match LocalPort 22 Group !sg-epc_odba_adm,sg-epc_{{ env }}_usr
   ForceCommand /epic/bin/epicmenu.app
   LogLevel VERBOSE

# Force SFTP on port 2022
Match LocalPort 2022 Group sg-epc_{{ env }}_usr
   ForceCommand internal-sftp -f AUTHPRIV -l VERBOSE -u 0022
   LogLevel VERBOSE

# Catch-all for users who did not match any of the above groups
Match Group *,!sg-epc_menu_{{ env }}_ssts,!sg-epc_odba_adm,!sg-sys_adm,!dmngrp,!epicuser,!iscagent,!rdkapp,!root
  ForceCommand exit 1
## End Epic specific configurations for restricted menus

{% endif %}
# prevent vec admin from using passwords that are created in tower for ssh authentication
Match User vecadmin
    PasswordAuthentication no
