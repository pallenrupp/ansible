---
  - block:
    - name: Upload a file as an iFile
      bigip_file_copy:
        provider: "{{provider}}"
        name: "{{item.path}}"
        source: "files/ifiles/{{item.path}}"
        datastore: ifile
      with_filetree: 'files/ifiles/'
      when: item.state == 'file'
      delegate_to: localhost
    rescue:
    # the bigip_file_copy module uses the api to upload and it doesn't have an
    # update concept, so if the file has change it tries to delete it and recreate
    # we watch for that failure, then copy the updated file to /tmp and run
    # the tmsh command to update the existing file locally.
    - name: Create temp directory to hold iFiles
      file:
        path: /shared/tmp/ifiles/
        state: directory
    - name: Copy ifile
      copy:
        src: '{{ item.src }}'
        dest: '/shared/tmp/ifiles/{{ item.path }}'
      loop: '{{ ansible_failed_result|json_query("results[?failed==`true`].item") }}'
    - name: Update external iFile
      bigip_command:
        provider: "{{ provider }}"
        commands:
          - "tmsh modify sys file ifile {{ item.path }} source-path file://localhost/shared/tmp/ifiles/{{ item.path }}"
        warn: no
      loop: '{{ ansible_failed_result|json_query("results[?failed==`true`].item") }}'

  # There is a pull request to add this functionality to the api and other modules
  # F5 came up short sighted again so we'll handle it on the command line. Create
  # the external file link under local traffic -> irules -> ifile list by stripping
  # the extension off the file name
  - name: Create external file links 
    bigip_command:
      provider: "{{ provider }}"
      commands:
        - "create ltm ifile {{ item.path }} { file-name /Common/{{item.path}} } "
      warn: no
    register: results
    changed_when: results.stdout_lines|length == 0
    with_filetree: 'files/ifiles/'
    when: item.state == 'file'

  - block:
    # This task will fail if the file doesn't exist.
    - name: Verify data group exists 
      bigip_data_group:
        provider: "{{ provider }}"
        name: "{{ item.path.split('.')[:-1]|join(' ') }}"
        internal: no
        state: present
        type: string
        separator: ":="
      with_filetree: 'files/ifiles/'
      when: item.state == 'file'
    rescue:
    # If the above task failed the file didn't exist so we'll create it using
    # the values below.  If the file did exist, using these values would clear
    # the contents of the file instead of adding the one record
    - name: Create data group 
      bigip_data_group:
        provider: "{{ provider }}"
        name: "{{ item.path.split('.')[:-1]|join(' ') }}"
        internal: no
        state: present
        type: string
        separator: ":="
        records:
          - key: '"/patient_care/example"'
            value: '"This can say whatever I want"'
      loop: '{{ ansible_failed_result|json_query("results[?failed==`true`].item") }}'

  # Copy any irules up to the device how they are named on disk under files/irules/
  - name: Add iRule
    bigip_irule:
      provider: "{{provider}}"
      module: "ltm"
      name: "{{item.path}}"
      content: "{{lookup('file','{{item.src}}')}}"
    with_filetree: 'files/irules/'
    when: item.state == 'file'
