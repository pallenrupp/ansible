---
  - name: Set AD authentication details
    bigip_device_auth_ldap:
      provider: "{{provider}}"
      bind_dn: "CN=hslinux,CN=users,DC=ds,DC=vanderbilt,DC=edu"
      remote_directory_tree: "CN=users,DC=ds,DC=vanderbilt,DC=edu"
      login_ldap_attr: 'samaccountname'
      servers:
        - ds.vanderbilt.edu
      port: '389'
      ssl: 'start-tls'
      validate_certs: false
      scope: 'one'
      check_member_attr: true

  # password triggers an update everytime
  - name: Set AD auth bind password
    bigip_device_auth_ldap:
      provider: "{{provider}}"
      bind_password: "{{ svc_password }}"
    changed_when: false

  - name: Set auth type to AD
    bigip_command:
      commands:
        - modify auth source type active-directory
      provider: "{{ provider }}"
      warn: no
    changed_when: false

  - name: Lower ldap timeout
    bigip_command:
      commands:
        - modify auth ldap system-auth idle-timeout 295
      provider: "{{provider}}"
      warn: no
    changed_when: false

  - name: Set syslog to splunk
    bigip_remote_syslog:
      remote_host: splunkhf-syslog.service.vumc.org
      remote_port: '514'
      provider: "{{provider}}"

  - name: Configure ntp servers
    bigip_device_ntp:
      ntp_servers:
        - 1.ntp.vumc.io
        - 2.ntp.vumc.io
        - 3.ntp.vumc.io
        - 4.ntp.vumc.io
      provider: "{{provider}}"

  - name: Configure dns servers
    bigip_device_dns:
      name_servers:
        - 10.108.51.18
        - 10.101.62.234
      provider: "{{provider}}"

  - name: Configure ssh allow list
    bigip_device_sshd:
      allow : all
      provider: "{{provider}}"

  - name: Configure snmp options
    bigip_snmp:
      contact: "MCIT LINUX <vumc.it.linux@vumc.org>"
      provider: "{{provider}}"
      allowed_addresses:
        - 127.0.0.0/8
        - 10.152.28.168
        - 10.152.28.200
        - 10.152.28.201
        - 10.152.28.202
        - 10.109.24.74
        - 10.2.240.144
        - 10.109.16.73
        - 10.109.16.74
        - 10.109.16.75
        - 10.154.28.200
        - 10.101.48.38

  # oid won't honor the omit keyword so i'm using tmsh instead
  - name: Use tmsh to create specific snmp communities that the module failed on
    bigip_command:
      commands:
        - modify /sys snmp communities add { comm-public { community-name public source default } }
        - modify /sys snmp communities modify { comm-public { community-name public source default } }
        - modify /sys snmp communities add { emcsrm { community-name emcsrm } }
        - modify /sys snmp communities modify { emcsrm { community-name emcsrm } }
      provider: "{{ provider }}"
      warn: no
    changed_when: false

  - name: Create SMNP v2c read-only communities
    bigip_snmp_community:
      name: "{{ item.name }}"
      community: "{{ item.name }}"
      version: v2c
      source: "{{ item.source|default('default') }}"
      oid: "{{ item.oid|default('') }}"
      access: ro
      provider: "{{provider}}"
    loop:
      - { name: 'nagios', oid: '.1.3.6.1.4.1.3375.2.1' }

  - name: Set smtp to use relay
    bigip_smtp:
      provider: "{{provider}}"
      name: relay-trusted.service.vumc.org
      smtp_server: relay-trusted.service.vumc.org
      local_host_name: "{{ inventory_hostname|regex_replace('([tp])[ab]','\\1') }}"
      from_address: "no-reply@{{ inventory_hostname|regex_replace('([tp])[ab]','\\1') }}"
      state: present
    when: inventory_hostname_short is not regex("gtm.*")

  - name: Set smtp to use relay
    bigip_smtp:
      provider: "{{provider}}"
      name: relay-trusted.service.vumc.org
      smtp_server: relay-trusted.service.vumc.org
      local_host_name: "{{ inventory_hostname|regex_replace('(gtm.*[0-9]{2,3})[0-9]([tp].*)','\\g<1>0\\g<2>') }}"
      from_address: "no-reply@{{ inventory_hostname|regex_replace('(gtm.*[0-9]{2,3})[0-9]([tp].*)','\\g<1>0\\g<2>') }}"
      state: present
    when: inventory_hostname_short is regex("gtm.*")

  # K13180: Configuring the BIG-IP system to deliver locally generated email messages (11.x - 15.x)
  # https://support.f5.com/csp/article/K13180
  - name: Set local smtp relay
    bigip_command:
      commands:
        - modify /sys outbound-smtp mailhub relay-trusted.service.vumc.org:25
      provider: "{{ provider }}"
      warn: no
    changed_when: false

  - name: Collect all BIG-IP information
    bigip_device_info:
      gather_subset:
        - partitions
      provider: "{{provider}}"
    register: facts

  - include_tasks: remote-role-groups-ltm.yml
    when: inventory_hostname_short is regex("ltm.*")
  - include_tasks: remote-role-groups-gtm.yml
    when: inventory_hostname_short is regex("gtm.*")
  - include_tasks: remote-role-groups-vpc.yml
    when: inventory_hostname_short is regex("vpc.*")
