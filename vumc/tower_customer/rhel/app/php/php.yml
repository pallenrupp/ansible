# Use this to install php in a standard way with the most common modules that
# we get requested (gd,mbstring,mysqlnd,pdo,ldap,odbc).  The newer versions
# from software collections will also install the php-fpm components
#
#
#   Optional Parameters:
#
#   - version :            This will default to whatever version "php" is
#                          to the system to use a different version specify
#                          "5.4", "5.6", "7.2" etc
#   - additional_packages: Use this to add other packages during the php
#                          install to avoid having to have another software
#                          installation task
#
#
#   USAGE EXAMPLES
#
#   Simple
#
#    - include_tasks: ../../app/php/php.yml
#
#   Specific Version
#
#    - include_tasks: ../../app/php/php.yml
#      vars:
#        version: "7.2"
#
#   Multiple Versions
#
#    - include_tasks: ../../app/php/php.yml
#      vars:
#        version: ["7.2","7.3"]
#
#   Additional Packages
#
#    - include_tasks: ../../app/php/php.yml
#      vars:
#        additional_packages: ['phpMyAdmin','php-pecl-yaml']
#
#   Specific Version and Additional Packages
#
#    - include_tasks: ../../app/php/php.yml
#      vars:
#        version: "7.2"
#        additional_packages: ['rh-php72-php-soap','rh-php72-php-zip']
#
#
---
# BEGIN - Default block for php
  - block:
    - name: Add standard php packages
      set_fact:
        php_packages: "{{ php_packages|default([]) + p_item.packages }}"
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { packages: ['php','php-gd','php-mbstring','php-ldap','php-odbc','php-pdo'] }
      - { packages: ['php-mysql'], cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { packages: ['php-mysqlnd'], cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/php.d'
      - '/etc/php-fpm.d'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item.file ] }}'
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { file: '/etc/php.ini' }
      - { file: '/etc/php-fpm.conf', cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["php-fpm"]
      when: ansible_distribution_major_version is version('8', '>=')
    when: '"0" in query("items", version|default(["0"]) ) or "8.0" in query("items", version|default(["0"]) )'
# END - Defaul block for apache

# BEGIN - Block for php 5.4 from software collections
  - block:
    - name: Install php 5.4 packages from software collections
      set_fact:
        php_packages: "{{ php_packages|default([]) + ['php54','php54-php-gd','php54-php-mbstring','php54-php-mysqlnd','php54-php-pdo','php54-php-ldap','php54-php-odbc'] }}"
        enable_rhscl: true
        cacheable: no

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/opt/rh/php54/root/etc/php.d'
      - '/opt/rh/php54/root/etc/php-fpm.d'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/opt/rh/php54/root/etc/php.ini'
      - '/opt/rh/php54/root/etc/php-fpm.conf'

    when: '"5.4" in query("items", version|default([]) )'
# END - Block for php 5.4 from software collections

# BEGIN - Block for php 5.5 from software collections
  - block:
    - name: Install php 5.5 packages from software collections
      set_fact:
        php_packages: "{{ php_packages|default([]) + ['php55','php55-php-gd','php55-php-mbstring','php55-php-mysqlnd','php55-php-pdo','php55-php-ldap','php55-php-odbc','php55-php-fpm'] }}"
        enable_rhscl: true
        cacheable: no

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/opt/rh/php55/root/etc/php.d'
      - '/opt/rh/php55/root/etc/php-fpm.d'
      - '/opt/rh/php55/root/usr/lib64/php/modules'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/opt/rh/php55/root/etc/php.ini'
      - '/opt/rh/php55/root/etc/php-fpm.conf'

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["php55-php-fpm"]

    when: '"5.5" in query("items", version|default([]) )'
# END - Block for php 5.5 from software collections

# BEGIN - Block for php 5.6 from software collections
  - block:
    - name: Install php 5.6 packages from software collections
      set_fact:
        php_packages: "{{ php_packages|default([]) + ['rh-php56','rh-php56-php-gd','rh-php56-php-mbstring','rh-php56-php-mysqlnd','rh-php56-php-pdo','rh-php56-php-ldap','rh-php56-php-odbc','rh-php56-php-fpm'] }}"
        enable_rhscl: true
        cacheable: no

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php56/php.d'
      - '/etc/opt/rh/rh-php56/php-fpm.d'
      - '/opt/rh/rh-php56/root/usr/lib64/php/modules'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php56/php.ini'
      - '/etc/opt/rh/rh-php56/php-fpm.conf'

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["rh-php56-php-fpm"]

    when: '"5.6" in query("items", version|default([]) )'
# END - Block for php 5.6 from software collections

# BEGIN - Block for php 7.0 from software collections
  - block:
    - name: Install php 7.0 packages from software collections
      set_fact:
        php_packages: "{{ php_packages|default([]) + ['rh-php70','rh-php70-php-gd','rh-php70-php-mbstring','rh-php70-php-mysqlnd','rh-php70-php-pdo','rh-php70-php-ldap','rh-php70-php-odbc', 'rh-php70-php-fpm'] }}"
        enable_rhscl: true
        cacheable: no

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php70/php.d'
      - '/etc/opt/rh/rh-php70/php-fpm.d'
      - '/opt/rh/rh-php70/root/usr/lib64/php/modules'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php70/php.ini'
      - '/etc/opt/rh/rh-php70/php-fpm.conf'

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["rh-php70-php-fpm"]

    when: '"7.0" in query("items", version|default([]) )'
# END - Block for php 7.0 from software collections

# BEGIN - Block for php 7.1 from software collections
  - block:
    - name: Install php 7.1 packages from software collections
      set_fact:
        php_packages: "{{ php_packages|default([]) + ['rh-php71','rh-php71-php-gd','rh-php71-php-mbstring','rh-php71-php-mysqlnd','rh-php71-php-pdo','rh-php71-php-ldap','rh-php71-php-odbc','rh-php71-php-fpm'] }}"
        enable_rhscl: true
        cacheable: no

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php71/php.d'
      - '/etc/opt/rh/rh-php71/php-fpm.d'
      - '/opt/rh/rh-php71/root/usr/lib64/php/modules'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php71/php.ini'
      - '/etc/opt/rh/rh-php71/php-fpm.conf'

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["rh-php71-php-fpm"]

    when: '"7.1" in query("items", version|default([]) )'
# END - Block for php 7.1 from software collections

# BEGIN - Block for php 7.2 from software collections
  - block:
    - name: Install php 7.2 packages from software collections
      set_fact:
        php_packages: "{{ php_packages|default([]) + ['rh-php72','rh-php72-php-gd','rh-php72-php-mbstring','rh-php72-php-mysqlnd','rh-php72-php-pdo','rh-php72-php-ldap','rh-php72-php-odbc','rh-php72-php-fpm'] }}"
        enable_rhscl: true
        cacheable: no

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php72/php.d'
      - '/etc/opt/rh/rh-php72/php-fpm.d'
      - '/opt/rh/rh-php72/root/usr/lib64/php/modules'

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item ] }}'
        cacheable: no
      loop_control:
        loop_var: p_item
      loop:
      - '/etc/opt/rh/rh-php72/php.ini'
      - '/etc/opt/rh/rh-php72/php-fpm.conf'

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["rh-php72-php-fpm"]

    when: '"7.2" in query("items", version|default([]) )'
# END - Block for php 7.2 from software collections

# BEGIN - Block for php 7.3 from software collections
  - block:
    - name: Add php 7.3 packages
      set_fact:
        php_packages: "{{ php_packages|default([]) + p_item.packages }}"
        enable_rhscl: true
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { packages: ['rh-php73','rh-php73-php-gd','rh-php73-php-mbstring','rh-php73-php-mysqlnd','rh-php73-php-pdo','rh-php73-php-ldap','rh-php73-php-odbc','rh-php73-php-fpm'], cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { packages: ['@php:7.3/common','php-gd','php-mbstring','php-ldap','php-mysqlnd','php-odbc','php-pdo'], cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item.dir ] }}'
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { dir: '/etc/opt/rh/rh-php73/php.d',                  cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { dir: '/etc/opt/rh/rh-php73/php-fpm.d',              cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { dir: '/opt/rh/rh-php73/root/usr/lib64/php/modules', cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { dir: '/etc/php.d',                                  cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }
      - { dir: '/etc/php-fpm.d',                              cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item.file ] }}'
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { file: '/etc/opt/rh/rh-php73/php.ini',      cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { file: '/etc/opt/rh/rh-php73/php-fpm.conf', cond: "{{ ansible_distribution_major_version is version('7', '<=') }}" }
      - { file: '/etc/php.ini',                      cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }
      - { file: '/etc/php-fpm.conf',                 cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["rh-php73-php-fpm"]
      when: ansible_distribution_major_version is version('7', '<=')

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["php-fpm"]
      when: ansible_distribution_major_version is version('8', '>=')

    when: '"7.3" in query("items", version|default([]) )'
# END - Block for php 7.3 from software collections

# BEGIN - Block for php 8.0 from appstreams
  - block:
    - name: Add php 8.0 packages
      set_fact:
        php_packages: "{{ php_packages|default([]) + p_item.packages }}"
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { packages: ['@php:8.0/common','php-gd','php-mbstring','php-ldap','php-mysqlnd','php-odbc','php-pdo'], cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - name: Search directories for php permissions
      set_fact:
        php_specific_directories: '{{ php_specific_directories|default([]) + [ p_item.dir ] }}'
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { dir: '/etc/php.d',                                  cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }
      - { dir: '/etc/php-fpm.d',                              cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - name: Specific file permissions for php
      set_fact:
        php_specific_files: '{{ php_specific_files|default([])+ [ p_item.file ] }}'
        cacheable: no
      when: p_item.cond|default(true)
      loop_control:
        loop_var: p_item
      loop:
      - { file: '/etc/php.ini',                      cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }
      - { file: '/etc/php-fpm.conf',                 cond: "{{ ansible_distribution_major_version is version('8', '>=') }}" }

    - include_tasks: ../../common/sudoers.yml
      vars:
        service: ["php-fpm"]
      when: ansible_distribution_major_version is version('8', '>=')

    when: '"8.0" in query("items", version|default([]) )'
# END - Block for php 8.0 from appstreams

#
# Everything below here is common to all apache versions
#
  - name: Enable Red Hat Software Collections repo
    rhsm_repository:
      name: rhel-server-rhscl-*-rpms
      state: enabled
    when: (rhel6 or rhel7) and enable_rhscl|default(false)

  - name: Install php packages
    package:
      name: "{{ php_packages|default([]) + additional_packages|default([]) }}"
      state: present
      lock_timeout: 180 # wait up to 3 minutes for a lock ansible/ansible#57189

  - name: Directory permissions for php
    file:
      dest: '{{ p_item }}'
      state: directory
      owner: root
      group: '{{ group }}'
      mode: '0775'
    loop_control:
      loop_var: p_item
    loop: '{{ php_specific_directories|default([]) }}'

  - include_tasks: ../../common/seboolean.yml
    vars:
      sename: domain_kernel_load_modules
    when: ansible_selinux.status == 'enabled'

#
# We'll put any paths here that might have files that will need permissions regulated
# despite the version.  If the path doesn't exist it just skips it and doesn't throw an
# error
#
  - name: Finding PHP configuration files
    find:
      paths: '{{ php_specific_directories|default([]) }}'
      recurse: "yes"
    register: find_result

  - name: Setting file permissions for php
    file:
      path: '{{ p_item }}'
      owner: root
      group: "{{ group }}"
      mode: 0664
    loop_control:
      loop_var: p_item
    loop: "{{ find_result.files|json_query('[?mode!=`0664`].path') + php_specific_files|default([]) }}"
