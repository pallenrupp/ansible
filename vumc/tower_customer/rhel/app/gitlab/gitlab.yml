---
  - block:
    # https://docs.gitlab.com/omnibus/update/package_signatures.html
    - name: Check for Gitlab pubkey
      shell: /bin/rpm --quiet -q gpg-pubkey-f27eab47-60d4a67e
      args:
        warn: no
      check_mode: no
      register: pubkey
      changed_when: false
      failed_when: pubkey.rc > 0
    rescue:
      - name: Copy Gitlab GPG key
        copy:
          src: 'files/etc/pki/rpm-gpg/gitlab_gpg.key'
          dest: '/etc/pki/rpm-gpg/gitlab_gpg.key'
          mode: '0400'
          owner: root
          group: root
      - name: Install Gitlab GPG key
        shell: /bin/rpm --import /etc/pki/rpm-gpg/gitlab_gpg.key
        args:
          warn: no

  - block:
    # https://docs.gitlab.com/runner/install/linux-repository.html#current-gpg-public-key
    - name: Check for Gitlab Runner pubkey
      shell: /bin/rpm --quiet -q gpg-pubkey-35dfa027-60ba0235
      args:
        warn: no
      check_mode: no
      register: runner
      changed_when: false
      failed_when: runner.rc > 0
    rescue:
      - name: Copy Gitlab Runner GPG key
        copy:
          src: 'files/etc/pki/rpm-gpg/gitlab_runner_gpg.key'
          dest: '/etc/pki/rpm-gpg/gitlab_runner_gpg.key'
          mode: '0400'
          owner: root
          group: root
      - name: Install Gitlab Runner GPG key
        shell: /bin/rpm --import /etc/pki/rpm-gpg/gitlab_runner_gpg.key
        args:
          warn: no

  - name: Set list of packages to install for gitlab
    set_fact:
      cacheable: no
      gitlab_packages:
        ['awscli','git','python2-pip','screen','unzip','zip']

  # RHEL7 specific git requirements
  - block:
    - name: Enable Red Hat Software Collections repo
      rhsm_repository:
        name: rhel-server-rhscl-7-rpms
        state: enabled

    - name: Add git from software collections to list of packages to install
      set_fact:
        cacheable: no
        gitlab_packages: "{{ gitlab_packages + ['rh-git29-git'] }}"

    - name: Copy Role files
      copy:
        src: files/etc/profile.d/enablerh-git29.sh
        dest: /etc/profile.d/enablerh-git29.sh
        mode: 0644
        owner: root
        group: root
    when: rhel7

  - name: Install gitlab required packages
    package:
      name: "{{ gitlab_packages }}"
      state: present
      lock_timeout: 180 # wait up to 3 minutes for a lock ansible/ansible#57189
