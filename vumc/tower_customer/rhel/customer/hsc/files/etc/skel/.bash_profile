# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH
hsc_dev=`id -nG | grep sg-hsc_dev_adm`
if [ -n "$hsc_dev" ]; then
    exec sudo -u hscapp /app001/scripts/menu.sh
fi