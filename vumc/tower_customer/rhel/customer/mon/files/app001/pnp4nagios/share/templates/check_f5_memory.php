<?php

$opt[1] = "--vertical-label \"MB\" --alt-autoscale --alt-y-grid -r --title \"Memory Usage for $hostname / $servicedesc\" ";

$def[1] =  "DEF:TMM_Total=$rrdfile:$DS[1]:AVERAGE " ;
$def[1] .=  "DEF:TMM_Used=$rrdfile:$DS[2]:AVERAGE " ;

$def[1] .= "HRULE:$WARN[1]#ffff00:\"Warning $WARN[1]MB\" ";
$def[1] .= "HRULE:$CRIT[1]#ff0000:\"Critical $CRIT[1]MB\\n\" ";

$def[1] .= "COMMENT:\"\\t\\t\\tLAST\\t\\t\\tAVERAGE\\t\\tMAX\\n\" " ;

$def[1] .= "AREA:TMM_Used#E8630C:\"TMM_Used\\t\" " ;
$def[1] .= "GPRINT:TMM_Used:LAST:\"%6.2lf MB\\t\\t\" " ;
$def[1] .= "GPRINT:TMM_Used:AVERAGE:\"%6.2lf \\t\\t\" " ;
$def[1] .= "GPRINT:TMM_Used:MAX:\"%6.2lf \\n\" " ;

?>
