<?php

# $Id: check_nrpe_cpu.php,v 1.1 2011/06/01 19:25:05 englank Exp $
# managed by Cfengine

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#   PNP Template for check_cpu.sh
#   Author: Mike Adolphs (http://www.matejunkie.com/

$_WARNRULE = '#FFFF00';
$_CRITRULE = '#FF0000';

$opt[1] = "--vertical-label \"Memory [G]\" -l 0 -r --title \"Hugepage Usage for $hostname / $servicedesc\" ";

$def[1] =  "DEF:mem_dhp_used=$rrdfile:$DS[2]:AVERAGE " ;
$def[1] .=  "DEF:mem_shp_free=$rrdfile:$DS[4]:AVERAGE " ;
$def[1] .=  "DEF:mem_shp_used=$rrdfile:$DS[3]:AVERAGE " ;

$def[1] .= "COMMENT:\"\\t\\t\\t\\t\\tLAST\\t\\t\\tAVERAGE\\t\\t\\tMAX\\n\" " ;

$def[1] .= "AREA:mem_dhp_used#E80C3E:\"Dynamic Hugepage Used\\t\":STACK " ; 
$def[1] .= "GPRINT:mem_dhp_used:LAST:\"%6.2lf G\\t\\t\" " ;
$def[1] .= "GPRINT:mem_dhp_used:AVERAGE:\"%6.2lf \\t\\t\\t\" " ;
$def[1] .= "GPRINT:mem_dhp_used:MAX:\"%6.2lf \\n\" " ;

$def[1] .= "AREA:mem_shp_used#E8630C:\"Static Hugepage Used\\t\":STACK " ;
$def[1] .= "GPRINT:mem_shp_used:LAST:\"%6.2lf G\\t\\t\" " ;
$def[1] .= "GPRINT:mem_shp_used:AVERAGE:\"%6.2lf \\t\\t\\t\" " ;
$def[1] .= "GPRINT:mem_shp_used:MAX:\"%6.2lf \\n\" " ;

$def[1] .= "AREA:mem_shp_free#008000:\"Static Hugepage Free\\t\":STACK " ; 
$def[1] .= "GPRINT:mem_shp_free:LAST:\"%6.2lf G\\t\\t\" " ;
$def[1] .= "GPRINT:mem_shp_free:AVERAGE:\"%6.2lf \\t\\t\\t\" " ;
$def[1] .= "GPRINT:mem_shp_free:MAX:\"%6.2lf \\n\" " ;

$def[1] .= "HRULE:".$WARN[3].$_WARNRULE.':"" ';
$def[1] .= "HRULE:".$CRIT[3].$_CRITRULE.':"" ';
?>
