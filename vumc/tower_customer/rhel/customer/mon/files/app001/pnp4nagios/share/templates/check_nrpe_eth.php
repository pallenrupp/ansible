<?php

$_WARNRULE = '#FFFF00';
$_CRITRULE = '#FF0000';
$lower = $MAX[2] * -1;

if (!function_exists('formatBytes')) {
    function formatBytes($bytes, $precision = 2) { 
        $units = array('b', 'Kb', 'Mb', 'Gb', 'Tb'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1000)); 
        $pow = min($pow, count($units) - 1); 

        $bytes /= pow(1000, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow]; 
    }
}

$opt[1] = "--vertical-label \"bytes/sec\" --units=si --slope-mode --alt-autoscale --alt-y-grid -r --title \"$hostname / Network Throughput for $servicedesc\" ";

$def[1] =  "DEF:in=$rrdfile:$DS[1]:AVERAGE " ;
$def[1] .= "DEF:out=$rrdfile:$DS[2]:AVERAGE " ;
$def[1] .= "CDEF:outi=out,-1,* " ;
$def[1] .= "CDEF:inkb=in,1000,/ ";
$def[1] .= "CDEF:outkb=out,1000,/ ";

$def[1] .= rrd::gradient('in','f0f0f0','0000a0','in',20);
$def[1] .= "GPRINT:inkb:LAST:\"%3.0lf Kb LAST \" ";
$def[1] .= "GPRINT:inkb:MAX:\"%3.0lf Kb MAX \" ";
$def[1] .= "GPRINT:inkb" . ':AVERAGE:"%3.0lf Kb AVERAGE \j" ';

$def[1] .= rrd::gradient('outi','ffff42','ee7318','out',20);
$def[1] .= "GPRINT:outkb:LAST:\"%3.0lf Kb LAST \" ";
$def[1] .= "GPRINT:outkb:MAX:\"%3.0lf Kb MAX \" ";
$def[1] .= "GPRINT:outkb" . ':AVERAGE:"%3.0lf Kb AVERAGE \j" ';

$def[1] .= "COMMENT:\"  \\l\" " ;
$def[1] .= "LINE1:0#ffffff " ;
$def[1] .= "HRULE:".$WARN[1].$_WARNRULE.':"Inbound Warning on  '.formatBytes($WARN[1]).'" ' ;
$def[1] .= "HRULE:".($WARN[2] * -1).$_WARNRULE.':"Outbound Warning on  '.formatBytes($WARN[2]).'\j" ';
$def[1] .= "HRULE:".$CRIT[1].$_CRITRULE.':"Inbound Critical on '.formatBytes($CRIT[1]).'" ';
$def[1] .= "HRULE:".($CRIT[2] * -1).$_CRITRULE.':"Outbound Critical on '.formatBytes($CRIT[2]).'\j" ';


?>
