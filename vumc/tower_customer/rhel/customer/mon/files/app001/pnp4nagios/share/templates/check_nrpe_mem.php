<?php

$alpha = 'CC';
$colors = array(
    '#008000' . $alpha,
    '#F4d03F' . $alpha,
    '#25345C' . $alpha,
    '#88008A' . $alpha,
    '#4F7774' . $alpha,
);

$lower = $MAX[2] * -1;

$opt[1] = sprintf('-T 55 -l 0 --vertical-label "GB" --title "%s / Memory Usage" --rigid --upper-limit '.$CRIT[1].' --lower-limit '.$lower, $hostname);
$def[1] = '';
$def[1] =  "DEF:physical=$rrdfile:$DS[1]:AVERAGE " ;
$def[1] .= "DEF:swap=$rrdfile:$DS[2]:AVERAGE " ;
$def[1] .= "CDEF:swapi=swap,-1,* " ;
//$def[1] .= rrd::area ("physical", $colors[0], rrd::cut(ucfirst($NAME[1]), 15));
$def[1] .= rrd::gradient('physical',$colors[0],'00ff00',rrd::cut(ucfirst($NAME[1])),10);
$def[1] .= rrd::gprint  ("physical", array('LAST','MAX','AVERAGE'), "%4.2lf %s\\t");
//$def[1] .= rrd::area ("swapi", $colors[1], rrd::cut(ucfirst($NAME[2]), 15));
$def[1] .= rrd::gradient('swapi',$colors[1],'ffff00',rrd::cut(ucfirst($NAME[2])),10);
$def[1] .= rrd::gprint  ("swap", array('LAST','MAX','AVERAGE'), "%4.2lf %s\\t");
$def[1] .= rrd::hrule( $CRIT[1], "#ff0000", "PHYSICAL utilization critical at $CRIT[1] GB\\n" );
$def[1] .= rrd::hrule( $lower, "#00ffff", "SWAP     utilization critical at $CRIT[2] GB\\n" );
?>
