<?php
$opt[1] = "--vertical-label \"\" -l 0 -r --title \"NSCD cache usage for $hostname / $servicedesc\" ";

$def[1] =  "DEF:cache_hit=$rrdfile:$DS[1]:AVERAGE " ;
$def[1] .=  "DEF:cache_miss=$rrdfile:$DS[2]:AVERAGE " ;

$def[1] .= "COMMENT:\"\\t\\t\\t\\tLAST\\t\\t\\tAVERAGE\\t\\t\\tMAX\\n\" " ;

$def[1] .= "AREA:cache_hit#008000:\"Cache Hits\\t\\t\":STACK " ; 
$def[1] .= "GPRINT:cache_hit:LAST:\"%6.2lf \\t\\t\" " ;
$def[1] .= "GPRINT:cache_hit:AVERAGE:\"%6.2lf \\t\\t\\t\" " ;
$def[1] .= "GPRINT:cache_hit:MAX:\"%6.2lf \\n\" " ;
$def[1] .= "AREA:cache_miss#E80C3E:\"Cache Misses\\t\":STACK " ;
$def[1] .= "GPRINT:cache_miss:LAST:\"%6.2lf \\t\\t\" " ;
$def[1] .= "GPRINT:cache_miss:AVERAGE:\"%6.2lf \\t\\t\\t\" " ;
$def[1] .= "GPRINT:cache_miss:MAX:\"%6.2lf \\n\" " ;

?>
