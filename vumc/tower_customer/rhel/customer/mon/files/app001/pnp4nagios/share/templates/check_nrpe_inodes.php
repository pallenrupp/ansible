    <?php
    #
    # Copyright (c) 2006-2009 Joerg Linge (http://www.pnp4nagios.org)
    # Template for integer values
    #
    $def[1] = "";
    $opt[1] = "";
    $opt[1] .= "-l0 --units-exponent=0 --upper-limit 100 --vertical-label \"inodes used %\"  --title \"inodes used %\" --color=GRID#00991A --color=MGRID#00991A --color=ARROW#FF0000";
    $colors = array('#000000', '#0f0', '#f0f', '#00f', '#0ff', '#42f48f', '#aeaf44', '#ce880e', '#0dcea4', '#0cc1ce', '#0b73ce', '#4f0ace', '#ce09ca', '#ce0957', '#050', '#550', '#500', '#505', '#005', '#055');
    foreach ( $DS as $KEY => $VAL ){
            $def[1] .= "DEF:var_float$KEY=$RRDFILE[$KEY]:$DS[$KEY]:MAX " ;
            $def[1] .= "CDEF:var$KEY=var_float$KEY,FLOOR " ;
            $label=str_pad($LABEL[$KEY],15," ",STR_PAD_RIGHT);
            $def[1] .= "LINE2:var$KEY$colors[$KEY]:\"$label\" " ;
     
            if ($WARN[$KEY] != "") {
                $def[1] .= "HRULE:$WARN[$KEY]#FFFF00 ";
            }
            if ($CRIT[$KEY] != "") {
                $def[1] .= "HRULE:$CRIT[$KEY]#FF0000 ";
            }
            $def[1] .= "GPRINT:var$KEY:LAST:\"%2.0lf $UNIT[$KEY]\\n\" ";
    }
    ?>


