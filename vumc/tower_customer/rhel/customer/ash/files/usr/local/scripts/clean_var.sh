#!/bin/bash
###########################################################
# if /var is over 50% full see if we can clean insights
# file from /var/tmp.  There is currently an open incident
# with Red Hat Case #02120038 about insights leaving files
# in /var/tmp.
###########################################################

# Check var filling; don't worry if less than 50% full
if (( `df -Pm /var|tail -1|awk '{print $5}'|awk -F"%" '{print $1}'` < 50 )); then
    exit 0
fi

# find insights files left in /var/tmp/ directories and remove
for i in `find /var/tmp/ -name insights\-$HOSTNAME*|awk -F"/" '{print $4}'`; do
#    echo "removing /var/tmp/$i";
    rm -fR /var/tmp/$i;
done

# remove empty files older than 37 days old
for i in `find /var/tmp/ -empty -type f -mtime +37|grep -v systemd`; do
    rm $i;
done

# remove empty directories older than 37 days old
for i in `find /var/tmp/ -empty -type d -mtime +37|grep -v systemd`; do
    rmdir $i;
done
