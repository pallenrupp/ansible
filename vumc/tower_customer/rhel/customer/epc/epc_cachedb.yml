---
    - block:
        - include_tasks: ../../app/ssh/ssh.yml
          vars:
            options:
              - { epic: "{{ item.epic }}" }
          when: item.cond
          loop:
            - { epic: 'dev', cond: '{{ cache_env_dev }}' }
            - { epic: 'uat', cond: '{{ cache_env_uat }}' }
            - { epic: 'trn', cond: '{{ cache_env_trn }}' }
            - { epic: 'prd', cond: '{{ cache_env_prd }}' }
            - { epic: 'sup', cond: '{{ cache_env_sup }}' }
            - { epic: 'rpt', cond: '{{ cache_env_rpt }}' }

        - name: Allow ssh to listen on tcp port 2022
          seport:
            ports: 2022
            proto: tcp
            setype: ssh_port_t
            state: present

    - name: Add groups to access.conf
      lineinfile:
        path: /etc/security/access.conf
        regexp: '^\+\s*:\s*{{ item.access_group }}\s:.*'
        insertbefore: '^-.*ALL'
        line: '+ : {{ item.access_group }} : ALL'
      when: item.cond
      loop:
        - { access_group: 'sg-epc_dev_usr', cond: '{{ dev and not ecp_app_srvr }}' }
        - { access_group: 'sg-epc_uat_usr', cond: '{{ tst }}' }
        - { access_group: 'sg-epc_trn_usr', cond: '{{ trn }}' }
        - { access_group: 'sg-epc_prd_usr', cond: '{{ prd }}' }
        - { access_group: 'sg-epc_sup_usr', cond: '{{ cache_env_sup }}' }
        - { access_group: 'sg-epc_rpt_usr', cond: '{{ cache_env_rpt }}' }

    - name: Loop through sudo commands and expand as necessary
      set_fact:
        commands: "{{ commands|default([]) + [item] }}"
        cacheable: no
      when: item.cond|default(true)
      loop:
        - { command: 'NOPASSWD: /bin/su - epicdmn', username: '%sg-epc_odba_adm' }

    - include_tasks: ../users.yml
      vars:
        uid: 1601
        gid: 1605
        username: epicdmn
        group: epicuser
        comment: epicdmn User
        home: /home/epicdmn
        app_mode: '0700'
        ad_group: '{{ dmn_ad_group|default([]) }}'
        additional_groups: ["dmngrp"]

    - include_tasks: ../users.yml
      vars:
        uid: 1659
        gid: 1605
        username: epichlthcon
        group: epicuser
        comment: epichlthcon User
        home: /home/epichlthcon
        app_mode: '0700'
        ad_group: ''

    - include_tasks: ../users.yml
      vars:
        uid: 1602
        gid: 1605
        username: epicsupt
        group: epicuser
        comment: epicsupt User
        home: /home/epicsupt
        app_mode: '0700'
        ad_group: ''
        local: yes

    - include_tasks: ../users.yml
      vars:
        uid: 1603
        gid: 1605
        username: epictxt
        group: epicuser
        comment: epictxt User
        home: /home/epictxt
        app_mode: '0700'
        ad_group: ''

    - include_tasks: ../users.yml
      vars:
        uid: 1604
        gid: 1605
        username: iscagent
        access_level: LOCAL
        group: iscagent
        comment: iscagent User
        home: /home/iscagent
        app_mode: '0700'
        ad_group: ''

    - name: Install client requested packages
      package:
        name: "{{ packages }}"
        state: present
        lock_timeout: 180 # wait up to 3 minutes for a lock ansible/ansible#57189
      vars:
        packages:
          ['at','iperf3','ksh','openldap-clients','patch','perl-CGI','perl-core','psmisc','rsync','screen','time']

    - name: Gather facts
      setup:
        gather_subset:
          - mounts
        gather_timeout: 5

    - name: Set permissions - not mounted
      file:
        path: '{{ item.path }}'
        owner: root
        group: root
        mode: '0755'
        state: directory
      when: item.cond and item.path not in (ansible_mounts|json_query('[*].mount'))
      loop:
        - { path: '/backup',         cond: '{{ inventory_hostname_short in ["epc1110lt","epc1113lr","epc1116lp","epc3137lp"] }}' }
        - { path: '/backup/altdev',  cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/altqa',   cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/alttst',  cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/dev',     cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/poc',     cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/qa',      cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/tst',     cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/tstrpt',  cond: '{{ inventory_hostname_short in ["epc1110lt"] }}' }
        - { path: '/backup/mst',     cond: '{{ inventory_hostname_short in ["epc1113lr"] }}' }
        - { path: '/backup/prep',    cond: '{{ inventory_hostname_short in ["epc1113lr"] }}' }
        - { path: '/backup/ref',     cond: '{{ inventory_hostname_short in ["epc1113lr"] }}' }
        - { path: '/backup/prd',     cond: '{{ inventory_hostname_short in ["epc1116lp"] }}' }
        - { path: '/backup/prd01',   cond: '{{ inventory_hostname_short in ["epc1116lp"] }}' }
        - { path: '/backup/rpt',     cond: '{{ inventory_hostname_short in ["epc3117lp"] }}' }
        - { path: '/backup/rpt01',   cond: '{{ inventory_hostname_short in ["epc3117lp"] }}' }
        - { path: '/epic',           cond: '{{ true }}' }
        - { path: '/epic_core',      cond: '{{ true }}' }

    - name: Set permissions - mounted
      file:
        path:  '{{ item.path }}'
        owner: '{{ item.owner }}'
        group: '{{ item.group }}'
        mode:  '{{ item.mode }}'
        state: directory
      when: item.path in (ansible_mounts|json_query('[*].mount'))
      loop:
        - { path: '/epic',      owner: 'epicadm', group: 'epicsys',  mode: '02755' }
        - { path: '/epic_core', owner: 'epicadm', group: 'cachegrp', mode: '0777'  }

    - name: Set permissions
      file:
        path:  '{{ item.path }}'
        owner: '{{ item.owner|default("root") }}'
        group: '{{ item.group|default("root") }}'
        mode:  '{{ item.mode|default("0755") }}'
        state: directory
      when: item.cond|default(true)
      loop:
        - { path: '/epic/jrn',                          owner: 'root',    group: 'root',     mode:  '0755', cond: "{{ '/epic' in (ansible_mounts|json_query('[*].mount')) and '/epic/jrn' not in (ansible_mounts|json_query('[*].mount'))}}" }
        - { path: '/epic/jrn',                          owner: 'epicadm', group: 'cachegrp', mode:  '0755', cond: "{{ '/epic/jrn' in (ansible_mounts|json_query('[*].mount')) }}" }
        - { path: '/epic/logon',                        owner: 'epicadm', group: 'epicsys',  mode: '02755', cond: "{{ '/epic' in (ansible_mounts|json_query('[*].mount')) }}" }
        - { path: '/epicfiles'                                          }
        - { path: '/etc/systemd/journald.conf.d'                        }
        - { path: '/usr/local/epic',                    owner: 'epicadm', group: 'epicsys' }
        - { path: '/usr/local/etc/cachesys'                             }
        - { path: '/usr/local/share/applications/epic'                  }

    - name: Set root permissions on epic mountpoints that are not mounted
      file:
        path:  '{{ item.path }}'
        owner: '{{ item.owner|default("root") }}'
        group: '{{ item.group|default("root") }}'
        mode:  '{{ item.mode|default("0755") }}'
        state: directory
      when: "lookup('vars', 'cache_env_' + item.env)|bool and '{{ item.base }}' in (ansible_mounts|json_query('[*].mount')) and '{{ item.path }}' not in (ansible_mounts|json_query('[*].mount'))"
      loop:
        - { base: '/epic',                path: '/epic/prd',                      env: 'prd' }
        - { base: '/epic',                path: '/epic/prd01',                    env: 'prd' }
        - { base: '/epic',                path: '/epic/rpt',                      env: 'rpt' }
        - { base: '/epic',                path: '/epic/rptlocal',                 env: 'rpt' }
        - { base: '/epic',                path: '/epic/rpt01',                    env: 'rpt' }
        - { base: '/epic',                path: '/epic/sup',                      env: 'sup' }
        - { base: '/epic',                path: '/epic/sup01',                    env: 'sup' }
        - { base: '/epic',                path: '/epic/supmo',                    env: 'sup' }
        - { base: '/epic',                path: '/epic/supmo01',                  env: 'sup' }
        - { base: '/epicfiles',           path: '/epicfiles/prdfiles',            env: 'prd' }
        - { base: '/epicfiles',           path: '/epicfiles/prdfiles',            env: 'sup' }
        - { base: '/epicfiles',           path: '/epicfiles/rptfiles',            env: 'rpt' }
        - { base: '/epicfiles/prdfiles',  path: '/epicfiles/prdfiles/Users.temp', env: 'prd' }
        - { base: '/epicfiles/prdfiles',  path: '/epicfiles/prdfiles/Users.temp', env: 'sup' }
        - { base: '/epicfiles/rptfiles',  path: '/epicfiles/rptfiles/Users.temp', env: 'rpt' }

    - name: Set app permissions on epic mountpoints that are mounted
      file:
        path:  '{{ item.path }}'
        owner: '{{ item.owner|default("epicadm") }}'
        group: '{{ item.group|default("epicsys") }}'
        mode:  '{{ item.mode|default("02755") }}'
        state: directory
      when: "lookup('vars', 'cache_env_' + item.env)|bool and '{{ item.base }}' in (ansible_mounts|json_query('[*].mount')) and '{{ item.path }}' in (ansible_mounts|json_query('[*].mount'))"
      loop:
        - { base: '/epic',                path: '/epic/prd',                      env: 'prd' }
        - { base: '/epic',                path: '/epic/prd01',                    env: 'prd' }
        - { base: '/epic',                path: '/epic/rpt',                      env: 'rpt' }
        - { base: '/epic',                path: '/epic/rptlocal',                 env: 'rpt' }
        - { base: '/epic',                path: '/epic/rpt01',                    env: 'rpt' }
        - { base: '/epic',                path: '/epic/sup',                      env: 'sup' }
        - { base: '/epic',                path: '/epic/sup01',                    env: 'sup' }
        - { base: '/epic',                path: '/epic/supmo',                    env: 'sup' }
        - { base: '/epic',                path: '/epic/supmo01',                  env: 'sup' }
        - { base: '/epicfiles',           path: '/epicfiles/prdfiles',            env: 'prd',  group: 'epicuser',  mode: '02775' }
        - { base: '/epicfiles',           path: '/epicfiles/prdfiles',            env: 'sup',  group: 'epicuser',  mode: '02775' }
        - { base: '/epicfiles',           path: '/epicfiles/rptfiles',            env: 'rpt',  group: 'epicuser',  mode: '02775' }
        - { base: '/epicfiles/prdfiles',  path: '/epicfiles/prdfiles/Users.temp', env: 'prd',  group: 'epicuser',  mode: '03777' }
        - { base: '/epicfiles/prdfiles',  path: '/epicfiles/prdfiles/Users.temp', env: 'sup',  group: 'epicuser',  mode: '03777' }
        - { base: '/epicfiles/rptfiles',  path: '/epicfiles/rptfiles/Users.temp', env: 'rpt',  group: 'epicuser',  mode: '03777' }

    - name: Epic pam limits
      pam_limits:
        dest: /etc/security/limits.d/99-epic.conf
        domain: '{{ item.domain }}'
        limit_type: '{{ item.limit_type }}'
        limit_item: '{{ item.limit_item }}'
        value: '{{ item.value }}'
      when: item.cond|default(true)
      loop:
        - { domain: 'root',     limit_type: 'soft', limit_item: 'core',    value: '-1' }
        - { domain: 'root',     limit_type: 'hard', limit_item: 'core',    value: '-1' }
        - { domain: 'epicadm',  limit_type: 'soft', limit_item: 'core',    value: '-1' }
        - { domain: 'epicadm',  limit_type: 'hard', limit_item: 'core',    value: '-1' }
        - { domain: 'epicdmn',  limit_type: 'soft', limit_item: 'core',    value: '-1' }
        - { domain: 'epicdmn',  limit_type: 'hard', limit_item: 'core',    value: '-1' }
        - { domain: 'epicadm',  limit_type: 'soft', limit_item: 'data',    value: '2097152', cond: '{{ dev }}' }
        - { domain: 'epicadm',  limit_type: 'hard', limit_item: 'data',    value: '2097152', cond: '{{ dev }}' }
        - { domain: 'epicdmn',  limit_type: 'soft', limit_item: 'data',    value: '204800', cond: '{{ dev }}' }
        - { domain: 'epicdmn',  limit_type: 'hard', limit_item: 'data',    value: '204800', cond: '{{ dev }}' }
        - { domain: 'epicdmn',  limit_type: 'soft', limit_item: 'nproc',   value: '-1' }
        - { domain: 'epicdmn',  limit_type: 'hard', limit_item: 'nproc',   value: '-1' }
        - { domain: 'epicadm',  limit_type: 'soft', limit_item: 'nproc',   value: '-1' }
        - { domain: 'epicadm',  limit_type: 'hard', limit_item: 'nproc',   value: '-1' }
        - { domain: 'epicadm',  limit_type: 'soft', limit_item: 'memlock', value: '-1' }
        - { domain: 'epicadm',  limit_type: 'hard', limit_item: 'memlock', value: '-1' }

    - name: Create EPIC cache server sysctl
      sysctl:
        name: '{{ item.name }}'
        value: '{{ item.value }}'
        state: present
        sysctl_file: /etc/sysctl.d/99-epic.conf
      when: item.cond|default(true)
      loop:
        - { name: 'fs.file-max',                  value: '1000000000' }
        - { name: 'fs.aio-max-nr',                value: '524888', cond: "{{ rhel8 }}" }
        - { name: 'kernel.core_pattern',          value: '/epic_core/%e.%h.%p.%t.dump' }
        - { name: 'kernel.msgmnb',                value: '65536' }
        - { name: 'kernel.msgmni',                value: '32000' }
        - { name: 'kernel.numa_balancing',        value: '0' }
        - { name: 'kernel.pid_max',               value: '4000000' }
        - { name: 'kernel.sem',                   value: '250 1024000000 500 32000' }
        - { name: 'net.core.netdev_max_backlog',  value: '300000' }
        - { name: 'net.core.rmem_max',            value: '33554432' }
        - { name: 'net.core.wmem_max',            value: '33554432' }
        - { name: 'net.ipv4.tcp_keepalive_time',  value: '600' }
        - { name: 'net.ipv4.tcp_keepalive_intvl', value: '60' }
        - { name: 'net.ipv4.tcp_rmem',            value: '4096 87380 33554432' }
        - { name: 'net.ipv4.tcp_wmem',            value: '4096 65536 33554432' }
        - { name: 'vm.dirty_expire_centisecs',    value: '200' }
        - { name: 'vm.dirty_writeback_centisecs', value: '50' }
        - { name: 'vm.hugetlb_shm_group',         value: '1600' }
        - { name: 'vm.swappiness',                value: '1' }

    - name: Create EPIC cache server hugepages sysctl file
      sysctl:
        name: '{{ item.name }}'
        value: '{{ item.value }}'
        state: present
        sysctl_file: /etc/sysctl.d/99-epic-hugepages.conf
      loop:
        - { name: 'vm.nr_hugepages',              value: 6,          host: 'epc1110ld'}
        - { name: 'vm.nr_hugepages',              value: 24,         host: 'epc1110lt'}
        - { name: 'vm.nr_hugepages',              value: 50,         host: 'epc1110lp'}
        - { name: 'vm.nr_hugepages',              value: 21,         host: 'epc1111lt'}
        - { name: 'vm.nr_hugepages',              value: 35,         host: 'epc1113lr'}
        - { name: 'vm.nr_hugepages',              value: 800,        host: 'epc1116lp'}
        - { name: 'vm.nr_hugepages',              value: 500,        host: 'epc1117lp'}
        - { name: 'vm.nr_hugepages',              value: 4,          host: 'epc1120ld'}
        - { name: 'vm.nr_hugepages',              value: 4,          host: 'epc1121ld'}
        - { name: 'vm.nr_hugepages',              value: 50,         host: 'epc3110lp'}
        - { name: 'vm.nr_hugepages',              value: 800,        host: 'epc3116lp'}
        - { name: 'vm.nr_hugepages',              value: 500,        host: 'epc3117lp'}
      when: inventory_hostname_short == item.host

    - name: Create symbolic links
      file:
        src: '{{ item.src }}'
        dest: '{{ item.dest }}'
        state: link
        force: '{{ item.force|default(False) }}'
      when: item.cond
      loop:
        - { cond: '{{ cache_db_prd and ( cache_env_prd or cache_env_sup ) }}', src: '/epicfiles/prdfiles', dest: '/epic/prdfiles', force: True }
        - { cond: '{{ cache_db_prd and cache_env_rpt }}', src: '/epicfiles/rptfiles', dest: '/epic/rptfiles', force: True }
        - { cond: '{{ dev or tst or cache_db_trn }}', src: '/epicfiles/nonprdfiles', dest: '/epic/nonprdfiles', force: True }
        - { cond: '{{ rhel8 and cache_db }}', src: '/dev/null', dest: '/etc/sysctl.d/50-coredump.conf' }

    - name: insert/update vmware-tools settings
      ini_file:
        path: /etc/vmware-tools/tools.conf
        section: vmbackup
        option: enableSyncDriver
        value: false
        create: yes

    - name: Copy customer files
      copy:
        src: "{{ item.src|default('files' + item.dest) }}"
        dest: "{{ item.dest }}"
        owner: "{{ item.owner }}"
        group: "{{ item.group }}"
        mode: "{{ item.mode }}"
      when: item.cond|default(true)
      loop:
        - { dest: '/etc/systemd/journald.conf.d/epic.conf',               owner: 'root',    group: 'root',    mode: '0744' }
        - { dest: '/usr/local/share/applications/epic/epic_lv_backup.sh', owner: 'root',    group: 'root',    mode: '0744' }

    - name: Copy systemd files
      copy:
        src: "{{ item.src|default('files' + item.dest) }}"
        dest: "{{ item.dest }}"
        owner: "{{ item.owner|default('root') }}"
        group: "{{ item.group|default('root') }}"
        mode: "{{ item.mode|default('0644') }}"
      register: systemd
      when: item.cond|default(true)
      loop:
        - { dest: '/etc/systemd/system/epic.service', cond: '{{ prd and rhel8 }}' }

    - name: enable services
      service:
        name: epic
        enabled: yes
      when: prd

    - include_tasks: ../../common/fact.yml
      vars:
        factname: grub
        options:
          - { cmdline: "{{ item.cmdline }}" }
      when: item.cond
      loop:
        - { cmdline: 'modeset=0 elevator=deadline init_on_alloc=0 transparent_hugepage=never default_hugepagesz=1G hugepagesz=1G rd.blacklist=vmwgfx ipv6.disable=1 rd.lvm.lv=vg00/lvusr rd.lvm.lv=vg00/lvroot', cond: '{{ rhel8 }}' }

    - name: Modify /etc/systemd/  .conf files
      ini_file:
        path: '{{ item.path }}'
        section: '{{ item.section }}'
        option: '{{ item.option }}'
        value: '{{ item.value }}'
        create: yes
      register: systemd_settings
      loop:
        - { path: '/etc/systemd/logind.conf', section: 'Login',   option: 'UserTasksMax',    value: 'infinity' }
        - { path: '/etc/systemd/system.conf', section: 'Manager', option: 'DefaultTasksMax', value: 'infinity' }
        - { path: '/etc/systemd/system.conf', section: 'Manager', option: 'CtrlAltDelBurstAction', value: 'none', cond: '{{ dev }}' }
      when: rhel8

    - name: Reload systemd
      systemd:
        daemon_reload: yes
      when: systemd.changed

    # disable fstrim.timer because we default it to being on for everything else
    - include_tasks: ../../common/fact.yml
      vars:
        factname: fstrim
        options:
          - { enabled: false }
