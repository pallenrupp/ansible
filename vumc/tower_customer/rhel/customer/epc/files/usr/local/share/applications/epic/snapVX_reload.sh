#!/bin/bash

################################################################################################
#                                                                                              #
# Mark Shanks, Jason Minton                                                                    #
# mark.w.shanks@vumc.org, j.minton@vumc.org                                                    #
#                                                                                              #
# Script:                                                                                      #
# /usr/local/share/applications/epic/snapVX_reload.sh - Must run as (root) sudo.               #
#                                                                                              #
# Description:                                                                                 #
# Mounts SAN SnapVX Snapshot to epic file system from storage array for environment refreshes. #
#                                                                                              #
################################################################################################

#   FUNCTION: Logging datetime stamps.

snapVX_step()
{
    echo "`date +%m/%d/%Y' '%H:%M:%S` ${1}"
}

#   FUNCTION: Email Errors from AUTO - short description (appended to email subject).

snapVX_mail()
{
    grep -B1 "`date +%m/%d/%Y`.*ERR" ${FILE} | mail -s "${SERVER} (${SCRIPT}) ${1}" "${MAIL}"
}

#   FUNCTION: Setup for Automated/Scheduled run from AUTO.

snapVX_init()
{
    rm -f ${FILE}
    rm -f ${LOCK}

    snapVX_step "SnapVX Process Environment Initialization."

    if [ `whoami` != 'root' ]; then echo "ERROR: Must run as [root]."; exit 1; fi
    
#   Redirect STDOUT and STDERR to the log file.
    exec 1>>${FILE} 2>&1

#   Create Log File.
    touch ${FILE}
    chown root:epicsys ${FILE}
    chmod 664 ${FILE}

#   Prevent multiple threads from running simultaneously.
    if [ -e "${LOCK}" ]; then

    snapVX_step   "ERROR: LOCK [${LOCK}] exists; Exiting!"
    snapVX_mail "- ERROR: LOCK Prevented Execution."; exit 1; fi

#   Create Process Lock.
    echo "PID: $$" > ${LOCK}
    chown epicadm:epicsys ${LOCK}
    chmod 664 ${LOCK}

    if [ ! -e "${LOCK}" ]; then snapVX_step "ERROR: Unable to create LOCK [${LOCK}]; proceeding ... "; ((ERRORS++)); fi
}

#   FUNCTION: Unmount/Deactivate/Export Volume Group.

snapVX_prep()
{
    snapVX_step  "[${INSTID}][${TARGET}] - [umount -f ${FOLDER}]"

    if mountpoint -q ${FOLDER}; then umount -f ${FOLDER}; fi

    if [ ${?} != 0 ]; then snapVX_step "ERROR: [${INSTID}][${TARGET}] - [${FOLDER}] umount -f Failed!"; ((ERRORS++)); exit 1; fi

    snapVX_step "[${INSTID}][${TARGET}] - [vgchange -a n ${VGNAME}]";                  vgchange -a n    ${VGNAME}
    snapVX_step "[${INSTID}][${TARGET}] - [vgexport ${VGNAME}]";                       vgexport         ${VGNAME}
}

#   FUNCTION: Import/Reactivate/Remount Volume Group.

snapVX_post()
{
    if [ ! -d "${FOLDER}" ]; then mkdir -p ${FOLDER}; fi

    if [ ${?} != 0 ]; then snapVX_step "ERROR: [${INSTID}][${TARGET}] - [${FOLDER}] mkdir -p Failed!";  ((ERRORS++)); exit 1; fi

    snapVX_step "[${INSTID}][${TARGET}] - [vgimportclone -n ${VGNAME} -i ${VECTOR}]";  vgimportclone -n ${VGNAME} -i ${VECTOR}
    snapVX_step "[${INSTID}][${TARGET}] - [lvrename ${VGNAME} lvprd01 ${LVNAME}]";     lvrename         ${VGNAME} lvprd01 ${LVNAME}
    snapVX_step "[${INSTID}][${TARGET}] - [vgchange -a y ${VGNAME}]";                  vgchange -a y    ${VGNAME}
    snapVX_step "[${INSTID}][${TARGET}] - [systemctl daemon-reload]";                  systemctl daemon-reload

    sleep 66;

#   if ! grep -qs ${FOLDER} /proc/mounts
    if ! mountpoint -q ${FOLDER}
    then
        snapVX_step "[${INSTID}][${TARGET}] - [mount -a]"; mount -a
    fi

#   snapVX_step "[${INSTID}][${TARGET}] - [mount -o nouuid,discard ${SYSTEM} ${FOLDER}]"

#   if ! mount -l | grep "on ${FOLDER}"; then mount -o nouuid,discard ${SYSTEM} ${FOLDER}; fi
#   if [ ${?} != 0 ]; then snapVX_step "ERROR: [${INSTID}][${TARGET}] - [${FOLDER}] mount -o Failed!";  ((ERRORS++)); exit 1; fi

    chown epicadm:epicsys ${FOLDER}
    chmod 755 ${FOLDER}
}

#   MAIN

if [ ${#} -ne 2 ]; then echo "USAGE: snapVX_reload.sh [REL,RELVAL,SUPDC1,SUPMO1,SUPDC3,SUPMO3] [import,export]"; exit 1; fi

# Initialize Environment.
ERRORS=0
SLEEPS=0

SERVER=`hostname`
SCRIPT=`basename $0`

DATE=`date +%Y%m%d.%H%M%S`

PATH=/usr/bin:/usr/sbin:/bin
CONF=/home/epicadm/snapvx/conf/snapVX_relink.conf
LOCK=/home/epicadm/snapvx/lock/${SERVER}.${SCRIPT}
FILE=/home/epicadm/snapvx/logs/${SCRIPT}.${DATE}.${1}.${2}

MAIL=`cat ${CONF} | grep ^email | awk -F \: ' { print $2 } ' -`

# Initialize Data from SnapVX Configuration File
CONFIG=`grep -w ${1} ${CONF}`

SOURCE=$(     echo ${CONFIG} | awk '      { printf ("%-s", $1) } ' -)
TARGET=$(     echo ${CONFIG} | awk '      { printf ("%-s", $2) } ' -)
HOSTVM=$(     echo ${CONFIG} | awk '      { printf ("%-s", $3) } ' -)
INSTID=$(     echo ${CONFIG} | awk '      { printf ("%-s", $4) } ' -)
SNAPVX=$(     echo ${CONFIG} | awk '      { printf ("%-s", $5) } ' -)
VOLUME=$(     echo ${CONFIG} | awk '      { printf ("%-s", $6) } ' -)
DEVICE=$(     echo ${CONFIG} | awk '      { printf ("%-s", $7) } ' -)

SNAPVX_SID=$( echo ${SNAPVX} | awk -F : ' { printf ("%-s", $1) } ' -)
SNAPVX_SG=$(  echo ${SNAPVX} | awk -F : ' { printf ("%-s", $2) } ' -)
SNAPVX_LNSG=$(echo ${SNAPVX} | awk -F : ' { printf ("%-s", $3) } ' -)
SNAPVX_DAYS=$(echo ${SNAPVX} | awk -F : ' { printf ("%-s", $4) } ' -)

VXPATH=$(     echo ${VOLUME} | awk -F , ' { printf ("%-s", $1) } ' -)
DMPATH=$(     echo ${VXPATH} | awk -F : ' { printf ("%-s", $1) } ' -)
OSPATH=$(     echo ${VXPATH} | awk -F : ' { printf ("%-s", $2) } ' -)
VGNAME=$(     echo ${DMPATH} | awk -F - ' { printf ("%-s", $1) } ' -)
LVNAME=$(     echo ${DMPATH} | awk -F - ' { printf ("%-s", $2) } ' -)

INSTID=$(     echo ${INSTID} | awk      ' { printf ("%-s", (substr ($0,1,3) == "SUP") ? "SUP" : $0); } ' -)
VECTOR=$(     echo ${DEVICE} | awk -F : ' { printf ("/dev/%-s /dev/%-s /dev/%-s /dev/%-s /dev/%-s /dev/%-s /dev/%-s /dev/%-s", $1,$2,$3,$4,$5,$6,$7,$8); } ' -)

FOLDER=/epic/${OSPATH}
SYSTEM=/dev/mapper/${DMPATH}

snapVX_step "Working on [${INSTID}][${TARGET}]."

# Initialize SnapVX Environment.
snapVX_init

case ${2} in

    export) snapVX_prep ;;
    import) snapVX_post ;;

esac

if [ ${?} != 0 ]; then

snapVX_step "ERROR:   [${INSTID}][${TARGET}] - [${2}] Failed!"; ((ERRORS++)); exit 1; fi
snapVX_step "Finished [${INSTID}][${TARGET}] - SnapVX Process!"

# Environment Copy Delay.
if [ ${2} == "import" ]; then

while [ `find ${FOLDER} -name iris.lck | wc -l` -eq 0 ]; do ((SLEEPS++)); if [ ${SLEEPS} -gt 11 ]; then exit 1; else sleep 1; fi; done

fi

# Remove LOCK if running from AUTO.
rm -f ${LOCK}
rm -f `find ${FOLDER} -name iris.lck`

# If any ERRORS were logged, send an email alert.
if [ "${ERRORS}" -gt "0" ]; then snapVX_mail "- ${ERRORS} ERRORS DETECTED"; fi

snapVX_step "Exiting [$SCRIPT]."

exit 0
