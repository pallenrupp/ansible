#!/bin/bash

if [ ${#} -ne 1 ]; then echo "USAGE: snapVX_report.sh [PRDDC1,PRDDC3]"; exit; fi

CONFIG=/app001/epicadm/snapvx/conf/snapVX_relink.conf

SNAPVX=$(echo `grep -w ${1} ${CONFIG}` | awk      ' { printf ("%-s", $5) } ' -)
PORTVX=$(echo               ${SNAPVX}  | awk -F : ' { printf ("%-s", $1) } ' -)

if [ ${1} ]; then l="-lnsg ${1}"; fi; /opt/emc/SYMCLI/bin/symsnapvx -sid ${PORTVX} list ${l} -linked -by_tgt
