#!/usr/bin/perl -w

# EPIC CREATE OR REMOVE LOCAL HOME DIR SINCE NFS IS NOT TO BE USED ON EPC HOSTS
# VERSION 1.01 - ADDED PROTECTED NAME CHECK


use strict;
use File::Path;


# VARIABLES
my $instruction = $ARGV[0];
my $vunetid = $ARGV[1];

my $path = "/home";
my $uid = getpwnam($vunetid);

my @protected = (
  "epicadm",
  "epicdmn",
  "epicsupt",
  "epictxt",
  "iscagent"
);

chomp( my $hostname = `hostname -s` );
chomp( my $username = `whoami` );
chomp( my $mnttype = `stat -f -L -c %T $path` );


# VERIFY REQUIRED COMMAND LINE OPTIONS ARE PRESENTED, LOG, AND SHOW HELP
&do_help(print "[ERROR] add|remove is required\n") if (!$instruction);
&do_help(print "[ERROR] vunetid is required\n") if (!$vunetid);


# VALIDATE VUNETID FORMATTING
if ( $vunetid !~ /^[a-zA-Z0-9]*$/ ) {

    print "[ERROR] Invalid characters in vunetid $vunetid.  Command not completed.\n";
    `logger "EpicHomedir [ERROR] $username - Invalid charactors in vunetid $vunetid.  Command not completed."`;
    exit 1;

}


# GET UID FROM VUNETID
if (!$uid) { 

    print "[ERROR] Unable to get uid for vunetid $vunetid.  Command not completed.\n";
    `logger "EpicHomedir [ERROR] $username - Unable to get uid for vunetid $vunetid.  Command not completed."`;
    exit;

}


# CHECK TO SEE IF NAME IS PROTECTED
if ( $vunetid ~~ @protected ) {

    print "[ERROR] Protected name.  Command not completed.\n";
    `logger "EpicHomedir [ERROR] $username - Referenced protected name.  Command not completed."`;
    exit;

}


# CHECK PATH TYPE AND VUNETID EXISTENCE
if ( $mnttype ne "xfs" ) {

    print "[ERROR] Invalid mount type as $mnttype for $path.  Command not completed.\n";
    `logger "EpicHomedir [ERROR] $username - Invalid mount type as $mnttype for $path.  Command not completed."`;
    exit 1;

} 
else {


    if ( $instruction eq "add" ) {


        if (-e "$path/$vunetid" ) {

            print "[ERROR] The $path/$vunetid directory already exists.  Command not completed.\n";
            `logger "EpicHomedir [ERROR] $username - The $path/$vunetid directory already exists.  Command not completed."`;
            exit 1;

        }
        else  {

            mkdir("$path/$vunetid", 0700) unless(-d "$path/$vunetid");
            chown "$uid", "0", "$path/$vunetid";
            `cp -r /etc/skel/. $path/$vunetid`;
            chown "$uid", "0", "$path/$vunetid";
            chown "$uid", "0", "$path/$vunetid/.bash_logout";
            chown "$uid", "0", "$path/$vunetid/.bash_profile";
            chown "$uid", "0", "$path/$vunetid/.bashrc";
            chown "$uid", "0", "$path/$vunetid/.zshrc";

            print "[DONE] The $path/$vunetid directory has been created.  Command completed.\n";
            `logger "EpicHomedir [DONE] $username - The $path/$vunetid directory has been created.  Command completed."`;
            exit;

        } 

    }
    elsif ( $instruction eq "remove" ) {

        if (-e "$path/$vunetid" ) {

            print "Are you sure you want to remove the $path/$vunetid local directory? [Y|N]: ";
            chomp( my $answer = <STDIN> );

            if ( $answer eq "Y" ) {
        

                rmtree( "$path/$vunetid" );

                print "[DONE] The $path/$vunetid directory has been removed.  Command completed.\n";
                `logger "EpicHomedir [DONE] $username - The $path/$vunetid directory has been removed.  Command completed."`;
                exit;

            }
            else {

                print "[ERROR] The $path/$vunetid directory has not been removed.  Command not completed.\n";
                `logger "EpicHomedir [ERROR] $username - The $path/$vunetid directory has not been removed.  Command not completed."`;
                exit;

            }


        }
        else  {

            print "[ERROR] The $path/$vunetid directory does not exist.  Command not completed.\n";
            exit 1;

        } 

    }
    else {

        print "[ERROR] $instruction instruction not understood.  Command not completed.\n";
        do_help();
        exit 1;

    }

}


exit;


# SUB FUNCTIONS BELOW

sub do_help {

    print "\nEpic script to allow Epic admins to create or remove local home directories under /home.\n";
    print "NFS is not to be used on Epic hosts.\n\n";

    print "Minimum required options:\n";
    print " add|remove\n";
    print " vunetID\n\n";
    print "Syntax: EpicHostname.pl add|remove vunetid \n\n"; 

    exit 1;

}

