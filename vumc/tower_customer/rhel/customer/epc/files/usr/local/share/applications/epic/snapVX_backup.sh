#!/bin/bash
#
# Script: /usr/local/share/applications/epic/snapVX_backup.sh
# SVN:
#
# Description:
# Performs a SAN SnapVX Snapshot and mounts to /backup.
# Must run as (root) sudo.
#
# Contact:
# Jason Minton
# j.minton@vumc.org
#
# 20170807 j.minton - Script created.  
#
################################################################################
CONF=/app001/epicadm/Avamar/conf/snapVX_backup.conf

DTE=`date +%Y%m%d`
MYNAME=`basename $0`
PROXY=`hostname`
PATH=/usr/bin:/usr/sbin:/bin

EMAIL=`cat ${CONF} | grep ^email | awk -F \: '{print $2}'`
LOG=/app001/epicadm/Avamar/logs/${MYNAME}_${DTE}.log
RUNLOCK=/app001/epicadm/Avamar/locks/${PROXY}.${MYNAME}.LOCK
SSHKEY=/app001/epicadm/.ssh/id_rsa


declare -A SNAP_INFO
((ERRFLAG=0))


################################################################################

# FUNCTION: Logging
# Expects a log message.
stamplog() {
    echo "`date +%m/%d/%Y' '%H:%M:%S` $1"
}

# FUNCTION: Email Errors from AUTO.
# Expects a short description (appended to email subject).
mailerr() {
    # Only send email if running from AUTO.
    grep -B1 "`date +%m/%d/%Y`.*ERR" ${LOG} | mail -s "${PROXY} (${MYNAME}) $1" "${EMAIL}"
}

# FUNCTION: Cache Freeze
# Expects a hostname and Instance ID
instfreeze() {
    local RES

    epicenv=$(echo $2 | tr "[A-Z]" "[a-z]")
    RES=$(ssh -n -i $SSHKEY epicadm@$1 /epic/${epicenv}/bin/instfreeze 2>/dev/null)
    stamplog "[$INSTID][$HOST] - SSH Output: $RES"
    return $?
}

# FUNCTION: Cache Thaw
# Expects a hostname and Instance ID
instthaw() {
    local RES

    epicenv=$(echo $2 | tr "[A-Z]" "[a-z]")
    RES=$(ssh -n -i $SSHKEY epicadm@$1 /epic/$epicenv/bin/instthaw 2>/dev/null)
    stamplog "[$INSTID][$HOST] - SSH Output: $RES"
    return $?
}

# FUNCTION: Setup for Automated/Scheduled run from AUTO.
do_init() {
    # Verify running as root
    if [[ `whoami` != 'root' ]]; then
        echo "ERROR: Must run as [root]."
        exit
    fi
    
    # Redirect STDOUT and STDERR to the ${LOG} file.
    exec 1>>${LOG} 2>&1

    touch ${LOG}
    chown root:epicadm ${LOG}
    chmod 664 ${LOG}

    # Prevent this script from running multiple copies at the same time.
    if [[ -e "${RUNLOCK}" ]]; then
        stamplog "ERROR: RUNLOCK [${RUNLOCK}] exists; exiting."
        mailerr "- ERROR: RUNLOCK Prevented Execution."
        exit 1
    fi

    # Create Run Lock
    echo "PID: $$" > ${RUNLOCK}
    chown epicadm:epicadm ${RUNLOCK}
    if [[ ! -e "${RUNLOCK}" ]]; then
        stamplog "ERROR: Unable to create RUNLOCK [${RUNLOCK}]; proceeding... "
        ((ERRFLAG++))
    fi
}

# FUNCTION: DO POST
snapvx_post() {
    INSTID=$1
    SNAP_INFO=$2
    RET=0

    for (( cnt=1; cnt<=$((${SNAP_INFO[CNT]})); cnt++ ))
    do
        # vgimport VGs
        stamplog "[${INSTID}][$HOST] - [vgimport ${SNAP_INFO[lv_${cnt}_VG]}]"
        vgimport ${SNAP_INFO[lv_${cnt}_VG]}
        #if [[ $? != 0 ]]; then
        #    stamplog "ERROR: vgimport Failed."
        #    ((ERRFLAG++))
        #    RET=1
        #fi

        # vgchange (activate LV)
        stamplog "[${INSTID}][$HOST] - [vgchange -a y ${SNAP_INFO[lv_${cnt}_VG]}]"
        vgchange -a y ${SNAP_INFO[lv_${cnt}_VG]}
        #if [[ $? != 0 ]]; then
        #    stamplog "ERROR: vgchange Failed."
        #    ((ERRFLAG++))
        #    RET=1
        #fi

        if [[ ! -d "${SNAP_INFO[lv_${cnt}_MOUNT]}" ]]; then
            mkdir ${SNAP_INFO[lv_${cnt}_MOUNT]}
            if [[ $? != 0 ]]; then
                stamplog "ERROR: [${INSTID}][$HOST] - [${SNAP_INFO[lv_${cnt}_MOUNT]}] missing and [mkdir] failed."
                ((ERRFLAG++))
                RET=1
            fi
        fi

        # mount process
        stamplog "[${INSTID}][$HOST] - [mount -o ro -o nouuid ${SNAP_INFO[lv_${cnt}_DMPATH]} ${SNAP_INFO[lv_${cnt}_MOUNT]}]"
        mount -o ro -o nouuid ${SNAP_INFO[lv_${cnt}_DMPATH]} ${SNAP_INFO[lv_${cnt}_MOUNT]}
        if [[ $? != 0 ]]; then
            stamplog "ERROR: mount Failed."
            #((ERRFLAG++))
            #RET=1
        fi

    done

    return ${RET}
}

# FUNCTION: DO PREP
snapvx_prep() {
    INSTID=$1
    SNAP_INFO=$2
    RET=0

    for (( cnt=1; cnt<=$((${SNAP_INFO[CNT]})); cnt++ ))
    do
        # unmount process
        stamplog "[${INSTID}][$HOST] - [umount ${SNAP_INFO[lv_${cnt}_MOUNT]}]"
        if mount -l | grep "on ${SNAP_INFO[lv_${cnt}_MOUNT]}"; then

            fuser -cuv ${SNAP_INFO[lv_${cnt}_MOUNT]}
            umount -f  ${SNAP_INFO[lv_${cnt}_MOUNT]}

            sleep 66;

#           if [[ $? != 0 ]]; then
            if mountpoint -q ${SNAP_INFO[lv_${cnt}_MOUNT]}; then

                fuser -cuv ${SNAP_INFO[lv_${cnt}_MOUNT]}
                stamplog "ERROR: umount Failed."

            #    ((ERRFLAG++))
            #    RET=1
            fi
        else
            stamplog "Not mounted... skipping umount."
        fi

        # vgchange (deactivate LV)
        stamplog "[${INSTID}][$HOST] - [vgchange -a n ${SNAP_INFO[lv_${cnt}_VG]}]"
        vgchange -a n ${SNAP_INFO[lv_${cnt}_VG]}
        #if [[ $? != 0 ]]; then
        #    stamplog "ERROR: vgchange Failed."
        # Need to check if this is necessary before adding back ERRFLAGs
        #    ((ERRFLAG++))
        #    RET=1
        #fi

        # vgexport (remove the VG)
        stamplog "[${INSTID}][$HOST] - [vgexport ${SNAP_INFO[lv_${cnt}_VG]}]"
        vgexport ${SNAP_INFO[lv_${cnt}_VG]}
        #if [[ $? != 0 ]]; then
        #    stamplog "ERROR: vgexport Failed."
        # Need to check if this is necessary before adding back ERRFLAGs
        #    ((ERRFLAG++))
        #    RET=1
        #fi

    done
    return ${RET}
}

# FUNCTION: Load LVs for Current Instance into ASSOC ARRAY
load_snap_info() {
    VOLS=$1
    ((IDX=0))

    for VOL in $(echo $VOLS | tr "," " " | sed -e 's/^ *//g;s/ *$//g')
    do
        ((IDX++))
        DMPATH=$(echo ${VOL} | awk -F : '{print $1}')
        MNT=$(echo ${VOL} | awk -F : '{print $2}')
        SNAP_INFO[lv_${IDX}_DMPATH]=/dev/mapper/${DMPATH}
        SNAP_INFO[lv_${IDX}_VG]=$(echo ${DMPATH} | awk -F \- '{print $1}')
        SNAP_INFO[lv_${IDX}_MOUNT]=/backup/${MNT}
    done
    SNAP_INFO[CNT]=${IDX}
}

################################################################################

do_init
stamplog "Beginning Backup Process."
grep -vE "^#|^email|^[[:blank:]]|^$" ${CONF} | grep "^${PROXY}" | while read -r PROXYKEY HOST INSTID SNAPVX VOLS
do
    # Clear and re-declare SNAP_INFO Assoc Array for each ENV
    unset SNAP_INFO
    declare -A SNAP_INFO
    load_snap_info $VOLS 

    SNAPVX_SID=$(echo $SNAPVX | awk -F \: '{print $1}')
    SNAPVX_SG=$(echo $SNAPVX | awk -F \: '{print $2}')
    SNAPVX_LNSG=$(echo $SNAPVX | awk -F \: '{print $3}')
    SNAPVX_DAYS=$(echo $SNAPVX | awk -F \: '{print $4}')

    # Generate SnapVX Snapshot Name
    YYYYmmddHHMM=`date +%Y%m%d%H%M`
    SNAPVX_NAME="CACHE_${HOST}_SNP_${YYYYmmddHHMM}"

    stamplog "Working on [${INSTID}][${HOST}]."

    stamplog "[$INSTID][$HOST] - Running Pre Snap Commands."
    if snapvx_prep ${INSTID} ${SNAP_INFO}; then
        stamplog "[${INSTID}][$HOST] - Prep completed."
    else
        stamplog "ERROR: [${INSTID}][$HOST] - Prep failed."
        ((ERRFLAG++))
        continue
    fi

    # Freeze the Cache Instance
    stamplog "[${INSTID}][${HOST}] - Freezing Instance."
    instfreeze $HOST $INSTID
    if [[ $? != 0 ]]; then
        stamplog "ERROR: instfreeze Failed."
        ((ERRFLAG++))
        continue
    fi

    # Create SnapVX Snapshot
    stamplog "[${INSTID}][${HOST}] - [/opt/emc/SYMCLI/bin/symsnapvx -sid $SNAPVX_SID -sg $SNAPVX_SG -name $SNAPVX_NAME establish -ttl -delta $SNAPVX_DAYS -noprompt]"
    /opt/emc/SYMCLI/bin/symsnapvx -sid $SNAPVX_SID -sg $SNAPVX_SG -name $SNAPVX_NAME establish -ttl -delta $SNAPVX_DAYS -noprompt
    if [[ $? != 0 ]]; then
        stamplog "ERROR: symsnapvx Failed."
        ((ERRFLAG++))
        instthaw $HOST ${INSTID}
        continue
    fi

    # Thaw the Cache Instance
    stamplog "[$INSTID][$HOST] - Thawing Cache Instance."
    instthaw ${HOST} ${INSTID}
    if [[ $? != 0 ]]; then
        stamplog "ERROR: instthaw Failed."
        ((ERRFLAG++))
    fi

    # Relink SnapVX Snapshot
    stamplog "[${INSTID}][${HOST}] - [/opt/emc/SYMCLI/bin/symsnapvx -sid $SNAPVX_SID -sg $SNAPVX_SG -lnsg $SNAPVX_LNSG -snapshot_name $SNAPVX_NAME relink -noprompt]"
    /opt/emc/SYMCLI/bin/symsnapvx -sid $SNAPVX_SID -sg $SNAPVX_SG -lnsg $SNAPVX_LNSG -snapshot_name $SNAPVX_NAME relink -noprompt
    if [[ $? != 0 ]]; then
        stamplog "ERROR: symsnapvx Failed."
        ((ERRFLAG++))
        continue
    fi

    stamplog "[$INSTID][$HOST] - Running Post Snap Commands."
    if snapvx_post ${INSTID} ${SNAP_INFO}; then
        stamplog "[${INSTID}][$HOST] - Post completed."
    else
        stamplog "ERROR: [${INSTID}][$HOST] - Post failed."
        ((ERRFLAG++))
        continue
    fi

    stamplog "Finished [${INSTID}][$HOST] - SnapVX Snapshot Creation."

done

# Remove RUNLOCK if running from AUTO
rm -f ${RUNLOCK}

# If any ERRORS were logged, send an email alert.
if [ "${ERRFLAG}" -gt "0" ]; then
    mailerr "- ${ERRFLAG} ERRORS DETECTED"
fi

stamplog "Exiting [$MYNAME]."
exit 0
