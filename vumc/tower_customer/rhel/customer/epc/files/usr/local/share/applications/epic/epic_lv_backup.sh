#!/bin/bash
# 
# Script: epic_lv_backup.sh       
# SVN:
#
# Description:
#
# Contact:
# Jason Minton
# j.minton@vumc.org
# 
# 20160317 KRG - Original Draft
# 20160318 KRG - change for loop to use instances.txt file 
# 20160323 KRG - activate freeze and thaw and mod mail list 1 
# 20160323 KRG - remove mkdir and rmdir commands
# 20160617 j.minton - rewrite
# 20170428 j.minton - Modified to allow for multile LVs per Epic ENV
# 20170605 j.minton - Added sync command. Added logging cleanup.
# 20171009 j.minton - Modified to only do LVM snapshot for Avamar; without TSM arch.
#
################################################################################
# Constant/Global Vars
DTE=`date +%Y-%m-%d.%H%M`
MYNAME=`basename $0`
PATH=/usr/bin:/usr/sbin:/bin
CONF=/epic/conf/epic_lv_backup.conf
LOG=/epic/logs/${MYNAME}_${DTE}.log
EMAIL=`cat ${CONF} | grep ^email | awk -F \: '{print $2}'`
declare -A SNAP_INFO
((ERRFLAG=0))


stamplog() {
    echo "`date +%m/%d/%Y' '%H:%M:%S` $1"
}

mailerr() {
    if [[ ${AUTO} ]]; then
        grep "`date +%m/%d/%Y`.*ERR" ${LOG} | mail -s "${HOST} (${MYNAME}) $1" "${EMAIL}"
    fi
}

instrecover() {
    stamplog "-- Attempting [instthaw] for Instance [$1]."
    /epic/$1/bin/instthaw

    return $?
}

usage() {
    echo "Usage: $MYNAME"
    echo 
    echo "Run in automatic mode based on ${CONF}"
    echo "$MYNAME"
    echo
    echo "Manual command line options:"
    echo
    echo "Run manual snapshot for <INSTID>."
    echo "$MYNAME -e <INSTID>"
    echo
    echo "Run Cleanup Process for <INSTID>."
    echo "$MYNAME -e <INSTID> -c"
    echo 
    echo "Check Status of <INSTID>."
    echo "$MYNAME -e <INSTID> -s"
    echo
}

docleanup() {
    INSTID=$1
    SNAP_INFO=$2

    stamplog "Instance [${INSTID}] - Attempting cleanup process."
    for (( cnt=1; cnt<=$((${SNAP_INFO[CNT]})); cnt++ ))
    do
        # unmount process
        stamplog "Instance [${INSTID}] - Unmounting [${SNAP_INFO[lv_${cnt}_SNAPMOUNT]}]."
        umount ${SNAP_INFO[lv_${cnt}_SNAPMOUNT]}

        # lvremove process
        stamplog "Instance [${INSTID}] - Removing [${SNAP_INFO[lv_${cnt}_SNAPVOL]}]."
        lvremove -f ${SNAP_INFO[lv_${cnt}_SNAPVOL]}
    done
    return 0
}

load_snap_info() {
    VOLS=$1
    ((IDX=0))

    for VOL in $(echo $VOLS | tr "," " " | sed -e 's/^ *//g;s/ *$//g')
    do
        ((IDX++))
        DMPATH=$(echo ${VOL} | awk -F : '{print $1}')
        MNT=$(echo ${VOL} | awk -F : '{print $2}')
        SNAP_INFO[lv_${IDX}_SNAPSIZE]=$(echo ${VOL} | awk -F : '{print $3}')
        SNAP_INFO[lv_${IDX}_SRCVOL]=/dev/mapper/${DMPATH}
        SNAP_INFO[lv_${IDX}_VG]=$(lvs -o vg_name --noheadings ${SNAP_INFO[lv_${IDX}_SRCVOL]} | awk '{print $1}')
        SNAP_INFO[lv_${IDX}_LV]=$(lvs -o lv_name --noheadings ${SNAP_INFO[lv_${IDX}_SRCVOL]} | awk '{print $1}')
        SNAP_INFO[lv_${IDX}_SNAPLV]=${SNAP_INFO[lv_${IDX}_LV]}_snap
        SNAP_INFO[lv_${IDX}_SNAPVOL]=/dev/mapper/${SNAP_INFO[lv_${IDX}_VG]}-${SNAP_INFO[lv_${IDX}_SNAPLV]}
        SNAP_INFO[lv_${IDX}_SNAPMOUNT]=/backup/${MNT}
    done
    SNAP_INFO[CNT]=${IDX}
}


while getopts ?e:isch option; do    
    case $option in
        e) INSTID="$OPTARG";;   # Specify the instance (poc/tst/rel/etc.).
        s) GETSTAT="1";;        # Display Current Status Only.
        c) CLEANUP="1";;        # Run the cleanup steps for the specified INSTID.
        h) usage; exit 0;;      # Help: Show Usage
    esac
done
shift "$((OPTIND-1))"

# If no arguments were passed run in AUTO mode.
if [ "${OPTIND}" -eq "1" ]; then
    AUTO="1"
    touch ${LOG}
    chown epicadm:epicsys ${LOG}
    exec 1>>${LOG} 2>&1
fi

if [[ ${GETSTAT} ]]; then

    lvs -o lv_dm_path,lv_name,lv_active,lv_size,origin,snap_percent,vg_name,vg_size,vg_free,lv_permissions,lv_attr

elif [[ ${CLEANUP} ]] && [[ ${INSTID} ]]; then

    VOLS=$(grep "^${INSTID}[[:blank:]]" ${CONF} | awk '{print $2}')
    load_snap_info ${VOLS} 

    # Run Cleanup 
    docleanup ${INSTID} ${SNAP_INFO}

elif [[ ${AUTO} ]] || [[ ${INSTID} ]]; then

    grep -vE "^#|^email|^[[:blank:]]|^$" ${CONF} | while read -r INST VOLS
    do
        # Check if instance should run.
        if [[ ${AUTO} != "1" ]] && [[ ${INST} != ${INSTID} ]]; then
            continue
        fi
        INSTID=${INST}

        unset SNAP_INFO
        declare -A SNAP_INFO
        load_snap_info $VOLS 

        stamplog "WORKING on [${INSTID}]..."
        docleanup ${INSTID} ${SNAP_INFO}

        # Freeze Cache
        stamplog "Instance [${INSTID}] - Freezing Instance."
        /epic/${INSTID}/bin/instfreeze
        if [[ $? != 0 ]]; then
            stamplog "ERROR: Instance [${INSTID}]. [instfreeze] failed -- SKIPPING."
            ((ERRFLAG++))
            instrecover ${INSTID}
            continue
        fi

        # Flush filesystem
        /bin/sync
        if [[ $? != 0 ]]; then
            stamplog "WARNING: Instance [${INSTID}]. Filesystem I/O flush [sync] failed. Snapshot will proceed."
        fi

        # Create the lvm Snapshot
        for (( cnt=1; cnt<=$((${SNAP_INFO[CNT]})); cnt++ ))
        do
            lvcreate -L ${SNAP_INFO[lv_${cnt}_SNAPSIZE]} -s -n ${SNAP_INFO[lv_${cnt}_SNAPLV]} ${SNAP_INFO[lv_${cnt}_SRCVOL]}
            if [[ $? != 0 ]]; then
                stamplog "ERROR: Instance [${INSTID}]. [lvcreate] failed on [${SNAP_INFO[lv_${cnt}_SNAPLV]}] -- SKIPPING." 
                ((ERRFLAG++))
                instrecover ${INSTID}
                continue 2
            fi
            stamplog "Instance [${INSTID}] - LV [${SNAP_INFO[lv_${cnt}_SNAPLV]}] created."
        done

        # Thaw the Cache Instance
        if instrecover ${INSTID}; then
            stamplog "Instance [${INSTID}] - Thaw Completed Successfully."
        else
            stamplog "ERROR: Instance [${INSTID}]. [instthaw] failed. Proceeding with snapshot for [${INSTID}]."
            ((ERRFLAG++))
        fi

        # Mount the Snapshot
        for (( cnt=1; cnt<=$((${SNAP_INFO[CNT]})); cnt++ ))
        do
            stamplog "Instance [${INSTID}] - mounting [${SNAP_INFO[lv_${cnt}_SNAPVOL]}]."
            mount -o ro -o nouuid ${SNAP_INFO[lv_${cnt}_SNAPVOL]} ${SNAP_INFO[lv_${cnt}_SNAPMOUNT]}
            if [[ $? != 0 ]]; then
                stamplog "ERROR: Instance [${INSTID}]. [mount] failed on [${SNAP_INFO[lv_${cnt}_SNAPVOL]}] to [${SNAP_INFO[lv_${cnt}_SNAPMOUNT]}]."
                ((ERRFLAG++))
                continue 2
            fi
        done

        stamplog "Finished with [${INSTID}]."
    done
else
    usage
fi

if [ "${ERRFLAG}" -gt "0" ]; then
    mailerr "- ${ERRFLAG} ERRORS DETECTED"
fi

exit 0
