- hosts: all
  gather_facts: no
  strategy: free
  vars:
    ad_group: sg-san_adm
    contact: vumc.it.storage@vumc.org

    #
    # Server Types
    #
    epc_hosts: ['tsm05lp']

    #
    # Groups of sudo commands
    #
    commands_epc:
      - { command: '/usr/local/scripts/autoyum, /sbin/lvs, /usr/local/scripts/EpicHomedir.pl', username: '%sg-epc_adm' }
      - { command: 'NOPASSWD: /usr/bin/csession', username: 'epicadm', run_as: 'epicdmn' }
      - { command: 'NOPASSWD: /bin/su - epicdmn', username: 'epicadm' }
      - { command: 'NOPASSWD: /usr/local/share/applications/epic/snapVX_backup.sh', username: 'epicadm' }
      - { command: 'NOPASSWD: /usr/local/share/applications/epic/snapVX_report.sh', username: 'epicadm' }
      - { command: 'NOPASSWD: /usr/local/share/applications/epic/snapVX_relink.sh', username: 'epicadm' }
      - { command: 'NOPASSWD: /opt/emc/SYMCLI/bin/symsnapvx -sid * list *',         username: 'epicadm' }
      - { command: 'NOPASSWD: /bin/cat /app001/avamar/var/clientlogs/*',            username: 'epicadm' }
      - { command: 'NOPASSWD: !/bin/cat /app001/avamar/var/clientlogs/*\ *',        username: 'epicadm' }
      - { command: 'NOPASSWD: !/bin/cat /app001/avamar/var/clientlogs/*..*',        username: 'epicadm' }

  vars_files:
    - ../../common/vars.yml

  tasks:
    - name: Loop through sudo commands and expand as necessary
      set_fact:
        commands: "{{ commands|default([]) + lookup('vars', 'commands_' + item.name) }}"
        cacheable: no
      when: item.cond
      loop:
        - { name: 'epc',     cond: '{{ inventory_hostname_short in epc_hosts }}' }

    - name: tsm pam limits
      pam_limits:
        dest: /etc/security/limits.d/91-nofile.conf
        domain: '{{ item.domain }}'
        limit_type: '{{ item.limit_type }}'
        limit_item: '{{ item.limit_item }}'
        value: '{{ item.value }}'
      when: item.cond
      loop:
        - { domain: 'inst05a', limit_type: '-', limit_item: 'memlock', value: 256,    cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }
        - { domain: 'inst05a', limit_type: '-', limit_item: 'nofile',  value: 524288, cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }
        - { domain: 'inst05a', limit_type: '-', limit_item: 'stack',   value: 40960,  cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }

    - include_tasks: ../groups.yml
      vars:
        group: '{{ item.group }}'
        gid: '{{ item.gid }}'
        additional_groups: '{{ item.additional_groups|default([]) }}'
      when: item.cond
      loop:
        - { group: 'inst05a',  gid: '1174', additional_groups: [{ group: 'emcapp',  gid: '1175' },{ group: 'epicadm', gid: '1600' }], cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }

    - include_tasks: ../users.yml
      vars:
        uid:          '{{ item.uid }}'
        username:     '{{ item.username }}'
        ad_group:     '{{ item.ad_group|default([]) }}'
        group:        '{{ item.group }}'
        comment:      '{{ item.comment|default("Customer App Account") }}'
        home:         '{{ item.home }}'
        app_mode:     '{{ item.app_mode }}'
        contact:      '{{ item.contact|default("vumc.it.storage@vumc.org") }}'
        access_level: '{{ item.access_level|default("LOCAL") }}'
        additional_groups: '{{ item.additional_groups|default([]) }}'
      when: item.cond
      loop:
        - { uid: '1174', username: 'inst05a', group: 'inst05a', home: '/home/inst05a',   app_mode: '0755', access_level: 'ALL', comment: 'tsm System User', ad_group: 'sg-san_adm', cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }
        - { uid: '1175', username: 'emcapp',  group: 'emcapp',  home: '/home/emcapp',    app_mode: '0700', access_level: 'ALL', comment: 'EMC Solutions Enabler', contact: 'estar.ssi@vumc.org', cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }
        - { uid: '1600', username: 'epicadm', group: 'epicadm', home: '/app001/epicadm', app_mode: '0700', access_level: 'ALL', comment: 'EPIC Admin', ad_group: 'sg-bak_usr', contact: 'estar.ssi@vumc.org', cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }

    - block:
      - include_tasks: ../../common/auth.yml
        vars:
          homedir: /home
          service: sssd

      - name: copy customer files
        copy:
          src: "{{ item.src|default('files/' + item.dest) }}"
          dest: "{{ item.dest }}"
          owner: root
          group: root
          mode: "{{ item.mode }}"
        loop:
          - { dest: '/usr/local/share/applications/epic/snapVX_backup.sh', mode: '0744' }
          - { dest: '/usr/local/share/applications/epic/snapVX_relink.sh', mode: '0744' }
          - { dest: '/usr/local/share/applications/epic/snapVX_report.sh', mode: '0744' }

      - include_tasks: ../../common/perms.yml
        vars:
          username: root
          group: root
          dirmode: '0755'
          dironly: 'yes'
          path: /usr/local/share/applications/epic
      when: inventory_hostname_short in ["tsm05lp"]

    - include_tasks: ../../app/autofs/autofs.yml
    - include_tasks: ../../common/cron.yml
      vars:
        username: 'epicadm'
      when: inventory_hostname_short in ["tsm05lp"]

    - name: Install requested packages
      package:
        name: '{{ item.packages }}'
        state: present
        lock_timeout: 180 # wait up to 3 minutes for a lock ansible/ansible#57189
      when: item.cond
      loop:
        - { packages: ['device-mapper-multipath','ksh','libaio','libstdc++.i686','libstdc++.x86_64','libxml2.i686','libxml2.x86_64','numactl','libcmpiCppImpl0','libxslt','libwsman1','openwsman-client','openwsman-server','sblim-sfcb','sblim-sfcc'], cond: "{{ true }}" }
        - { packages: ['iperf','perl-Switch','rsync'], cond: "{{ inventory_hostname_short in ['tsm05lp'] }}" }

    - name: copy custom limits.conf file to prod servers
      copy:
        src: files/etc/security/limits.conf
        dest: /etc/security/limits.conf
        username: root
        group: root
        mode: '0644'
      when: prd == true

    # get all custom sudo commands
    - include_tasks: ../../common/sudoers.yml
    # get any custom sudo commands and specific services
    - include_tasks: ../../common/sudoers.yml
      vars:
        service:  '{{ item.service }}'
        username: '{{ item.username }}'
      when: item.cond
      loop:
       - { service: ['inst05a','agentsvc'], username: 'inst05a', cond: '{{ inventory_hostname_short in ["tsm05lp"] }}' }
