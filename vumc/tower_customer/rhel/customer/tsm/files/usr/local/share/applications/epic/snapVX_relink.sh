#!/bin/bash
 
###########################################################################################
#                                                                                         #
# Mark Shanks, Jason Minton                                                               #
# mark.w.shanks@vumc.org, j.minton@vumc.org                                               #
#                                                                                         #
# Script:                                                                                 #
# /usr/local/share/applications/epic/snapVX_relink.sh - Must run as (root) sudo.          #
#                                                                                         #
# Description:                                                                            #
# Relinks lastest SAN SnapVX Snapshot to storage array for environment refreshe.          #
#                                                                                         #
###########################################################################################

#   FUNCTION: Logging datetime stanps.

snapVX_step()
{
    echo "`date +%m/%d/%Y' '%H:%M:%S` ${1}"
}

#   FUNCTION: Email Errors from AUTO - short description (appended to email subject).

snapVX_mail()
{
    grep -B1 "`date +%m/%d/%Y`.*ERR" ${FILE} | mail -s "${SERVER} (${SCRIPT}) ${1}" "${MAIL}"
}

#   FUNCTION: Setup for Automated/Scheduled run from AUTO.

snapVX_init()
{
    rm -f ${FILE}
    rm -f ${LOCK}

    snapVX_step "SnapVX Process Environment Initialization."

    if [ `whoami` != 'root' ]; then echo "ERROR: Must run as [root]."; exit; fi
    
#   Redirect STDOUT and STDERR to the log file.
    exec 1>>${FILE} 2>&1

#   Create Log File.
    touch ${FILE}
    chown root:epicadm ${FILE}
    chmod 664 ${FILE}

#   Prevent multiple threads from running simultaneously.
    if [ -e "${LOCK}" ]; then

    snapVX_step   "ERROR: LOCK [${LOCK}] exists; Exiting!"
    snapVX_mail "- ERROR: LOCK Prevented Execution."; exit 1; fi

#   Create RUN Lock.
    echo "PID: $$" > ${LOCK}
    chown epicadm:epicadm ${LOCK}
    chmod 664 ${LOCK}

    if [ ! -e "${LOCK}" ]; then snapVX_step "ERROR: Unable to create LOCK [${LOCK}]; proceeding ... "; ((ERRORS++)); fi
}

#   MAIN

if [ ${#} -lt 2 ]; then echo "USAGE: snapVX_relink.sh [PJX,REL,RELVAL,SUPDC1,SUPMO1,SUPDC3,SUPMO3] [link,relink,unlink] [CACHE_EPC?116LP_SNP_YYYYMMDDHHMI]"; exit 1; fi

# Initialize Environment.
ERRORS=0
SERVER=`hostname`
SCRIPT=`basename $0`

DATE=`date +%Y%m%d.%H%M%S`

PATH=/usr/bin:/usr/sbin:/bin
CONF=/app001/epicadm/snapvx/conf/snapVX_relink.conf
LOCK=/app001/epicadm/snapvx/lock/${SERVER}.${SCRIPT}
FILE=/app001/epicadm/snapvx/logs/${SCRIPT}.${DATE}.${1}

MAIL=`cat ${CONF} | grep ^email | awk -F \: ' { print $2 } ' -`

# Initialize Data from SnapVX Configuration File.
CONFIG=`fgrep -w ${1} ${CONF}`

SOURCE=$(     echo ${CONFIG} | awk '      { printf ("%-s", $1) } ' -)
TARGET=$(     echo ${CONFIG} | awk '      { printf ("%-s", $2) } ' -)
HOSTVM=$(     echo ${CONFIG} | awk '      { printf ("%-s", $3) } ' -)
INSTID=$(     echo ${CONFIG} | awk '      { printf ("%-s", $4) } ' -)
SNAPVX=$(     echo ${CONFIG} | awk '      { printf ("%-s", $5) } ' -)
VOLUME=$(     echo ${CONFIG} | awk '      { printf ("%-s", $6) } ' -)

SNAPVX_SID=$( echo ${SNAPVX} | awk -F : ' { printf ("%-s", $1) } ' -)
SNAPVX_SG=$(  echo ${SNAPVX} | awk -F : ' { printf ("%-s", $2) } ' -)
SNAPVX_LNSG=$(echo ${SNAPVX} | awk -F : ' { printf ("%-s", $3) } ' -)
SNAPVX_DAYS=$(echo ${SNAPVX} | awk -F : ' { printf ("%-s", $4) } ' -)

INSTID=$(     echo ${INSTID} | awk      ' { printf ("%-s", (substr ($0,1,3) == "SUP") ? "SUP" : $0); } ' -)

snapVX_step "Working on [${INSTID}][${TARGET}]."

# Get Latest SnapVX Snapshot.
if [ ${1} != "SUPMO3" -a ${1} != "SUPDC3" ]

then SNAPVX_NAME=`/app001/epicadm/snapvx/user/snapVX_report.sh PRDDC1 | fgrep CACHE_${TARGET}_SNP_ | sort -k3 | tail -1 | awk ' { printf ("%-s", $3); } ' -`
else SNAPVX_NAME=`/app001/epicadm/snapvx/user/snapVX_report.sh PRDDC3 | fgrep CACHE_${TARGET}_SNP_ | sort -k3 | tail -1 | awk ' { printf ("%-s", $3); } ' -`

fi

if [ ${3} ]; then SNAPVX_NAME=${3}; fi

# Initialize SnapVX Environment.
snapVX_init

# Relink the SnapVX Snapshot to its target.
snapVX_step "[${INSTID}][${TARGET}] - [/opt/emc/SYMCLI/bin/symsnapvx -sid ${SNAPVX_SID} -sg ${SNAPVX_SG} -lnsg ${SNAPVX_LNSG} -snapshot_name ${SNAPVX_NAME} ${2} -noprompt]"
                                       /opt/emc/SYMCLI/bin/symsnapvx -sid ${SNAPVX_SID} -sg ${SNAPVX_SG} -lnsg ${SNAPVX_LNSG} -snapshot_name ${SNAPVX_NAME} ${2} -noprompt

if [ ${?} != 0 ]; then snapVX_step "ERROR: [${INSTID}][${TARGET}] - [symsnapvx] Failed!"; ((ERRORS++)); fi

snapVX_step "Finished [${INSTID}][${TARGET}] - SnapVX Relink."

# Remove LOCK if running from AUTO.
rm -f ${LOCK}

# If any ERRORS were logged, send an email alert.
if [ "${ERRORS}" -gt "0" ]; then snapVX_mail "- ${ERRORS} ERRORS DETECTED"; fi

snapVX_step "Exiting [$SCRIPT]."

exit 0
