- hosts: all
  gather_facts: no
  strategy: free
  vars:
    uid: 1036
    gid: 1036
    username: adiapp
    group: adiapp
    ad_group: []
    access_level: LOCAL
    comment: Customer App Account
    home: /app001
    contact: vumc.it.adi-is@vumc.org
    adi_accessVU_prd: ['adi1000lp','adi3000lp']
    adi_accessVU_srv: '{{ adi_accessVU_prd }}'
    adi_avi_dev: ['adi12ld','adi12lt','adi52ld','adi52lt']
    adi_avi_srv: '{{ adi_avi_dev }}'
    adi_ping_dev: ['adi32ld','adi33ld']
    adi_ping_test: ['adi32lt','adi33lt']
    adi_ping_prd: ['adi32lp','adi33lp','adi82lp','adi83lp']
    adi_ping_srv: '{{ adi_ping_dev + adi_ping_test + adi_ping_prd }}'
    adi_svn_prd: ['adi11lp']
    adi_svn_srv: '{{ adi_svn_prd }}'
    adi_sailpoint_dev: ['adi1001ld']
    adi_sailpoint_uat: ['adi1001lt','adi1002lt']
    adi_sailpoint_prd: ['adi1001lp','adi3001lp']
    adi_sailpoint_srv: '{{ adi_sailpoint_dev + adi_sailpoint_uat + adi_sailpoint_prd }}'

    access_adi_dev: 'adi1000lp.hs.it.vumc.io adi3000lp.hs.it.vumc.io'

  vars_files:
    - ../../common/vars.yml

  tasks:
    - name: Loop through access levels and expand as necessary
      set_fact:
        access_level: "{{ lookup('vars', 'access_' + item.name) }} {{ access_level }}"
      when: item.cond
      loop:
        - { name: 'adi_dev', cond: '{{ inventory_hostname_short in adi_avi_dev }}' }

    - name: Loop through services to add to sudoers
      set_fact:
        service: "{{ service|default([]) + query('items', item.name) }}"
        cacheable: no
      when: item.cond
      loop:
        - { name: ['ping','ping-admin'], cond: '{{ inventory_hostname_short in adi_ping_srv }}' }
        - { name: ['ping-dr'],           cond: '{{ inventory_hostname_short in ["adi82lp"] }}' }
        - { name: ['ping-qa'],           cond: '{{ inventory_hostname_short in ["adi33lt"] }}' }
        - { name: 'startjboss',          cond: '{{ inventory_hostname_short in ["adi12ld","adi12lt","adi52ld","adi52lt"] }}' }

    - name: expand ad_group as necessary
      set_fact:
        # convert existing ad_group to string, add the new item, then convert back to a list
        ad_group: '{{ ad_group + item.group }}'
        cacheable: no
      when: item.cond
      loop:
        - { group: ['sg-adi_avi_adm'],       cond: '{{ inventory_hostname_short not in adi_ping_srv }}' }
        - { group: ['sg-adi_ping_vumc_adm'], cond: '{{ inventory_hostname_short in adi_ping_srv }}' }
        - { group: ['sg-adi_sailpoint_dev'], cond: '{{ inventory_hostname_short in ( adi_sailpoint_dev + adi_sailpoint_uat ) }}' }

    - include_tasks: ../groups.yml
    - include_tasks: ../users.yml

    - block:
        - include_tasks: ../../common/cron.yml
      when: '(inventory_hostname_short in adi_svn_srv) or
            (inventory_hostname_short in adi_avi_srv) or
            (inventory_hostname_short in adi_ping_srv) or
            (inventory_hostname_short in adi_accessVU_srv)'

    - include_tasks: ../../app/auditd/auditd.yml
    - include_tasks: ../../app/autofs/autofs.yml

    - name: Allow java cacerts to be editted
      file:
        path: /etc/pki/java/cacerts
        mode: '0644'
        owner: '{{ username }}'
        group: root
      when: inventory_hostname_short in adi_avi_srv

    - name: Increasing user process limit
      pam_limits:
        dest: /etc/security/limits.d/90-nproc.conf
        domain: '{{ item.domain }}'
        limit_type: '{{ item.limit_type }}'
        limit_item: '{{ item.limit_item }}'
        value: '{{ item.value }}'
      when: inventory_hostname_short in ['adi12lt','adi12ld','adi52lt','adi1000lp','adi3000lp']
      loop:
        - { domain: '*',      limit_type: 'soft', limit_item: 'nproc', value: 1024 }
        - { domain: 'root',   limit_type: 'soft', limit_item: 'nproc', value: 'unlimited' }
        - { domain: 'adiapp', limit_type: 'soft', limit_item: 'nproc', value: 10240 } #T00685761
        - { domain: 'adiapp', limit_type: 'hard', limit_item: 'nproc', value: 10240 } #T00685761

    - name: Copy systemd unit files
      template:
        src: '{{ item.src|default("files/" + item.dest) }}'
        dest: '{{ item.dest }}'
        mode: '0644'
        owner: root
        group: root
      when: item.cond
      loop:
        - { dest: '/etc/systemd/system/ping.service',       cond: '{{ inventory_hostname_short in ( adi_ping_srv ) }}' }
        - { dest: '/etc/systemd/system/ping-dr.service',    cond: '{{ inventory_hostname_short in ["adi82lp"] }}' }
        - { dest: '/etc/systemd/system/ping-qa.service',    cond: '{{ inventory_hostname_short in ["adi33lt"] }}' }
        - { dest: '/etc/systemd/system/ping-admin.service', cond: '{{ inventory_hostname_short in ( adi_ping_srv ) }}' }
      register: systemd

    - include_tasks: ../../common/sudoers.yml
      vars:
        commands:
          - { username: '%sg-adi_avi_adm', command: '/usr/sbin/tcpdump' }

    - name: Copy startjboss init file
      copy:
        src: files/etc/init.d/startjboss
        dest: /etc/init.d/startjboss
        mode: '0755'
        owner: root
        group: root
      when: inventory_hostname_short in ['adi12ld','adi12lt','adi52ld','adi52lt']

    - name: Install requested packages
      package:
        name: '{{ item.packages }}'
        state: present
        lock_timeout: 180 # wait up to 3 minutes for a lock ansible/ansible#57189
      loop:
        - { hosts: '{{ adi_avi_srv }}',       packages: ['java-1.6.0-openjdk','perl-DBI'] }
        - { hosts: '{{ adi_svn_srv }}',       packages: ['openldap-clients','openldap-servers','perl-ExtUtils-Embed'] }
        - { hosts: '{{ adi_avi_dev }}',       packages: ['nmap'] }
        - { hosts: '{{ adi_accessVU_prd }}',  packages: ['libXext','libXrender','libXtst'] }
        - { hosts: '{{ adi_sailpoint_srv }}', packages: ['java-1.8.0-openjdk','git','curl','unzip','wget'] }
        - { hosts: '{{ adi_ping_srv }}',      packages: ['java-11-openjdk','ant'] }
        - { hosts: '{{ adi_sailpoint_srv }}', packages: ['java-11-openjdk'] }
      when: inventory_hostname_short in item.hosts

    - name: Reload systemd config
      systemd:
        daemon_reload: yes
      when: systemd.changed
