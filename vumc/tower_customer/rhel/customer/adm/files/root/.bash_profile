# .bash_profile

LOGNAME=$( /usr/bin/logname 2> /dev/null )
HOSTNAME=$( /bin/hostname )

eval `ssh-agent`

echo "Accessing root on this server requires a Pre-Approved Change."
echo
echo "What is your Change number?"
read -r change

if [ -z "$change" ]; then
    echo "Change number required. Aborting."
    exit
fi

echo
echo "Provide a short description of your work."
read -r desc

if [ -z "$desc" ]; then
    echo "Change description required. Aborting."
    exit
fi

logger -t "root_audit" -p authpriv.notice -f /var/log/secure "USER=$LOGNAME ; CHANGE=$change ; DESC=$desc"
echo

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH
