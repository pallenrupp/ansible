#!/app002/bot/bin/python3
import calendar
import csv
import dateparser
import datetime
import re
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sys
import time
import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning

# disable SSL Warnings
requests.packages.urllib3.disable_warnings()

# constants
SVC_USER  = "{{ svc_username }}"
SVC_PASS  = "{{ svc_password }}"

# used for f5 api
F5_CACHE_TIME           = 9 * 3600 # number of hours to cache data from F5 * 3600 to convert to seconds

try:
   patchperiod=sys.argv[1]
except:
   patchperiod='unset'

# confluence access
CONFLUENCE_SRV = "https://confluence.app.vumc.org"
admin={}
override={}

# function that will pull a json file from confluence and make it available
# via admin and override
def updatepatchroster():
    global override
    global admin
    try:
        admin['last_read']
    except:
        admin['last_read']=0
    # only update this if it hasn't been pulled recently
    if time.time() - admin['last_read'] > F5_CACHE_TIME:
        # set url for page we want attachments from
        patchurl='/rest/api/content/97584108/child/attachment'
        # retrieve a list of attachments from the page
        r=requests.get(CONFLUENCE_SRV+patchurl, auth=HTTPBasicAuth(SVC_USER, SVC_PASS), verify=False)
        if r.status_code == 200 and len(r.json()['results']) > 0:
            downloadurl=r.json()['results'][0]['_links']['download']
            # retrieve the json attachment and make the payload available
            r=requests.get(CONFLUENCE_SRV+downloadurl, auth=HTTPBasicAuth(SVC_USER, SVC_PASS), verify=False)
            if r.status_code == 200 and len(r.json()) > 0:
                override=r.json()['override']
                admin=r.json()['admin']
                admin['last_read']=time.time()

# function to find the patch dates for any given month
def calcdates(date_obj=datetime.datetime.now(),last=True):
    updatepatchroster()
    c = calendar.Calendar(firstweekday=calendar.SUNDAY)
    year=date_obj.year
    month=date_obj.month
    thismonth=str(year)+"-"+str("{:02d}".format(month))
    if thismonth in override:
        nonprod = dateparser.parse(override[thismonth]['nonprod']).date()
        prod = dateparser.parse(override[thismonth]['prod']).date()
        critical = dateparser.parse(override[thismonth]['critical']).date()
    else:
        monthcal = c.monthdatescalendar(year,month)
        nonprod = [day for week in monthcal for day in week if day.weekday() == calendar.THURSDAY and day.month == month][0]
        prod = nonprod + datetime.timedelta(days=14)
        critical = nonprod + datetime.timedelta(days=17)
    if last:
        null,null,lastmonth,null=calcdates(date_obj.replace(day=1) - datetime.timedelta(days=1),False)
    else:
        lastmonth=None
    return nonprod, prod, critical, lastmonth

nonprod_d, prod_d, critical_d, last = calcdates()

year=nonprod_d.year
month=nonprod_d.month

margin = datetime.timedelta(days = 2)
today = datetime.date.today()

if today - margin <= nonprod_d <= today + margin and patchperiod == 'nonprod':
    print ('nonprod patch week')
    sys.exit(0)

if today - margin <= prod_d <= today + margin and patchperiod == 'prod':
    print ('prod patch week')
    sys.exit(0)

sys.exit(1)
