#!/bin/perl -w

# $Id: GetCIsByWorkgroup.pl,v 1.1 2019/01/16 0:00:00 shutemh Exp
# RUNDECK INTEGRATION SCRIPT USED TO QUERY CMDB BY WORKGROUP AND GENERATE JOB CONFIG FILE FOR Auto_Patch_Oracle (resources.xml)


use strict;

use HTTP::Request::Common;
use LWP::UserAgent;
use XML::LibXML;


# PEGASUS API URL AND KEY (USING PRODUCTION)
my $PegasusURL = "https://pegasus.vumc.org/services/configuration.asmx"; #PRODUCTION KEY
my $PegasusKEY = "67930DECA1EE4081A4ED12B34F2748C6"; #PRODUCTION KEY


# STATIC VARIABLES
my $xml_CIWorkgroup = "VUMC IT LINUX";


# PATH AND FILE TO ORACLE AUTO PATCH PROJECT
my $xml_file = "/app001/rundeck/projects/Auto_Patch_Oracle/etc/resources.xml";


# OPEN FILE FOR OVERWRITE
open (RESULTSFILE, "> $xml_file");


# POST DATA
my $userAgent = LWP::UserAgent->new(agent => 'perl post');

my $message = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:con=\"http://itsm.vanderbilt.edu/services/configuration/\">
    <soap:Header/>
    <soap:Body>
        <con:GetCIsByWorkgroup>
            <con:apiKey>$PegasusKEY</con:apiKey>
            <con:workgroupName>$xml_CIWorkgroup</con:workgroupName>
        </con:GetCIsByWorkgroup>
    </soap:Body>
    </soap:Envelope>";


if ($message) {


    # POST PAYLOAD AND LOG RESULTS 
    my $response = $userAgent->request(POST $PegasusURL,
    Content_Type => 'text/xml',
    Content => $message);


    # PARSE XML AND REMOVE INVALID ATTRIBUTES SENT FROM PEGASUS
    my $xml_results = $response->content;
    $xml_results =~ s/xmlns=\"http:\/\/itsm.vanderbilt.edu\/services\/configuration\/\"//;
    my $dom = XML::LibXML->load_xml(string => $xml_results);


    # PRINT XML HEADER
    print RESULTSFILE "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<project>\n";


    foreach my $record ($dom->findnodes('//CIModelExtension')) {

        my $CIName = lc($record->findvalue('./Name'));
        my $IntegrationID = lc($record->findvalue('./IntegrationID'));
        my $MaintenanceContract = $record->findvalue('./MaintenanceContract');
        my $StatusName = $record->findvalue('./StatusName');


            # PRINT XML HOSTS DATA FOR INSTALLED AND "ORA_" MAINTENANCE CONTRACT ONLY
            if ( $StatusName eq "Installed" && $MaintenanceContract =~ /^ORA_/ ) {

                print RESULTSFILE "  <node name=\"$CIName\" tags=\"$MaintenanceContract\" hostname=\"$IntegrationID\" username=\"rdkapp\"/>\n";

            }

    }


    # PRINT XML FOOTER
    print RESULTSFILE "</project>";


    # INSPECT PEGASUS SUCCESS RESPONSE AND EXIT FAIL IF NOT FOUND
    if ( $response->as_string =~ m/<Success>false<\/Success>/ ) {

        exit 1;

    }

}

exit;
