#!/usr/bin/perl -w

#
# 2015/01/16 CHANGED "wog" TO VUIT UNIX FROM AI UNIX - SHUTEMH
#

use strict;

use LWP::UserAgent;
use XML::Simple;
use Data::Dumper;

open (RESULTSFILE, "> /usr/local/scripts/hostnames.txt");

my $ua = new LWP::UserAgent;
my @servers = ();
my @blacklist = ();
my $blacklisted = 0;

#open (F, "blacklist.in") || die "Could not open blacklist.in: $!\n"; 
open (F, "/usr/local/scripts/hostlist/blacklist.in") || die "Could not open blacklist.in: $!\n"; 
while (my $line = <F>) {
    chomp($line);
    $line =~ s/\s.*//;
    push (@blacklist, $line);
}
close F; 
#print Dumper \@blacklist;


my $response
= $ua->post('https://aida.mc.vanderbilt.edu/itsswebservices/configitems.asmx/GetCIByWorkGroup',
{ username => 'eaiunix',
password => 'SskA6j0Bf0$OC!yt&yc',
wog => 'VUMC IT LINUX',
});

my $content = $response->content;

my $xml_ref = XMLin("$content",KeyAttr=>[]);


for my $i ( 0 .. $#{ $xml_ref->{'diffgr:diffgram'}{'NewDataSet'}{'Table1'} } ) {

    if ( $xml_ref->{'diffgr:diffgram'}{'NewDataSet'}{'Table1'}[$i]{'ISTATUS'} eq "Installed" ) {

        if ( ( $xml_ref->{'diffgr:diffgram'}{'NewDataSet'}{'Table1'}[$i]{'SUBTYPE'} =~ m/^servers/ ) ) {
            $blacklisted = 0;
            foreach my $bl (@blacklist) {
                if ( lc( $xml_ref->{'diffgr:diffgram'}{'NewDataSet'}{'Table1'}[$i]{'NAME'} ) =~ m/^$bl/ ) {
                    #print "found " . lc( $xml_ref->{'diffgr:diffgram'}{'NewDataSet'}{'Table1'}[$i]{'NAME'} ) . "\n";
                    $blacklisted = 1;
                }
            }

            if ( not $blacklisted ) {
                push @servers, lc( $xml_ref->{'diffgr:diffgram'}{'NewDataSet'}{'Table1'}[$i]{'NAME'} );
            }
        }
    };

}

@servers = sort(@servers);

foreach (@servers) {
    print RESULTSFILE "$_\n";
}

