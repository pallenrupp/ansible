#!/usr/bin/python3
import re
import requests
import xmltodict

blacklist=[]
f = open("/usr/local/scripts/hostlist/blacklist.in", "r")
for x in f.readlines():
    blacklist.append(re.sub('\s.*','',x))


url = "https://pegasus.vumc.org/services/configuration.asmx/GetCIsByWorkgroup?apiKey=67930DECA1EE4081A4ED12B34F2748C6&workgroupName=VUMC IT LINUX"
r = requests.get(url)

activehosts=[]
if r.status_code == 200:
    xmldict=xmltodict.parse(r.text)
    if xmldict['ConfigurationResponse']['Success']=="true":
        for x in xmldict['ConfigurationResponse']['CIData']['CIModelExtension']:
            if x['StatusName'] == 'Installed':
                if re.match('^servers',x['SubTypeName']):
                    add=True
                    for b in blacklist:
                        if re.match('^'+b,x['Name'], re.IGNORECASE):
                            add=False
                            break
                    if add:
                        activehosts.append(x['Name'].lower())


with open('/usr/local/scripts/hostnames.txt', 'w') as f:
    for host in activehosts:
        f.write('%s\n' % host)
