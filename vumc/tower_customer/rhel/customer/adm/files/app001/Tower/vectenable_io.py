#!/usr/bin/python3.6
import argparse
import re
import requests
import sys

try:
    from tenable.io import TenableIO
except:
    raise ImportError('TenableSC module required, please install pytenable with pip')

#process command line arguments
parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description="Script to launch Tenable SC scans.  Please pass a hostname or list of hostnames to launch scan.")
parser.add_argument("-d", "--debug",   help="Enable Debug Output", default=False, action="store_true")
hostortower = parser.add_mutually_exclusive_group(required=True)
hostortower.add_argument("-i", "--jobid",   help="JobID from Tower to get inventory from", default=None, action="append")
hostortower.add_argument('hostname', nargs="?", default=None, help="Hostname to scan", action="append")
group = parser.add_mutually_exclusive_group(required=False)
group.add_argument("-a", "--alpha",   help="Scan will be against the Dev and Test group targets.", default=False, action="store_true")
group.add_argument("-b", "--bravo",   help="Scan will be against the Production group targets.", default=False, action="store_true")
group.add_argument("-c", "--charlie", help="Scan will be against the Critical/Clinical group targets.", default=False, action="store_true")
group.add_argument("-z", "--zulu",    help="Scan will be against the Zulu group targets.", default=False, action="store_true")
group.add_argument("-n", "--names",   help="Set scan title to hostnames", default=False, action="store")

args = parser.parse_args()

tower_url='https://tower.app.vumc.org'
headers={'Authorization': 'Bearer myiVsubfQcj76DBPVfOwzKrDLleU75'}

def get_all(api_str):
    url=tower_url+api_str
    r=requests.get(url, headers=headers)

    results=[]
    if r.json()['next']:
        results=results+r.json()['results']+get_all(r.json()['next'])
    else:
        results=r.json()['results']

    return results


# make sure we just have a number
if args.jobid:
    if re.match('[0-9]+',args.jobid[0]):
        if not (args.alpha or args.bravo or args.charlie or args.zulu or args.names):
           print ("error: one of the arguments -a/--alpha -b/--bravo -c/--charlie -z/--zulu -n/--names is required")
           sys.exit(1)
        towerjobid=re.match('[0-9]*',args.jobid[0])[0]

        url='/api/v2/jobs/'+str(towerjobid)+'/job_host_summaries/'

        args.hostname=[]

        x = requests.get(tower_url+url, headers=headers)
        if x.status_code != 200:
            print ('Tower job id: '+str(towerjobid)+' returned '+str(x.status_code)+' for job status')
            sys.exit(1)
        for y in get_all(url):
           args.hostname.append(y['summary_fields']['host']['name'])

    else:
        print ('Invalid tower job id')
        sys.exit(1)

jobname = 'VUMC IT Linux - Specific Host Scan'
jobname = 'VUMC IT Linux Test/Dev'          if args.alpha   else jobname
jobname = 'VUMC IT Linux Production'        if args.bravo   else jobname
jobname = 'VUMC IT Linux Clinical/Critical' if args.charlie else jobname
jobname = 'VUMC IT Linux Zulu Group'        if args.zulu    else jobname
if args.names:
    if args.names != "False":
        jobname = args.names
    else:
        jobname = ', '.join(args.hostname)

if args.debug:
    print ('jobname: '+jobname)
    print ('hostname: '+str(args.hostname))
    print ('alpha: '+str(args.alpha))
    print ('bravo: '+str(args.bravo))
    print ('charlie: '+str(args.charlie))
    print ('zulu: '+str(args.zulu))

    print ('scan = sc.scans.edit(318,targets='+str(args.hostname)+',name="'+jobname+'")')
    print ('sc.scans.launch(318)')
else:
    #Establish session with Tenable SC instance as ScanAPI user
    sc = TenableIO('2ffc522d31addc1997566864eb28457af90e5a9d312fa08eca4d1a8738d286e5', '40072538fa0165a07e7786b1ae1b885be8dc18f654968644200b6b8cd2354534')

    scan = sc.scans.configure(227,targets=args.hostname,name=jobname)
    sc.scans.launch(227)
