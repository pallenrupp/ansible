#!/bin/python
import argparse
import requests
import sys
import syslog
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning

headers={'Authorization': 'Bearer myiVsubfQcj76DBPVfOwzKrDLleU75'}

# set up the command line options
parser = argparse.ArgumentParser(description='Python script designed to disable hosts quickly in tower when being decommissioned')
parser.add_argument('-s',"--server", required=True, default=None, help='short server name', action="append")

args = parser.parse_args()

FQDN = args.server[0]

# search for server id by short name
url='https://tower.app.vumc.org/api/v2/hosts/?name__icontains='+FQDN
r=requests.get(url, headers=headers)
results=r.json();
# if we have exactly 1 result disable it
if results['count'] == 1:
  for x in results['results']:
    # throw some debugging to the console if run on the cli and to syslog for a record
    print ('disabling '+x['name']+'('+str(x['id'])+') in tower')
    syslog.syslog('disabling '+x['name']+'('+str(x['id'])+') in tower')
    url='https://tower.app.vumc.org/api/v2/hosts/'+str(x['id'])+'/'
    disablehost={'inventory':x['inventory'],'enabled':False}
    r=requests.patch(url, headers=headers, json=disablehost)
