#!/bin/python
import argparse
import requests
import sys
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning

headers={'Authorization': 'Bearer myiVsubfQcj76DBPVfOwzKrDLleU75'}

# set up the command line options
parser = argparse.ArgumentParser(description='Python script designed to add hosts quickly to tower before nagios errors on them')
parser.add_argument('-s',"--server", required=True, default=None, help='fqdn server name', action="append")
parser.add_argument('-e',"--environment", required=True, default=None, help='zulu, alpha, bravo, charlie #{target.customAttribute[\'CI Label RFID\']}', action="append")

args = parser.parse_args()

FQDN = args.server[0]
ENVIRONMENT = args.environment[0]

createhost={'name':FQDN}
url='https://tower.app.vumc.org/api/v2/inventories/9/hosts/'
r=requests.post(url, headers=headers, json=createhost)
newhostid=r.json()['id']
print ('Host: '+FQDN+'('+str(newhostid)+') created')


url='https://tower.app.vumc.org/api/v2/inventories/9/groups/?name__icontains='+ENVIRONMENT+'&format=json'
r=requests.get(url, headers=headers)
for x in r.json()['results']:
    group={'id': x['id']}
    url='https://tower.app.vumc.org/api/v2/hosts/'+str(newhostid)+'/groups/'
    r=requests.post(url, headers=headers, json=group)
