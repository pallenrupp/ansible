#!/bin/python3.6
import datetime
import requests
from requests.auth import HTTPBasicAuth
import csv
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sys
try:
    import dns.resolver
except ImportError or ModuleNotFoundError:
    print ('dnspython module is missing run pip3 install dnspython')
    sys.exit(1)

IB_WAPI_M = "v2.11.1"
IB_USER_M = "{{ svc_username }}"
IB_PASS_M = "{{ svc_password }}"
IB_SRVR_M = "infoblox.app.vumc.org"

srgs='infoblox-verification'

# Get list of all external domains from infoblox, include fqdn and list of shared record groups
def getdomains():
    ipurl = "https://"+IB_SRVR_M+"/wapi/"+IB_WAPI_M+"/zone_auth?view=External&_return_fields=fqdn,srgs,comment&_return_type=json"
    r=requests.get(ipurl, auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M))
    return r.json()

#def getdomains():
#    domains=[]
#    ipurl = "https://infoblox.app.vumc.org/wapi/v2.11.1/zone_auth?view=External&_return_fields=fqdn,srgs&_return_type=json&fqdn=tundrahelix.org"
#    r=requests.get(ipurl, auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M))
#    domains.append(r.json()[0])
#    ipurl = "https://infoblox.app.vumc.org/wapi/v2.11.1/zone_auth?view=External&_return_fields=fqdn,srgs&_return_type=json&fqdn=vumc.org"
#    r=requests.get(ipurl, auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M))
#    domains.append(r.json()[0])
#    return domains

# loop over list of external domains and add shared record group 'infoblox-validation' to their groups if it isn't currently a member
def associatedomain(domain):
    domain['srgs'].append(srgs)
    r=requests.put("https://"+IB_SRVR_M+"/wapi/"+IB_WAPI_M+"/"+domain['_ref'], auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M), json={'srgs':domain['srgs']})
    if r.status_code != 200:
       print ('error updating zone '+domain['fqdn'])
       return False
    return True

# create a dns resolver and point it to use google dns
res = dns.resolver.Resolver()
res.nameservers = ['8.8.8.8']

failed={}

# loop over external domains
for domain in getdomains():
    # the external domain list includes in-addr-arpa zones, skip them
    if '/' not in domain['fqdn']:
        # check if domain is part of our shared record group
        if srgs not in domain['srgs']:
           # if domain fails to add to shared record group skip to next domain, do not attempt to verify
           if not associatedomain(domain):
               continue
        # attempt to look up our audit record, print failures only
        try:
            TXT=res.resolve('owneraudit2022.'+domain['fqdn'],'TXT').rrset.to_text()
        except:
            TXT='broken'
        try:
            COMMENT=domain['comment']
        except:
            COMMENT=''
        if 'verified' not in TXT:
            NAMESERVERS=[]
            # Do record lookup
            try:
                NSLOOKUP=res.resolve(domain['fqdn'],'NS').rrset
            # Record any errors
            except Exception as e:
                e_type, e_value, e_traceback = sys.exc_info()
                NSLOOKUP=str(e_type.__name__)+': '+str(e_value)
            # If we didn't get an error list the name servers
            if isinstance(NSLOOKUP,str):
                NAMESERVERS=[NSLOOKUP]
            else:
                for x in res.resolve(domain['fqdn'],'NS').rrset:
                    NAMESERVERS.append(x.to_text())
            failed[domain['fqdn']]={'comment': COMMENT, 'ns': NAMESERVERS}

# If there are any records output, write them to a csv
if len(failed) > 0:
    str_output = ''
    with open('/tmp/domain_verification.csv', 'w') as csvfile:
        wr = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
        for x in sorted(failed):
            str_output+='<b>'+x+'</b> ('+failed[x]['comment']+'):<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+' '.join(failed[x]['ns'])+'<br><br>'
            line=""
            line+=x+','+failed[x]['comment']+','+" ".join(failed[x]['ns'])
#            print (x+' '+failed[x]['comment']+' '+" ".join(failed[x]['ns']))
            wr.writerow(line.split(','))


    fromaddr = "Infoblox Report <hsorch@adm06lp.hs.it.vumc.io>"
    toaddr = "vumc.it.linux@vumc.org"
    ccaddr = "software.store@vumc.org"
#    toaddr = "wilbur.longwisch@vumc.org"
#    ccaddr = ""
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Cc'] = ccaddr
    msg['Subject'] = "Infoblox External Domain Validation Results "+str(datetime.date.today())

    body = "The following domains are defined in the external view and failed validation.<br><br><br>"+str_output
    msg.attach(MIMEText(body, 'html'))

    with open("/tmp/domain_verification.csv", "rb") as fil:
        part = MIMEApplication(
            fil.read(),
            Name=basename("domain_verification.csv")
        )
    # After the file is closed
    part['Content-Disposition'] = 'attachment; filename="%s"' % basename("domain_verification.csv")
    msg.attach(part)

    server = smtplib.SMTP()
    server.connect()
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()
