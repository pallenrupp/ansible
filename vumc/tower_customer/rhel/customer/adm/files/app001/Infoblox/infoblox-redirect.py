#!/bin/python3.4
import argparse
import requests
from requests.auth import HTTPBasicAuth
import sys

parser = argparse.ArgumentParser(description='Python script to automate adding new domains to the redirection group')
parser.add_argument('-d',"--domain", required=True, default=None, help='testdomain.com', action="store")
parser.add_argument('-t',"--ticket", required=True, default=None, help='Txxxxx', action="store")

args = parser.parse_args()

error = False
domain = args.domain
comment = args.ticket
srgs=['domain-redirect']

SVC_USER  = "{{ svc_username }}"
SVC_PASS  = "{{ svc_password }}"
IB_USER_M = SVC_USER
IB_PASS_M = SVC_PASS
IB_SRVR_M = "infoblox.app.vumc.org"
IB_WAPI_M = "v2.11.1"


ipurl='https://'+IB_SRVR_M+'/wapi/'+IB_WAPI_M+'/zone_auth'

payload={'fqdn':domain,'comment':comment,'srgs':srgs,'view':'External','ns_group':'External'}
er=requests.post(ipurl, auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M), json=payload)

payload={'fqdn':domain,'comment':comment,'srgs':srgs,'view':'Internal','ns_group':'VUMC-standard'}
ir=requests.post(ipurl, auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M), json=payload)

if ir.status_code == 201 and er.status_code == 201:
  ipurl='https://'+IB_SRVR_M+'/wapi/'+IB_WAPI_M+'/grid?name~=Vanderbilt&_return_type=json'
  r=requests.get(ipurl,auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M))
  if r.status_code == 200:
    gridref=r.json()[0]['_ref']
    ipurl='https://'+IB_SRVR_M+'/wapi/'+IB_WAPI_M+'/'+gridref+'?_function=restartservices'
    payload={"restart_option": "RESTART_IF_NEEDED","services": ["DNS"]}
    restart=requests.post(ipurl, auth=HTTPBasicAuth(IB_USER_M, IB_PASS_M), json=payload)
    if restart.status_code == 200:
      print ('OK')
    else:
      print (restart.text)
else:
  print ("error")
  print (er.text)
  print (ir.text)

if error:
  sys.exit(1)
else:
  sys.exit(0)
