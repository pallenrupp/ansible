#!/bin/python3
import mechanicalsoup
import os
import re
import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning

userid = '{{ stonebranch_username }}'
passwd = '{{ stonebranch_password }}'
downloadpath = '/app001/Stonebranch/downloads/'

towerheaders={'Authorization': 'Bearer myiVsubfQcj76DBPVfOwzKrDLleU75'}
towerurl='https://tower.app.vumc.org/api/v2/job_templates/504/survey_spec/'

headers = {"Accept-Encoding":"gzip, deflate", "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "DNT":"1","Connection":"close", "Upgrade-Insecure-Requests":"1"}
# Impersonate firefox so we don't look directly like a script
browser = mechanicalsoup.StatefulBrowser(user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:74.0) Gecko/20100101 Firefox/74.0",)
browser.session.headers.update(headers)

# Open browser to start the session and obtain the secret token
r=browser.open('https://stonebranch.zendesk.com/access/unauthenticated')
page = browser.get_current_page()
postdata={}
# extract the pieces of data that the client submits via JS in a regular browser session
for x in page.findAll('script'):
   if x.has_attr('src'):
       if 'host' in x['src']:
           for y in x.attrs:
               if 'data-' in y:
                   postdata[y.replace('data-','')]=x[y]

# Add in our special pieces of data and the action of sign in
postdata['commit']="Sign+in"
postdata['user[email]']=userid
postdata['user[password]']=passwd

# get the magic token and add it to our post data
token=re.search('meta\ name="csrf-token"\ content="(.*)"\ ',r.text)[1]
postdata['authenticity_token']=token

# log in and record the session to a variable we can reuse
browser.post('https://stonebranch.zendesk.com/access/login',data=postdata)
good_login=browser.session

# set up a new browser with our logged in session
authedbrowser=mechanicalsoup.StatefulBrowser(session=browser.session)
# goto the download page
authedbrowser.open('https://stonebranch.zendesk.com/hc/en-us/articles/115004370689-Universal-Agent-Universal-Data-Mover')
page = authedbrowser.get_current_page()
downloads={}
# find all the download links where the text matches what we want to download
for a in page.findAll('a'):
    if re.search('^Linux((?!Debian|PPC|System).)*$',a.text):
        downloads[a.attrs['href'].split('/').pop()]=a.text

# create our download location if it doesn't exist
if not os.path.exists(downloadpath):
    os.makedirs(downloadpath)

# loop through the links and download anything that doesn't exist on disk
updated=False
for d in downloads:
    if not os.path.exists(downloadpath+d):
        authedbrowser.download_link(link_text=downloads[d],file=downloadpath+d)
        updated=True

# scan the disk to see what versions we have available
available=[]
for x in os.scandir(path=downloadpath):
    if x.is_file():
        available.append(x.name)

# Update the tower survey if we downloaded a new version
if updated:
    # connect to tower and get the survey spec
    r=requests.get(towerurl, headers=towerheaders)
    survey=r.json()

    # loop over the survey questions and update the "version" question with our
    # available list
    for x,q in enumerate(survey['spec'], start=1):
        if q['variable'] == 'installer':
            q['choices']="\n".join(available)

    # update the survey
    requests.post(towerurl, json=survey, headers=towerheaders)
    print ('Updated survey on https://tower.app.vumc.org/#/templates/job_template/504')
