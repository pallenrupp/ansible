#!/bin/python3
import psutil, os, sys, json, calendar, time, re;
from datetime import datetime, timedelta;
import requests
import socket
import syslog
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning

# 15 minutes in seconds
killtime=15*60

def get_lock(process_name):
    # Without holding a reference to our socket somewhere it gets garbage
    # collected when the function exits
    get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    try:
        get_lock._lock_socket.bind('\0' + process_name)
    except socket.error:
        for proc in psutil.process_iter():
            try:
                if proc.name() == 'nagios_downtime':
                    if time.time() - proc.create_time() > killtime:
                        syslog.syslog('Terminating '+proc.name()+', started at '+str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(proc.create_time())))+'  runtime: '+str(time.time() - proc.create_time())+' secs')
                        proc.terminate()
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        syslog.syslog('script still running, exit')
        sys.exit()

def get_all(api_str):
    url='https://tower.app.vumc.org'+api_str
    r=requests.get(url, headers=headers)
    results=[]
    if r.json()['next']:
        results=results+r.json()['results']+get_all(r.json()['next'])
    else:
        results=r.json()['results']
    return results

get_lock('nagios_downtime')

SVC_USER  = "{{ svc_username }}"
SVC_PASS  = "{{ svc_password }}"

headers={'Authorization': 'Bearer myiVsubfQcj76DBPVfOwzKrDLleU75'}

# create list of all hosts defined in nagios
url = 'https://nagios.app.vumc.org/nagios/cgi-bin/statusjson.cgi?query=hostlist'
r=requests.get(url, auth=(SVC_USER,SVC_PASS))
nagios_hosts=r.json()['data']['hostlist']

# create list of all hosts in downtime in nagios
downtime_nag=[]
downtime_comments={}
url = 'https://nagios.app.vumc.org/nagios/cgi-bin/statusjson.cgi?query=downtimelist&details=true&ineffect=yes'
r=requests.get(url, auth=(SVC_USER,SVC_PASS))
for x in r.json()['data']['downtimelist']:
    downtime_nag.append(r.json()['data']['downtimelist'][x]['host_name'])
    downtime_comments[r.json()['data']['downtimelist'][x]['host_name']]=r.json()['data']['downtimelist'][x]['comment']

# create list of all hosts in downtime in tower
downtime_tower=[]
tower_id={}
url='/api/v2/hosts/?enabled__icontains=false'
results=get_all(url)
for x in results:
    downtime_tower.append(x['name'].split('.')[0])
    tower_id[x['name'].split('.')[0]]={'id':x['id'], 'name': x['name'], 'enabled':  True, 'inventory': x['inventory']}

#hosts to remove from downtime in tower
for host in set(downtime_tower) - set(downtime_nag):
    # allows for condition of host defined in tower but not in nagios, will maintain state set in tower
    if host in nagios_hosts:
        print ('removing '+host+'('+str(tower_id[host]['id'])+') from downtime in tower')
        syslog.syslog('removing '+host+'('+str(tower_id[host]['id'])+') from downtime in tower')
        url='https://tower.app.vumc.org/api/v2/hosts/'+str(tower_id[host]['id'])+'/'
        r=requests.patch(url, json=tower_id[host], headers=headers)

#hosts to set in downtime in tower
for host in set(downtime_nag) - set(downtime_tower):
    if not re.search('((:?no|skip) (:?tower|sat(?:ellite)?))',downtime_comments[host]):
        url='https://tower.app.vumc.org/api/v2/hosts/?enabled__icontains=true&name__icontains='+host
        url='https://tower.app.vumc.org/api/v2/hosts/?name__icontains='+host
        r=requests.get(url, headers=headers)
        for x in r.json()['results']:
            print ('disabling '+host+'('+str(x['id'])+') in tower')
            syslog.syslog('disabling '+host+'('+str(x['id'])+') in tower')
            downtime={'id':x['id'], 'name': x['name'], 'enabled':  False, 'inventory': x['inventory']}
            url='https://tower.app.vumc.org/api/v2/hosts/'+str(x['id'])+'/'
            r=requests.patch(url, json=downtime, headers=headers)
