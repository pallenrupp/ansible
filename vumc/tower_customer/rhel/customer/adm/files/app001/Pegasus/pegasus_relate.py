#!/bin/python3.4
import argparse
import inspect, os
import json
import mechanicalsoup
import re
import sys

parser = argparse.ArgumentParser(description='Python script to more easily associate tickets,incidents,changes,and requests')
parser.add_argument('-f',"--fromthese", required=True, default=None, help='"IMxxxx Txxxx Cxxxx"', action="append")
parser.add_argument('-t',"--tothose", required=True, default=None, help='"Cxxxxx Rxxxxx"', action="append")

args = parser.parse_args()

fromthese = args.fromthese[0].split(" ")
tothose = args.tothose[0].split(" ")

SVC_USER = "{{ svc_username }}"
SVC_PASS = "{{ svc_password }}"
ERROR = False

def connect_to_pegasus():
    browser = mechanicalsoup.StatefulBrowser()
    browser.open("https://pegasus.vumc.org/User/Login?ReturnUrl=%2f")
    page = browser.get_current_page()
    browser.select_form('form')
    browser["Username"] = SVC_USER
    browser["Password"] = SVC_PASS
    browser["__RequestVerificationToken"] = page.find("input").attrs['value']
    resp = browser.submit_selected()
    return browser

def help():
    print ()
    print ('Usage: '+inspect.getfile(inspect.currentframe())+' "Reason for opening a general request ticket here"')
    print ()
    return

# log into pegasus
browser = connect_to_pegasus()
# open the vumc it general request form
browser.open("https://pegasus.vumc.org/request/start/199/?s=")
page = browser.get_current_page()
# extract the secret json payload that contains their s3rk!t key, duh huh
pegasuspayload=json.loads(re.findall(r"data.*=.*(\ \{.*\})",str(page.text),re.IGNORECASE)[0])

for i in fromthese:
    if re.match('^C',i):
        fromi={'fromTicketModuleId': '3'}
    if re.match('^IM',i):
        fromi={'fromTicketModuleId': '1'}
    if re.match('^R',i):
        fromi={'fromTicketModuleId': '4'}
    if re.match('^T',i):
        fromi={'fromTicketModuleId': '18'}
    for c in tothose:
        if re.match('^C',c):
            toc={'toTicketModuleId': '3', 'toTicketModuleName': 'Change'}
        if re.match('^IM',c):
            toc={'toTicketModuleId': '1', 'toTicketModuleName': 'Incident'}
        if re.match('^R',c):
            toc={'toTicketModuleId': '4', 'toTicketModuleName': 'Request'}
        if re.match('^T',c):
            toc={'toTicketModuleId': '18', 'toTicketModuleName': 'Request Task'}

        relation={'fromTicket': i, 'toTicket': c, 'isLegacy': 'true', 'addedBy': '', 'validateFunction': '', 'searchId': 0, 'SecureKey': pegasuspayload['SecureKey']}
        relation.update(fromi)
        relation.update(toc)
        resp=browser.post("https://pegasus.vumc.org/api/related/add",json=relation)

        if resp.status_code != 200:
            print ('An error has occurred with the pegasus submission')
            print (resp.status_code+' - '+resp.reason)
        else:
            response=resp.json()['Data']
            if response['Success']:
                print (response['Message'])
            else:
                ERROR = True
                print ("Error adding "+i+" to "+c+" :: "+response['Message'])

if ERROR:
    sys.exit(1)
else:
    sys.exit(0)
