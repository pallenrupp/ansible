#!/bin/python3.4
import argparse
import inspect, os
import json
import mechanicalsoup
import re
import sys

parser = argparse.ArgumentParser(description='Python script to add ci views to changes')
group = parser.add_mutually_exclusive_group()
parser.add_argument('change', default=None, help='"Cxxxxx"', action="append")
group.add_argument('-a',"--alpha", required=False, default=False, action="store_true")
group.add_argument('-b',"--bravo", required=False, default=False, action="store_true")
group.add_argument('-c',"--charlie", required=False, default=False, action="store_true")

args = parser.parse_args()

try:
  change = int(re.search('C([0-9]*)',args.change[0])[1])
except:
   print ('invalid change number: '+str(args.change[0]))
   sys.exit(1)

if change == 0:
   print ('invalid change number: '+str(change))
   sys.exit(1)

SVC_USER = "{{ svc_username }}"
SVC_PASS = "{{ svc_password }}"

ERROR = False

def connect_to_pegasus():
    browser = mechanicalsoup.StatefulBrowser()
    browser.open("https://pegasus.vumc.org/User/Login?ReturnUrl=%2f")
    page = browser.get_current_page()
    browser.select_form('form')
    browser["Username"] = SVC_USER
    browser["Password"] = SVC_PASS
    browser["__RequestVerificationToken"] = page.find("input").attrs['value']
    resp = browser.submit_selected()
    return browser

# log into pegasus
browser = connect_to_pegasus()
# open the vumc it general request form
browser.open("https://pegasus.vumc.org/request/start/199/?s=")
page = browser.get_current_page()
# extract the secret json payload that contains their s3rk!t key, duh huh
pegasuspayload=json.loads(re.findall(r"data.*=.*(\ \{.*\})",str(page.contents),re.IGNORECASE)[0])
# alternate way to find userkey
#page.find(id='hUK').attrs['value']

# viewids from longwiwl's pegasus login
viewid={}
viewid['alpha']="15142"
viewid['bravo']="15140"
viewid['charlie']="15141"

if args.alpha:
  view=viewid['alpha']
if args.bravo:
  view=viewid['bravo']
if args.charlie:
  view=viewid['charlie']

data={"UserKey":pegasuspayload['SecureKey'],"ChangeId":change,"ViewId":view}

resp=browser.post("https://pegasus.vumc.org/api/ci/create/view",json=data)
if resp.json()['Success']:
   print ('Success '+resp.json()['Message'])
else:
   print ('Fail '+resp.json()['Message'])
