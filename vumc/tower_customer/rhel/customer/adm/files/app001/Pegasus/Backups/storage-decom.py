#!/bin/python3.4
from __future__ import print_function
import argparse
import inspect,os
import json
import mechanicalsoup
import re
import socket
import sys

SVC_USER = "{{ svc_username }}"
SVC_PASS = "{{ svc_password }}"

# set up the command line options
parser = argparse.ArgumentParser(description='Python script designed to submit decommission tickets to vumc it storage')
parser.add_argument('-s',"--server", required=True, default=None, help='fqdn server name', action="append")
parser.add_argument('-a',"--address", required=True, default=None, help='csv list of ip addresses', action="append")
parser.add_argument('-v',"--vunetid", required=True, default=None, help='csv list of ip addresses', action="append")
args = parser.parse_args()

VUNETID = args.vunetid[0].split('@')[0]
FQDN = args.server[0]
IPS = args.address[0]

def connect_to_pegasus():
    browser = mechanicalsoup.StatefulBrowser()
    browser.open("https://pegasus.vumc.org/User/Login?ReturnUrl=%2f")
    page = browser.get_current_page()
    browser.select_form('form')
    browser["Username"] = SVC_USER
    browser["Password"] = SVC_PASS
    browser["__RequestVerificationToken"] = page.find("input").attrs['value']
    resp = browser.submit_selected()
    return browser

# log into pegasus
browser = connect_to_pegasus()
# open the VUMC IT STORAGE - MODIFY BACKUP CLIENT form
browser.open("https://pegasus.vumc.org/request/start/300?s=")
page = browser.get_current_page()
# extract the secret json payload that contains their s3rk!t key, duh huh
pegasuspayload=json.loads(re.findall(r"(?:data\ =)(.*\{.*\})",str(page.text),re.IGNORECASE)[0])

# Split out our IP list from vcommander
IPS = [x.strip() for x in IPS.split(',')]

HOSTLIST=""
SHORTNAMES=[]
for IP in IPS:
    try:
        hostname=socket.gethostbyaddr(IP)[0]+' ('+IP+')'
        shortname=socket.gethostbyaddr(IP)[1][0]
    except:
        hostname=FQDN+'* ('+IP+')'
        shortname=FQDN.split('.')[0]
    if HOSTLIST == "":
        HOSTLIST=hostname
    else:
        HOSTLIST+="\n"+hostname
    SHORTNAMES.append(shortname)

#make sure we have a unique set of short names, no duplicates
SHORTNAMES=list(set(SHORTNAMES))
SHORTLIST=""
for x in SHORTNAMES:
   if SHORTLIST == "":
       SHORTLIST=x
   else:
       SHORTLIST+=", "+x

pegasuspayload['RequestorContactId']=SVC_USER
pegasuspayload['PrimaryContactId']=VUNETID
pegasuspayload['RequestReason']="Decomission "+SHORTLIST+"\n\n"+HOSTLIST

for x in pegasuspayload['Fields']:
    if re.search("project",x['Label'], re.IGNORECASE):
        x['Value']="N/A"
        continue
    if re.search("reason",x['Label'], re.IGNORECASE):
        x['Value']="Remove"
        continue
    if re.search("department",x['Label'], re.IGNORECASE):
        x['Value']="VUMC IT"
        continue
    if re.search("node",x['Label'], re.IGNORECASE):
        x['Value']=HOSTLIST
        continue
    if re.search("workgroup",x['Label'], re.IGNORECASE):
        x['Value']="VUMC IT LINUX"
        continue
    if re.search("instructions",x['Label'], re.IGNORECASE):
        x['Value']="Remove from TSM/Avamar and remove all relevant backups"
        # search HOSTLIST for * and if it is found put a disclaimer that this record failed a reverse lookup
        continue

with open("/tmp/decom.txt", "w") as text_file:

    print ("RequestorContactId: "+pegasuspayload['RequestorContactId'], file=text_file)
    print ("PrimaryContactId: "+pegasuspayload['PrimaryContactId'], file=text_file)
    print ("RequestReason: "+pegasuspayload['RequestReason'], file=text_file)

    for x in pegasuspayload['Fields']:
        print (x['Label']+" "+x['Value'], file=text_file)

    resp=browser.post("https://pegasus.vumc.org/api/request/submission/start/"+pegasuspayload['SecureKey'],json=pegasuspayload)
    if resp.status_code != 200:
        print ('An error has occurred with the pegasus submission')
        print (resp.status_code+' - '+resp.reason)
        print ('An error has occurred with the pegasus submission', file=text_file)
        print (resp.status_code+' - '+resp.reason, file=text_file)
        sys.exit(1)
    else:
        print ("Ticket successfully created: "+resp.json()['Data']['TicketNumber'])
        print ("Ticket successfully created: "+resp.json()['Data']['TicketNumber'], file=text_file)
        sys.exit(0)
