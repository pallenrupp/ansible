#!/bin/python3.4
from __future__ import print_function
import argparse
import inspect,os
import json
import mechanicalsoup
import re
import socket
import sys

SVC_USER = "{{ svc_username }}"
SVC_PASS = "{{ svc_password }}"

# set up the command line options
parser = argparse.ArgumentParser(description='Python script designed to submit decommission tickets to vec')
parser.add_argument('-c',"--ci", required=True, default=None, help='ci of server', action="append")
parser.add_argument('-a',"--address", required=True, default=None, help='csv list of ip addresses', action="append")
parser.add_argument('-v',"--vunetid", required=True, default=None, help='vunetid of submitter', action="append")
args = parser.parse_args()

VUNETID = args.vunetid[0].split('@')[0]
CI = args.ci[0]
IPS = args.address[0]

def connect_to_pegasus():
    browser = mechanicalsoup.StatefulBrowser()
    browser.open("https://pegasus.vumc.org/User/Login?ReturnUrl=%2f")
    page = browser.get_current_page()
    browser.select_form('form')
    browser["Username"] = SVC_USER
    browser["Password"] = SVC_PASS
    browser["__RequestVerificationToken"] = page.find("input").attrs['value']
    resp = browser.submit_selected()
    return browser

# log into pegasus
browser = connect_to_pegasus()
# open the VEC SECURITY OPERATIONS AND SERVICES FIREWALL RULE DECOMMISSION REQUEST form
browser.open("https://pegasus.vumc.org/request/start/3701/?s=")
page = browser.get_current_page()
# extract the secret json payload that contains their s3rk!t key, duh huh
pegasuspayload=json.loads(re.findall(r"(?:data\ =)(.*\{.*\})",str(page.text),re.IGNORECASE)[0])

pegasuspayload['RequestorContactId']=SVC_USER
pegasuspayload['PrimaryContactId']=VUNETID
pegasuspayload['RequestReason']="Decomission "+CI

for x in pegasuspayload['Fields']:
    if re.search("project",x['Label'], re.IGNORECASE):
        x['Value']="host decom"
        continue
    if re.search("business",x['Label'], re.IGNORECASE):
        x['Value']="Lacy Blanton"
        continue
    if re.search("technical contact name",x['Label'], re.IGNORECASE):
        x['Value']=VUNETID
        continue
    if re.search("phone",x['Label'], re.IGNORECASE):
        x['Value']="555-5555"
        continue
    if re.search("ip",x['Label'], re.IGNORECASE):
        x['Value']=IPS
        continue
    if re.search("ci",x['Label'], re.IGNORECASE):
        x['Value']=CI
        continue
    if re.search("verified",x['Label'], re.IGNORECASE):
        x['Value']="¯\_(ツ)_/¯"
        continue
    if re.search("department",x['Label'], re.IGNORECASE):
        x['Value']="VUMC IT LINUX"
        continue
    if re.search("management",x['Label'], re.IGNORECASE):
        x['Value']="blantol"
        continue

resp=browser.post("https://pegasus.vumc.org/api/request/submission/start/"+pegasuspayload['SecureKey'],json=pegasuspayload)
if resp.status_code != 200:
    print ('An error has occurred with the pegasus submission')
    print (resp.status_code+' - '+resp.reason)
    sys.exit(1)
else:
    print ("Ticket successfully created: "+resp.json()['Data']['TicketNumber'])
    sys.exit(0)
