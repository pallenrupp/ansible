#!/bin/python3.4
import argparse
import inspect,os
import json
import mechanicalsoup
import re
import socket
import sys

SVC_USER = "{{ svc_username }}"
SVC_PASS = "{{ svc_password }}"

# set up the command line options
parser = argparse.ArgumentParser(description='Python script designed to submit decommission tickets to vumc it storage')
parser.add_argument('-s',"--server", required=True, default=None, help='fqdn server name', action="append")
parser.add_argument('-a',"--address", required=True, default=None, help='csv list of ip addresses', action="append")
parser.add_argument('-v',"--vunetid", required=True, default=None, help='csv list of ip addresses', action="append")
parser.add_argument('-d',"--datacenter", required=True, default=None, help='DC1, DC2, DC3 #{target.datacenter.name}', action="append")
parser.add_argument('-c',"--cluster", required=True, default=None, help='vsphere cluster name #{target.cluster.name}', action="append")
parser.add_argument('-ch',"--host", required=True, default=None, help='vsphere host #{target.managedSystem.name}', action="append")
parser.add_argument('-f',"--filename", required=True, default=None, help="vsphere file name #{target.customAttribute['vSphere Destination - Linux']}", action="append")

args = parser.parse_args()

VUNETID = args.vunetid[0].split('@')[0]
FQDN = args.server[0]
IPS = args.address[0]
DATACENTER = args.datacenter[0]
CLUSTER = args.cluster[0]
HOST = args.host[0]
FILENAME = args.filename[0]

def connect_to_pegasus():
    browser = mechanicalsoup.StatefulBrowser()
    browser.open("https://pegasus.vumc.org/User/Login?ReturnUrl=%2f")
    page = browser.get_current_page()
    browser.select_form('form')
    browser["Username"] = SVC_USER
    browser["Password"] = SVC_PASS
    browser["__RequestVerificationToken"] = page.find("input").attrs['value']
    resp = browser.submit_selected()
    return browser

# log into pegasus
browser = connect_to_pegasus()
# open the vumc it general request form
browser.open("https://pegasus.vumc.org/request/start/201/?s=")
page = browser.get_current_page()
# extract the secret json payload that contains their s3rk!t key, duh huh
pegasuspayload=json.loads(re.findall(r"(?:data\ =)(.*\{.*\})",str(page),re.IGNORECASE)[0])

# Split out our IP list from vcommander
IPS = [x.strip() for x in IPS.split(',')]

HOSTLIST=""
for IP in IPS:
    try:
        hostname=socket.gethostbyaddr(IP)[0]+' ('+IP+')'
    except:
        hostname=FQDN+'* ('+IP+')'
    if HOSTLIST == "":
        HOSTLIST=hostname
    else:
        HOSTLIST+="\n"+hostname

pegasuspayload['RequestorContactId']=SVC_USER
pegasuspayload['PrimaryContactId']=VUNETID
pegasuspayload['RequestReason']="Add backup solution to new server:\n\n"+HOSTLIST

for x in pegasuspayload['Fields']:
    if re.search("project",x['Label'], re.IGNORECASE):
        x['Value']="N/A"
        continue
    if re.search("location",x['Label'], re.IGNORECASE):
        x['Value']=DATACENTER
        continue
    if re.search("department",x['Label'], re.IGNORECASE):
        x['Value']="VUMC IT"
        continue
    if re.search("node",x['Label'], re.IGNORECASE):
        x['Value']=HOSTLIST
        continue
    if re.search("workgroup",x['Label'], re.IGNORECASE):
        x['Value']="VUMC IT LINUX"
        continue
    if re.search("cluster",x['Label'], re.IGNORECASE):
        x['Value']=CLUSTER+" - "+HOST
        continue
    if re.search("image name",x['Label'], re.IGNORECASE):
        x['Value']=FILENAME
        continue
    if re.search("operating",x['Label'], re.IGNORECASE):
        x['Value']="Linux"
        continue
    if re.search("sql",x['Label'], re.IGNORECASE):
        x['Value']="Yes"
        continue
    if re.search("lan backup",x['Label'], re.IGNORECASE):
        x['Value']="Yes"
        continue
    if re.search("vm image",x['Label'], re.IGNORECASE):
        x['Value']="Yes"
        continue

with open("/tmp/text.txt", "w") as text_file:

    print ("RequestorContactId: "+pegasuspayload['RequestorContactId'], file=text_file)
    print ("PrimaryContactId: "+pegasuspayload['PrimaryContactId'], file=text_file)
    print ("RequestReason: "+pegasuspayload['RequestReason'], file=text_file)

    for x in pegasuspayload['Fields']:
        print (x['Label']+" "+x['Value'], file=text_file)

    resp=browser.post("https://pegasus.vumc.org/api/request/submission/start/"+pegasuspayload['SecureKey'],json=pegasuspayload)
    if resp.status_code != 200:
        print ('An error has occurred with the pegasus submission')
        print (resp.status_code+' - '+resp.reason)
        print ('An error has occurred with the pegasus submission', file=text_file)
        print (resp.status_code+' - '+resp.reason, file=text_file)
        sys.exit(1)
    else:
        print ("Ticket successfully created: "+resp.json()['Data']['TicketNumber'])
        print ("Ticket successfully created: "+resp.json()['Data']['TicketNumber'], file=text_file)
        sys.exit(0)
