#!/bin/python3.6
# pip3 install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
import argparse
import requests
import urllib3
import os
import time
from urllib.parse import urlparse
from vmware.vapi.vsphere.client import create_vsphere_client
from com.vmware.vapi.std_client import DynamicID
from com.vmware.vcenter_client import VM
import com.vmware.vapi.std_client

parser = argparse.ArgumentParser(description='Python script to tag vms in vsphere')
parser.add_argument('-e',"--host", default='vvc1002op.hs.it.vumc.io', help='ESX Host defaults to vvc1002op.hs.it.vumc.io if omitted', required=False, action="store")
parser.add_argument('-t',"--tag", default='LINUX_AVA_IMG', help='Defaults to LINUX_AVA_IMG if omitted', required=False, action="store")
parser.add_argument('-v',"--vm", required=True, action="store")

args = parser.parse_args()

tag_name = args.tag
guestvm = args.vm

VCENTER_HOST = args.host
VCENTER_USER = "{{ hsorch_username }}"
VCENTER_PASS = "{{ hsorch_password }}"

def vcenter_client():
    '''
     vCenter client
    '''
    session = requests.session()
    session.verify = False
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    vcenter_host = urlparse(VCENTER_HOST).netloc
    try:
        vsphere_client = create_vsphere_client(server=VCENTER_HOST, username=VCENTER_USER, password=VCENTER_PASS, session=session)
    except com.vmware.vapi.std.errors_client.Unauthenticated:
        print ('connection error retrying...')
        time.sleep(1)
        vsphere_client=vcenter_client()
    return vsphere_client

def get_tag(tag_name):
    """Get tag id and category id based on tag name

    :param str tag_name: Tag name

    :return: Tag ID and Category ID
    :rtype: dict
    """
    global client
    tags = client.tagging.Tag.list()
    for tag in tags:
        t = client.tagging.Tag.get(tag)
        if t.name == tag_name:
            return {
                "id": t.id,
                "category_id": t.category_id
            }

def append_tag(tag, vm_moreof):
    """Add tag to VM based on moreof

    :param dict tag: Tag ID and Category ID
    :param str vm_moreof: VM object reference
    """
    global client
    dynamic_id = DynamicID(type='VirtualMachine', id=vm_moreof)
    client.tagging.TagAssociation.attach(
        tag_id=tag['id'], object_id=dynamic_id)
    for tag['id'] in client.tagging.TagAssociation.list_attached_tags(dynamic_id):
        if tag['id'] == tag['id']:
            tag_attached = True
            break
    assert tag_attached

def get_vm(vm_name):
    """
    Return the identifier of a vm
    Note: The method assumes that there is only one vm with the mentioned name.
    """
    names = set([vm_name])
    global client
    vms = client.vcenter.VM.list(VM.FilterSpec(names=names))
    if len(vms) == 0:
        print("VM with name ({}) not found".format(vm_name))
        return None
    vm = vms[0].vm
    print("Found VM '{}' ({})".format(vm_name, vm))
    return vm
#    return client.vcenter.VM.get(vm).identity.instance_uuid

# establish a single client connection for reuse
client=vcenter_client()
TAG=get_tag(tag_name)
VMID=get_vm(guestvm)
append_tag(TAG,VMID)
