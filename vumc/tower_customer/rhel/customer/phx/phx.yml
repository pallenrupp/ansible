- hosts: all
  gather_facts: no
  strategy: free
  vars:
    uid: 1506
    gid: 1506
    username: phxapp
    group: phxapp
    ad_group: sg-phx_adm
    access_level: LOCAL
    comment: Customer App Account
    home: /app001
    contact: pharmacy_bi_support@vumc.org

    rxapex_hosts: ['phx1002ld','phx1002lt','phx1002lp']
    rxrpt_hosts:  ['phx1003ld','phx1003lt','phx1003lp']
    splunk_clients: ['phx1001ld','phx1001lt','phx1001lp','phx1002ld','phx1002lt','phx1002lp','phx1003ld','phx1003lt','phx1003lp']

    mounts:
    - { mountpoint: '/Pharmacy_PHR',     export: 'bigdatavuhnfs.ea.vanderbilt.edu:/ifs/data/depts/PHR' }
    mounts_adc_phx:
    - { mountpoint: '/adc_phx',          export: 'bigdatavuhnfs.ea.vanderbilt.edu:/ifs/apps/datawarehouse/dw_exchange/VUMC_Flowlytics/ADC/PHXAPP' }
    mounts_epsi:
    - { mountpoint: '/epsi',             export: 'bigdatavuhnfs.ea.vanderbilt.edu:/ifs/data/depts/DOF/FinanceCT/DOFAppRecover/Drive\(F\)/EPSI_APP' }
    mounts_edwstaging:
    - { mountpoint: '/edw_drop_staging', export: 'i10filenfs.hs.it.vumc.io:/ifs/i10file/apps/edw_drop_staging/pharmacy' }
    mounts_fdb:
    - { mountpoint: '/fdb',              export: 'i10filenfs.hs.it.vumc.io:/ifs/i10file/apps/edw_drop_staging/fdb' }
    mounts_iis_phx:
    - { mountpoint: '/iis_phx',          export: 'bigdatavuhnfs.ea.vanderbilt.edu:/ifs/apps/datawarehouse/dw_exchange/VUMC_Flowlytics/IIS/PHXAPP' }
    mounts_phx1003_dev:
    - { mountpoint: '/phx1003',          export: 'i110filenfs.hs.it.vumc.io:/ifs/vumcio/i110file/apps/phx1003/dev' }
    mounts_phx1003_prod:
    - { mountpoint: '/phx1003',          export: 'i110filenfs.hs.it.vumc.io:/ifs/vumcio/i110file/apps/phx1003/prod' }
    mounts_phx1003_test:
    - { mountpoint: '/phx1003',          export: 'i110filenfs.hs.it.vumc.io:/ifs/vumcio/i110file/apps/phx1003/test' }

  vars_files:
    - ../../common/vars.yml

  tasks:
    - name: Loop through mounts and expand as necessary
      set_fact:
        mounts: "{{ mounts + lookup('vars', 'mounts_' + item.name) }}"
      when: item.cond
      loop:
        - { name: 'adc_phx',       cond: '{{ inventory_hostname_short in ["phx1001ld","phx1001lt","phx1001lp"] }}' }
        - { name: 'edwstaging',    cond: '{{ inventory_hostname_short in ["phx1001ld","phx1001lt","phx1001lp"] }}' }
        - { name: 'fdb',           cond: '{{ inventory_hostname_short in ["phx1001ld","phx1001lt","phx1001lp"] }}' }
        - { name: 'iis_phx',       cond: '{{ inventory_hostname_short in ["phx1001ld","phx1001lt","phx1001lp"] }}' }
        - { name: 'phx1003_dev',   cond: '{{ inventory_hostname_short in ["phx1003ld"] }}' }
        - { name: 'phx1003_prod',  cond: '{{ inventory_hostname_short in ["phx1003lp"] }}' }
        - { name: 'phx1003_test',  cond: '{{ inventory_hostname_short in ["phx1003lt"] }}' }

    - name: Set access_level as needed
      set_fact:
        access_level: "{{ access_level }} {{ item.access }}"
      when: item.cond
      loop:
        - { access: "phx1001lt.hs.it.vumc.io",                         cond: "{{ inventory_hostname_short in ['phx1001ld'] }}" }
        - { access: "phx1001ld.hs.it.vumc.io phx1001lp.hs.it.vumc.io", cond: "{{ inventory_hostname_short in ['phx1001lt'] }}" }

    - name: "Add {{ item.group }} to access.conf"
      lineinfile:
        path: /etc/security/access.conf
        regexp: '^\+\s*\:\s*{{ item.group }}\s:.*'
        insertbefore: '^-.*ALL'
        line: '+ : {{ item.group }} : ALL'
      when: item.cond
      loop:
        - { group: "sg-phx_file_adm", cond: "{{ inventory_hostname_short in ['phx1001ld'] }}" }

    - include_tasks: ../groups.yml
    - include_tasks: ../users.yml
    - include_tasks: ../../common/cron.yml
    - include_tasks: ../../app/autofs/autofs.yml
    - include_tasks: ../../app/python/python.yml
      vars:
        version: '{{ item.version }}'
      when: item.cond
      loop:
      - { version: '3.4',  cond: '{{ inventory_hostname_short in rxapex_hosts and rhel7 }}' }

    - include_tasks: ../../app/postgresql/postgresql.yml
      vars:
        version: '12'
      when: inventory_hostname_short in ['phx1001ld','phx1001lt','phx1001lp']

    - include_tasks: ../../app/splunk_client/splunk_client.yml
      when: inventory_hostname_short in splunk_clients

    - name: build list of services for sudoers
      set_fact:
        service: '{{ service |default([]) + query("items", item.name) }}'
      when: item.cond
      loop:
        - { name: 'weblogic', cond: '{{ inventory_hostname_short in rxapex_hosts }}' }

    - include_tasks: ../../common/sudoers.yml
      vars:
        commands:
          - { 'username':'%sg-phx_adm','run_as':'root','command':'/usr/bin/yum list *' }

    - name: Enable required repos
      rhsm_repository:
        name: '{{ item.name }}'
        state: enabled
      when: item.cond|default(true)
      loop:
      - { name: 'vumc_it_linux_Microsoft_prod_7', cond:  '{{ rhel7 }}' }

    - name: Enable required repos
      rhsm_repository:
        name: '{{ item.name }}'
        state: enabled
      when: item.cond|default(true)
      loop:
      - { name: 'codeready-builder-for-rhel-9-x86_64-rpms', cond:  '{{ rhel9 and rxrpt_hosts }}' }
      - { name: 'vumc_it_linux_Microsoft_prod_9', cond:  '{{ rhel9 and rxrpt_hosts }}' }

    - name: Install requested packages
      package:
        name: "{{ item.packages }}"
        state: present
        lock_timeout: 180 # wait up to 3 minutes for a lock ansible/ansible#57189
      when: item.cond
      loop:
        - { packages: ['freetds'], cond: "{{ true }}" }
        - { packages: ['bzip2','bzip2-devel','java-1.8.0-openjdk','libdb4-devel','libffi-devel','libpcap-devel','msodbcsql17','ncurses-devel','gdbm-devel','openssl','openssl-devel','pyodbc','readline-devel','sqlite-devel','tk-devel','unzip','xz-devel','zlib-devel'], cond: "{{ rhel7 }}" }
        - { packages: ['bzip2-devel','gdbm-devel','gitlab-runner','java-1.8.0-openjdk','libffi-devel','libpcap-devel','msodbcsql18','ncurses-devel','nodejs','openssl-devel','podman','python3-pyodbc','readline-devel','sqlite-devel','tk-devel','xz-devel','zlib-devel'], cond: "{{ rhel9 and rxrpt_hosts }}" }
        - { packages: ['git','java-11-openjdk','python3-pyodbc','unzip'], cond: '{{ rhel8 }}' }
        - { packages: ['gcc-c++','git','rh-git218'], cond: "{{ inventory_hostname_short in ['phx1001ld'] }}" }
        - { packages: ['@Development tools','rust','cargo'], cond: "{{ inventory_hostname_short in ['phx1003lt','phx1003ld','phx1003lp'] }}" }
        - { packages: ['nodejs'], cond: "{{ inventory_hostname_short in ['phx1001ld','phx1002ld','phx1002lt'] }}" }
        - { packages: ['chromedriver','chromium'], cond: "{{ inventory_hostname_short in ['phx1001ld','phx1001lt','phx1001lp'] }}" }  
      environment:
        ACCEPT_EULA: 'Y'

    - include_tasks: ../../app/nginx/nginx.yml
      when: rhel8

    - include_tasks: ../../common/perms.yml
      vars:
        username: "{{ item.username }}"
        group: "{{ item.group }}"
        filemode: "{{ item.filemode | default('') }}"
        dirmode: "{{ item.dirmode | default('') }}"
        recurse: "{{ item.recurse | default('') }}"
        path: "{{ item.path }}"
        excludes: "{{ item.excludes | default('') }}"
      loop:
        - { username: 'phxapp', group: 'root', path: '/etc/odbcinst.ini', filemode: '0644' }

    - name: copy systemd unit files
      copy:
        src: '{{ item.src |default("files" + item.dest) }}'
        dest: '{{ item.dest }}'
        owner: root
        group: root
        mode: '0644'
      when: item.cond
      register: systemd
      loop:
        - { dest: '/etc/systemd/system/weblogic.service', cond: '{{ inventory_hostname_short in rxapex_hosts }}' }

    - name: reload systemd
      systemd:
        daemon_reload: yes
      when: systemd.changed
