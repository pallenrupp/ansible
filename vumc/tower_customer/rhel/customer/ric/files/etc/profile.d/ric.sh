# Set umask for RIC Roles

EAI_IDNAME=$( /usr/bin/id -un )

if ( /usr/bin/groups "$EAI_IDNAME" | grep -q ric_ );then
    umask 0002
fi
