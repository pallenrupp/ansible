#!/bin/bash

NUMBER=$1

/usr/bin/pgrep -u apache | sort > /tmp/httpd.pids

EXISTS=`cat /tmp/httpd.pids | grep $NUMBER`

if [ "$NUMBER" = "$EXISTS" ]
    then
       kill -9 $NUMBER; echo "httpd pid $NUMBER has been killed"
    else
       echo "that pid is not an httpd process"
fi
exit
