# ====== appd-console.service =============================
# Start App Dynamics Console .. appd-controller.service must be active before starting this.
#
[Unit]
Description=AppDynamics Console
After=network.service

[Service]
Type=forking
ExecStart=/app001/scripts/start_console.sh
ExecStop=/app001/scripts/stop_console.sh
User=jas
Group=jas
TimeoutSec=900

[Install]
WantedBy=multi-user.target
