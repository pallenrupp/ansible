#====== appd-controller.service =============================
# Start App Dynamics Controller .. Must be active before appd-console.service runs
#
[Unit]
Description=AppDynamics Controller
After=appd-console.service


[Service]
Type=forking
ExecStart=/app001/scripts/start_controller.sh
ExecStop=/app001/scripts/stop_controller.sh
TimeoutSec=480
PIDFile=/app001/appdynamics/platform/product/controller/db/mysql.pid
User=jas
Group=jas

[Install]
WantedBy=multi-user.target
