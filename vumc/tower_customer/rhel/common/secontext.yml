# Set persistent selinux contexts
# This helper is used to both set context as would be done with semanage and actually apply that context to the files
# For now this should only be used for root owned files which we will manage for the customer
#
#   Required Parameters:
#   - target  : Path of directory or file
#   - setype  : selinux context to set and apply
#
#   Optional Parameters:
#   - recursive  : Append '(/.*)?' to the target path and uses restorecon with the -R flag
#   - ftype      : File type to target (see sefcontext module docs for full options f = regular files, d = directories)
#
#   USAGE EXAMPLE
#
#   - include_tasks: ../../common/secontext.yml
#     vars:
#      selinux_contexts:
#       - { target: '/app001/startup.sh',  setype: 'bin_t' }
#       - { target: '/app001/stopdown.sh', setype: 'bin_t' }
#     when: ansible_selinux.status == 'enabled'
---
- block:
  - name: List of custom selinux contexts
    debug:
      var: selinux_contexts
      verbosity: 1
    when: selinux_contexts is defined

  - name: verify files exist
    stat:
      path: '{{ item.target }}'
    register: context_results
    loop: '{{ selinux_contexts }}'
    when: selinux_contexts is defined and query('items', selinux_contexts)|length>0

  - name: Set persistent selinux contexts
    sefcontext:
      target: '{{ item.target + "(/.*)?" if item.recursive|default(false) else item.target }}'
      setype: '{{ item.setype }}'
      ftype: '{{ item.ftype |default(omit) }}'
    loop: '{{ selinux_contexts }}'
    when: selinux_contexts is defined and query('items', selinux_contexts)|length>0

  - name: ensure selinux file contexts are applied
    shell: restorecon -v {{ '-R' if item.recursive|default(false) else '' }} "{{ item.target }}"
    register: restorecon
    changed_when: restorecon.stdout != ""
    loop: '{{ selinux_contexts }}'
    when: context_results |json_query("results[?invocation.module_args.path==\'" + item.target +"\'].stat.exists") |first |bool
      # only run this task when the target path exists
  when: ansible_selinux.status == 'enabled'
