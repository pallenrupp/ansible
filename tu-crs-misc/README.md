### 

 This folder contains CRS team Ansible automation for kubernetes.
 Functionality requested (per Sid Kuthsa):
 1.  Restart and unseal vault - get sudo permissions 
 2.  service docker restart - on all nodes, permissions in stage to log in to the machine and sudo. 
 3.  service kubelet restart - on all nodes
 4.  (added by Peter) - drain and undrain k8s nodes.    
 anyone who is in dck5grpu should be able to run sudo on the scripts.

