### 

This folder contains Ansible automation scripts.

The directory structure handles two different styles of Ansible:

1.  Where most of your executable code is kept within a role, and 
    your playbook is a simple driver that invokes one or more roles.
  
    $ ansible-playbook ./yourplay.yml -i environments/<yourenv>/inventory  

    ./environments/<yourenv>/inventory
    ./environments/<yourenv>/group_vars
    ./roles/<yourrole>/tasks
    ./roles/<yourrole>/templates
    ./roles/<yourrole>/defaults
    ./roles/<yourrole>/handlers
   
2.  Where you code exists mostly in the playbook
   
    $ ansible-playbook ./yourplay.yml -i environments/<yourenv>/inventory
    
    In this case, your variables can be stored in the inventory file, with
    defaults in ./group_vars
    


