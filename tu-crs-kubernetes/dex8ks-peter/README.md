#
# Quick start 
#

# 1. Create and update your kubernete's cluster inventory file
  
     $ cp hosts_example.yml hosts_<your cluster name>.yml
     $ vi hosts_<your cluster name>.yml
     Update the managers and workers hosts
     Update the "clusterName" variable

# 2. Create and update Dex and Gangway configuration files

    In this step, we copy an example set of dex/gangway configuration
    files into your own directory, named after your clusterName variable 
    you defined in step 1.  Then, you change a few files to reflect your
    unique environment.  

    $ cd roles/dex/templates
    $ cp -pr example <clusterName> 
    $ cd <clusterName>

    Then, modify the following files to reflect your environment.
   
    - dex-configmap.yaml
    - dashboard-ingress.yaml
    - gangway-configmap.yaml
    
    Do not modify the other files.

# 3. Run the playbook :-)
  
    Go to the playbook's root directory 
    
    $ ansible-playbook -i hosts_<clusterName>.yml install_playbook.yml




#
# Reference -= Original (manual) instructions 
# per Kuthsa "Sid" Sudharsan
#
    We are going to create an Ansile Role for DEX setup so we can reuse it.
    Source: https://wiki.transunion.com/pages/viewpage.action?pageId=68584166


    Dex is an OpenID Connect (OIDC) provider that will be in charge of our 
    authentication. We will use Active Directory as a backend for Dex, but
    there are many other backend solutions to choose from.

    1- Create a dex-rbac.yaml file.
       The yaml is attached 

    2- Create the permissions for Dex.
       $ kubectl apply -f dex-rbac.yaml

    3- Create a dex-configmap.yaml file. 
       Modify the issuer URL, the redirect URIs, the client secret 
       and the Active Directory configuration accordingly.
       Configmap is attached. 
    
    4- Configure Dex.
       $ kubectl apply -f dex-configmap.yaml

    5- Create the dex-deployment.yaml file.
       dex-deployment is attached.
    
    6- Deploy Dex.
       $ kubectl apply -f dex-deployment.yaml

    7- Create a dex-service.yaml file.
       service yaml is attached.

    8- Create a service for the Dex deployment.
       $ kubectl apply -f dex-service.yaml

    9- Create a dex-ingress.yaml file. Change the host parameters and 
       your certificate issuer name accordingly.
       ingress yaml is attached.
 
    10- Create the ingress for the Dex service.
    
       $ kubectl apply -f dex-ingress.yaml
    
    This concludes installing DEX. done run command:



#
#Configure the Kubernetes API to access Dex as OpenID connect provider

   These steps have to be done on each of your Kubernetes master nodes.

   1- SSH to your master nodes. 
   2- Edit the Kubernetes API configuration. Add the OIDC parameters and modify the issuer URL accordingly.
      $ sudo vim /etc/kubernetes/manifests/kube-apiserver.yaml
   
   On all the masters in the cluster in /etc/kubernetes/manifests there
   will be file called apiserver.yaml, can you edit the yaml to add below
   new lines. Upon the change in few minutes the apiserver Pods will be
   restarted automatically. 
   
       - --oidc-issuer-url= https://crsapps-stage.transunion.com/
       - --oidc-client-id=k8s-authenticator
       - --oidc-username-claim=email
       - --oidc-groups-claim=groups
   
   3- The Kubernetes API will restart by itself.



#
# Deploy Gangway
#

   Gangway is a web interface made by Heptio. It will allow us to configure 
   kubectl with our user settings.

   # <SKIP>
   1- Generate a secret key for Gangway.
      $ kubectl -n kube-system create secret generic gangway-key \
         --from-literal=sesssionkey=$(openssl rand -base64 32)
   # </SKIP>
   
   2- Create a gangway-configmap.yaml file. Modify the cluster name, the
      URLs, and the client secret accordingly. For the client secret, use 
      the same secret that you specified in the Dex configmap during the 
      previous step.  gangway-configmap is attached.
   
   3- Configure Gangway.
      $ kubectl apply -f gangway-configmap.yaml
   
   4- Create a gangway-deployment.yaml file.
      gangway-deployment yaml is attached.
   
   5- Create the Gangway deployment.
      $ kubectl apply -f gangway-deployment.yaml
   
   6- Create a gangway-service.yaml file.
      gangway-service yaml is attached.
   
   7- Create the service for the Gangway deployment.
      $ kubectl apply -f gangway-service.yaml
   
   8- Create a gangway-ingress.yaml file. Modify the host parameter and 
      the certificate manager issuer accordingly.
      gangway-ingress yaml is attached.
   
   9- Create the ingress for the Gangway service.
      $ kubectl apply -f gangway-ingress.yaml
   
   10- Wait a couple of minutes while the certificate manager generates a 
       SSL certificate for Gangway and browse to
       http://chu3l5cn01.custstage.transunion.com:31981
   
   11- Click on "Sign In".
   
   12- Login with your Active Directory user after you hit ldap. 
 
   13- Copy your cluster administrator configuration.
   
       $ cd ~/.kube/
       $ cp config config_stg
   
   14- Follow the steps to generate your kubectl configuration file.
   
   15- You should now be logged in with your Active Directory user and you 
       should be able to list the pods in the default namespace, but not 
       in the kube-system namespace.
   
       $ kubectl get pods
       $ kubectl get pods -n kube-system Error from server (Forbidden): \
         pods is forbidden: User "sguyennet" cannot list pods in the namespace "kube-system"
   
   16- Log back with your cluster administrator user.
       $ export KUBECONFIG=~/.kube/admin-config
       $ kubectl get pods -n kube-system
   
   17- Create a dashboard-ingress.yaml file. Modify the dashboard URLs 
       and the host parameter accordingly.
   
       $ vim dashboard-ingress.yaml

apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: kubernetes-dashboard
  namespace: kube-system
  annotations:
          nginx.ingress.kubernetes.io/auth-url: "https://dashboard.k8s.inkubate.io/oauth2/auth"
          nginx.ingress.kubernetes.io/auth-signin: "https://dashboard.k8s.inkubate.io/oauth2/start?rd=https://$host$request_uri$is_args$args"
          nginx.ingress.kubernetes.io/secure-backends: "true"
          nginx.ingress.kubernetes.io/configuration-snippet: |
            auth_request_set $token $upstream_http_authorization;
            proxy_set_header Authorization $token;
spec:
  rules:
  - host: dashboard.k8s.inkubate.io
    http:
      paths:
      - backend:
          serviceName: kubernetes-dashboard
          servicePort: 443
        path: /

   18- Create the ingress for the dashboard service.
       $ kubectl apply -f dashboard-ingress.yaml
   
   19- Create an oauth2-proxy-ingress.yaml file. Modify the certificate 
      manager issuer and the host parameters accordingly.
      $ vim oauth2-proxy-ingress.yaml

apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/tls-acme: "true"
    certmanager.k8s.io/cluster-issuer: "letsencrypt-production"
    ingress.kubernetes.io/force-ssl-redirect: "true"
  name: oauth-proxy
  namespace: auth-system
spec:
  rules:
  - host: dashboard.k8s.inkubate.io
    http:
      paths:
      - backend:
       serviceName: oauth2-proxy
          servicePort: 8080
        path: /oauth2
  tls:
  - hosts:
    - dashboard.k8s.inkubate.io
    secretName: kubernetes-dashboard-external-tls19- Create the ingress for the Oauth2 proxy service.

   $ kubectl apply -f oauth2-proxy-ingress.yaml
   
   Conclusion
   
   Success! You are now able to access your Kubernetes cluster, in a 
   user-friendly fashion, both with the kubectl command line and with 
   the dashboard.
