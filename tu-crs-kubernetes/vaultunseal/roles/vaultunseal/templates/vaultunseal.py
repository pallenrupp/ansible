#!/usr/bin/python
"""
  This script is used to unseal a hashi-corp vault instance.
  By default, the script reads config file /etc/sysconfig.vaultunseal.yaml
  but this may be overridden with the --configfile option.
  The configuration file contains:
    - variables used to establish a connection with a vault server
    - a list of unseal keys used to unseal a vault instance

  Below is an example configuration file
------------------- snip here --------------------
---
vault_addr: "https://chu3l9ap417.cust.transunion.com:8200"
vault_cacert: "/etc/vault/certs/chu3l9ap417.cust.transunion.com.crt"

keys:
  - +EeJxgEIY9TyF4DtZchVOstPWscvZXZHKNmYk5Ld/t0K
  - jRGrWxMaBtdZqZsq7k3n1BShxt5G5SD7lSw0y/+6CVwo
  - 6vf/Q11UnrOTMPJfwVj1Y2wWCwZVXMbTaPLjF5WTZjB8
------------------- snip here --------------------

 Change History:
    12/12/2019 - P. Rupp - Initial Implementation
"""
import argparse
import os
import pexpect
import platform
import socket
import string
import subprocess
import sys
import time
import yaml 
import yaml.parser

class ns():
  #for global variables
  pass

class results():
   """
   the 'results' class represents shell (bash) command results and output
   Objects created from this class contain the shell command executed 
   (self.cmd), the return code (self.rc), and the stdout/stderr streams
   (self.stdout/self.stderr) as python strings. 
   The class provides get/set routines - i.e, myobject.getcmd() - 
   for all variables noted.
   """

   def  __init__(self,rc = 0,cmd = None,stdout = None,stderr = None):
      self.rc = rc
      self.cmd = cmd
      self.stdout = stdout
      self.stderr = stderr
   def __repr__(self):
      r (self.rc, self.cmd, self.stdout, self.stderr)

   def __str__(self):
      work = (str(self.rc),str(self.cmd),str(self.stdout),str(self.stderr))
      return "%s"%(work)

   def getrc(self):
      return self.rc

   def setrc(self,rc):
      self.rc = rc

   def getcmd(self):
      return self.cmd

   def setcmd(self,cmd):
      self.cmd = cmd

   def getstdout(self):
      return self.stdout

   def setstdout(self,stdout):
      self.stdout = stdout

   def getstderr(self):
      return self.stderr

   def setstderr(self,stderr):
      self.stderr = stderr

def shell(cmd):
   """
   The shell function is a convenient wrapper to execute (bash) 
   commands, and collect the return code and output in an easy way.
   The command returns a 'results' object.
   """
   rob = results()
   rob.setcmd(cmd)
   proc = subprocess.Popen(cmd,
                  shell = True,
                  stderr = subprocess.PIPE,
                  stdout = subprocess.PIPE,
                  universal_newlines = True)
   o = proc.stdout.read() 
   rob.setstdout(o)
   e = proc.stderr.read() 
   rob.setstderr(e)
   rc = proc.wait() 
   rob.setrc(rc)

   return rob 


def getparms():

  d = """
This program unseals a hashicorp Vault instance
default config file found in /etc/sysconfig/vaultunseal.yaml
overridden with --configfile option
  """
  p = argparse.ArgumentParser(description = d)
  p.add_argument('--configfile','-c',nargs = 1,required = True,help = "config.yaml file",dest = "configfile")

  args = p.parse_args(sys.argv[1:])

  if (not os.path.isfile(args.configfile[0])):
     p.error("--configfile %s is not a file or missing."%(args.configfile))

  if (not os.path.isfile(args.configfile[0])):
     p.error("-keyfile is not a file or missing.")
 
  return args

def log(tag,msg):

   TIMESTAMP = time.strftime("%Y.%m.%d.%H:%M:%S", time.localtime())
   msg = msg.strip()
   tag = tag.strip()
   if (tag  ==  ""): 
      tag = "INFO"
   for i in msg.splitlines():
      msgout = "%s %s [%s] %s\n"%(TIMESTAMP, globals.PGM, tag, i)
      sys.stdout.write(msgout)

def getconfig():

  """
   getconfig()
   Reads the global namespace object for the config file name, then
   parses it for correct syntax and variables.
  """

  configfile = globals.args.configfile[0]
  print("Opening configfile=", configfile)
  rc = True
  log("INFO","loading config file %s ..."%(configfile))
  try:
    data = yaml.load(open(configfile))
  except yaml.parser.ParserError, e:
    log("ERROR","parse error in file %s"%(configfile))
    log("ERROR",str(e))
    sys.exit(1)
  except IOError, e:
    log("ERROR","open error in file %s"%(configfile))
    log("ERROR",str(e))
    sys.exit(1)
 
  for i in ("keys", "vault_addr", "vault_cacert"):
    try:
      work = data[i]
    except KeyError, e:
      log("ERROR","config file %s missing '%s'"%(configfile,i))
      sys.exit(1)
  
  globals.keys = data["keys"]
  globals.vault_addr = data["vault_addr"]
  globals.vault_cacert = data["vault_cacert"]
  log("INFO","success!")

  return rc

def unseal():

  """
    main() performs various sanity checks, then
    tries to unseal a hashicorp vault instance
    using the keys found in the configuration file (.yaml) file.

    1. Sanity checks.
       - verifies yaml syntax, else exit w/error
       - verifies key counts are ok, else exit w/ERROR
          0 keys found (key count = 0) Exit w/error.
          n keys found less than the Theshold, exit w/ERROR
          n keys found are less than "Total Shares", exit w/WARN
              Warning (missing keys, but continue)

      (Vault "Threshold" and "Total Shares" values are obtained 
       from the Vault command, as follows:
         ---------- example ---------
         [root@chu3l9ap417 vault]# vault status
         Key                Value
         ---                -----
         Seal Type          shamir
         Initialized        true
         Sealed             true
         Total Shares       3
         Threshold          2
         Unseal Progress    0/2
         Unseal Nonce       n/a
         Version            1.2.0
         HA Enabled         true
          ---------- example ---------

    Note Well - in the future this routine can obtain the keys
    from other secure sources.

    2. Run vault unseal, passing in each available key 
   
  """
  rc = True

  #
  # build vault command connection options
  #  e/g,
  #  -address="https://chu3l9ap417.cust.transunion.com:8200"
  #  -ca-cert="/etc/vault/certs/chu3l9ap417.cust.transunion.com.crt"
  #
  #  The values are obtained from the configuration (.yaml) file
  #   vault_addr: "https://chu3l9ap417.cust.transunion.com:8200"
  #   vault_cacert: "/etc/vault/certs/chu3l9ap417.cust.transunion.com.crt"
  #  


  cmd_addr = "-address=%s"%(globals.vault_addr)

  if (globals.vault_cacert  ==  None or globals.vault_cacert  ==  ""):
     cmd_cert = " -tls-skip-verify"
  else:
     cmd_cert = " -ca-cert=%s"%(globals.vault_cacert)


  vault_status_cmd = "vault status %s %s"%(cmd_addr, cmd_cert)
  vault_operator_unseal_cmd = "vault operator unseal %s %s"%(cmd_cert,cmd_addr)

  log("INFO","checking vault server status...")
  log("INFO", vault_status_cmd)

  results = shell(vault_status_cmd)
  rc = results.rc
  log("INFO","rc = %s"%rc)

  if ( results.getrc()  ==  0 ):
     log("INFO","Vault is already unsealed.  Nothing to do, exiting.")
     sys.exit(0)

  if ( results.getrc()  ==  1 ):
     log("ERROR","Vault server is not running. Please restart and rerun this program.")
     sys.exit(1)

  log("INFO", "vault is up, sealed")
  log("INFO", "query vault for 'total shares' and 'theshold' values...")
  # Get vault 'Total Shares' value
  rob = shell("%s | grep -i 'Total' | awk '{print $3}'"%(vault_status_cmd))
  total_shares = int(rob.getstdout().lstrip().rstrip())

  # Get vault 'Threshold' value
  rob = shell("%s | grep -i 'Thres' | awk '{print $2}'"%(vault_status_cmd))
  threshold = int(rob.getstdout().lstrip().rstrip())

  log("INFO","vault 'Total Shares' found:  %s"%total_shares)
  log("INFO","vault 'Threshold' found:     %s"%threshold)
   
  keys = globals.keys
  if (len(keys) == 0):
     log("ERROR","No unseal keys found. Check keyfile %s, exiting."%keyfile)
     sys.exit(1)

  if (len(keys) < threshold):
     log("ERROR","Number of unseal keys found is less than threshold Check keyfile %s, exiting."%keyfile)
     sys.exit(1)
     
  if (len(keys) < total_shares):
     log("WARN","Number of unseal keys found is less than Total Shares  Check keyfile %s"%keyfile)

  for i in keys:
     time.sleep(2)
     log("","unsealing key %s"%i)
     log("INFO",vault_operator_unseal_cmd)
     proc = pexpect.spawn(vault_operator_unseal_cmd) 
     proc.expect('Unseal Key',timeout = 1)
     proc.sendline(i)
     log("INFO", proc.read())
     proc.expect(pexpect.EOF)
     proc.close
     log("","")
     rob = shell(vault_status_cmd)
     log("INFO",vault_status_cmd)
     log("INFO",rob.stdout)
     log("INFO",rob.stderr)
     log("INFO","rc = %s"%str(rob.rc))

  return rc 
if (__name__ == "__main__"):
   try:
      globals = ns()
      PGM = os.path.basename(sys.argv[0])
      HOSTNAME = socket.gethostname()
      globals.PGM = PGM
      globals.HOSTNAME = HOSTNAME
      log("INFO","Starting %s"%(PGM))
      globals.args = getparms()
      getconfig() 
      unseal()
   except KeyboardInterrupt, e:
      log("INFO","Aborted...")
      sys.exit(1)

