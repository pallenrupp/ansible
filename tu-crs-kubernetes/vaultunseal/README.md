## Role Name : vaultunseal 

Install vault unsealer program and boot script
Configurations are separated based on environments and placed in environments directory. 

This playbook installs the following on each inventory node..
  1. python 'pexpect' module (required by vaultunseal.py)
  2. python program /usr/local/bin/vaultunseal.py
  4. python program configuration file /etc/sysconfig/vaultunseal.py.yaml
  3. systemd service file /etc/systemd/system/vaultunseal.service

Execution 

```
To remove everything...
$ ansible-playbook -i environments/{{stage/prod/dev}} playbook.yaml --tags "remove"

To install everything...
$ ansible-playbook -i environments/{{stage/prod/dev}} playbook.yaml
```

NOTE WELL:
  1. Currently, all the necessary python programs, scripts, configuration files
     to enable the unsealer are located here within the role's .templates directory;
     nothing is obtained from outside.  This will likely change in the future
     as the program code is kept in a separate git or artifactory release. 
     P. Rupp - 1/6/2020

     directory
