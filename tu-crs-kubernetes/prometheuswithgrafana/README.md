# Prometheus with Grafana using Ansible

In this project, we are configurating prometheus, node_exporter, alertmanager and Grafana. We setup Grafana dashboard which can use source as Prometheus.

## Getting Started

Step 1: Update ip address of instances in inventory file.

Step 2: Run ansible command to setup prometheus, node_exporter, alertmanager and Grafana services

Ansible command: ansible-playbook playbook.yml

## Downloading and staging software components

   As of 2/5/2020 - P. Rupp -TransUnion LLC

   The original playbook downloaded both Grafana and Prometheus software from 
   internet-based sources. Recently, TransUnion's infosecurity group has limited
   access to gihub.com repositories, preventing the Prometheus components from
   being downloaded.  Grafana components are currenly permitted, but this too
   mah be restricted in the future.

   As a resut, the Prometheus components needed to be manually downloaded via
   VPN or a Campus network, then uploaded into TU's on-prem Artifactory software
   repository.  The playbook was modified to refer to Artifactory.
   The grafana software repository remains on the internet, but now pooints to
   the official Grafana yum repository internet site.

   Below are instructions on how to download the PRometheus components 
   and stage them into TU's artifactory website.

	
   <IMPORTANT>
   Run the following commands on a Transunion VPN or CAMPUS network
   will not work on any server network affinities.
   </IMPORTANT>

   Download Instructions
	
   1. To download prometheus, navigate to https://prometheus.io/download/#prometheus
      Right click on link --> "copy link address"
      e.g,
      https://github.com/prometheus/prometheus/releases/download/v2.15.2/prometheus-2.15.2.linux-amd64.tar.gz
      # curl -JLO https://github.com/prometheus/prometheus/releases/download/v2.15.2/prometheus-2.15.2.linux-amd64.tar.gz
		
   2. To download prometheus Alert Manager
      navigate to https://prometheus.io/download/#alertmanager
      Right click on link --> "copy link address"
      e.g,
      # curl -JLO https://github.com/prometheus/alertmanager/releases/download/v0.20.0/alertmanager-0.20.0.linux-amd64.tar.gz
		
   3. To download prometheus node_exporter
      Navigate to https://prometheus.io/download/#node_exporter
      Right click on link --> " copy link address"
      https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_exporter-0.18.1.linux-amd64.tar.gz
		
	
   Upload to TU's Artifactory
   Use the "storage-local/OSE/k8s/" .  The repository and API key were setup by Peter Rupp. 

   $ curl -H  --insecure 'deployer:D3ployer' -T  <filename>  "https://artifactory.transunion.com/artifactory/storage-local/OSE/k8s/<file name"

