#!/usr/bin/env bash

rm -rf playvars.yml
for i in ansible_*.yml
do
   playvars $i >> playvars.yml
done
mv group_vars/all group_vars/all.$(date '+%Y%m%d')
cp playvars.yml group_vars/all
