#!/usr/bin/env bash
#
# plabook variables go in ./group_vars/all
#
ansible-playbook --flush-cache  -i  hosts.yml --become-method=sudo --become install_cli.yml
