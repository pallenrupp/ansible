#!/usr/bin/env bash

CMD="ansible-playbook -i /etc/ansible/postfix_dev.yaml /root/ansible/playbooks/postfix.yaml"

while true
do
    echo "Are you sure you want to run this? [y|n|q]"
    echo "$CMD"
    echo ""
    read -r answer
    answer=${answer,,}
    if  [[ "$answer" == "y" || "$answer" == "yes" ]]; then
       $CMD 
       break 
    elif [[ "$answer" == "n" || "$answer" == "no" || "$answer" == "q" ]]; then
       break 
    fi
done
