#!/usr/bin/env bash
#
# plabook variables go in ./group_vars/all
#
ansible-playbook --flush-cache  -v -i  hosts.ini --become-method=sudo --become install.yml
